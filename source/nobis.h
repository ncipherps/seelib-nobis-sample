/* nobis.h
 * Copyright (C) 2010-2017 nCipher Security, LLC.  All rights reserved.
 * ---------------------------------------------------------------------
 *
 * An abstraction intended to simplify development of applications
 * that access nCipher hardware security modules.  The name is taken
 * from a relatively obscure legal term:
 *   http://en.wikipedia.org/wiki/Coram_nobis
 *
 * Using nobis offers the following advantages:
 * - Simpler to use than simplecmd with more sensible defaults.
 * - Fewer dependencies (replaces simplecmd, simplebignum, seeutils,
 *   seeworld, signers, nfutils and cardset source files).
 * - The same interface is presented to host platforms and SEE.
 * - One step construction of contexts for message types.
 * - Automatic support for constructing merged key identifiers.
 * - NFKM support:
 *   - Use formatted idents when loading keys.
 *   - Automatic use of existing objects (such as with preload).
 *   - Store keys with optional generation certificate.
 * - FIPS 140 support:
 *   - Routines to share role based authorization with SEE.
 *   - Compliant key import routine that just works.
 *   - Apply FIPS certificates to commands with a transaction flag.
 * - Consistent error reporting for easier debugging.
 * ------------------------------------------------------------------ */
#ifndef NOBIS_H
#define NOBIS_H

#include <stdarg.h>
#include <limits.h>

#ifdef __cplusplus
extern "C" {
#endif

// CodeSafe v12.63 does not set NF_CROSSCC when buidling for SEElib on XC but
// We need a way to identify a SEElib build vs BSDSEE or GLIBSEE so this code
// will set NF_CROSSCC so we can continue to use it.
#ifndef NF_CROSSCC
# if defined(NF_CROSSCC_PPC_GCC) && !defined(NF_CROSSCC_BSDUSR) && !defined(__NGSOLO_GLIBC__)
#  define NF_CROSSCC 1
# endif
#endif

/* To the extent possible this library provides the same interface
 * to host, bsdlib and seelib programs. */
#ifdef NF_CROSSCC
# include <seelib.h>
# define NOBIS_STATUS_NOMEM Status_NoModuleMemory
# define NOBIS_TRACE_FILE stdout
#else
# include <nfastapp.h>
# include <nfkm.h>
# include <nfopt.h>
# ifdef NF_CROSSCC_BSDSR
#  define NOBIS_STATUS_NOMEM Status_NoModuleMemory
#  define NOBIS_TRACE_FILE stdout
# else
#  define NOBIS_STATUS_NOMEM Status_NoHostMemory
#  define NOBIS_TRACE_FILE stderr
#  ifdef _WIN32
#    define __func__ __FUNCTION__
#    define snprintf _snprintf
#    ifndef localtime_r
#      define localtime_r(a, b) localtime_s((b), (a))
#    endif
#  endif
# endif
#endif

/* Encapsulates a connection capable of responding to commands.
 * This spares the application from tracking the assorted data
 * structures necessary to use hardware security modules, load
 * security world keys and so on. */
struct nobis {
  const char *progname;
  unsigned flags;

  /* FIPS 140 authorization certificates and support */
  unsigned n_fips;
  unsigned m_fips;
  struct nobis_fips *fips;

  M_Cmd_GetWorldSigners_Reply worldsigners;

#ifndef NF_CROSSCC
  NFast_AppHandle     apph;
  NFastApp_Connection conn;
  NFKM_WorldInfo     *world;
  struct NF_UserData *udata;
#endif
};

enum {
  nobis_setup_privconn  = (1 << 0),
  nobis_setup_nosworld  = (1 << 1),
  nobis_setup_checknfkm = (1 << 2),
};

/**
 * Allocate resources associated with a hardserver or SEElib.
 *
 * @param nb nCore abstraction to initialize
 * @return Status_OK or a failure code from messages-a-en.h */
M_Status
nobis_setup(struct nobis *nb, unsigned flags,
            const char *progname);

/**
 * Reclaim resources associated with a nobis structure.
 *
 * @param nb nCore abstraction
 * @return Status_OK or a failure code from messages-a-en.h */
void
nobis_cleanup(struct nobis *nb);

/**
 * Send a command to an nCipher hardware security module.  On success
 * the user must free any data allocated in the reply, usually using
 * the nobis_freereply routine.  This is safe to do even on failure.
 *
 * @param nb nCore abstraction
 * @param flags additional features to use
 * @param command nCore command structure to process
 * @param reply nCore reply structure to fill
 * @return Status_OK or a failure code from messages-a-en.h */
enum {
  nobis_tx_freecmd = (1 << 0), /* reclaim any dynamic command memory */
  nobis_tx_fips    = (1 << 1), /* use FIPS certificates if possible */
  nobis_tx_signers = (1 << 2), /* use SEE world signer certificates */
  nobis_tx_quiet   = (1 << 3), /* don't log errors errors */
};
M_Status
nobis__transact(struct nobis *nb, const char *file, unsigned line,
                unsigned flags, M_Command *command, M_Reply *reply);
#define nobis_transact(nb, f, c, r) \
  nobis__transact((nb), __FILE__, __LINE__, (f), (c), (r))

/**
 * Send a command to an nCipher hardware security module using an
 * authorizing key.  Any certificates attached to the target command
 * are overwritten.  On success the user must free any data allocated
 * in the reply, usually using the nobis_freereply routine.  This is
 * safe to do even on failure.
 *
 * @param nb nCore abstraction
 * @param flags additional features to use
 * @param command nCore command structure to process
 * @param reply nCore reply structure to fill
 * @param auth_key key handle for use in a SigningKey certificate
 * @return Status_OK or a failure code from messages-a-en.h */
M_Status
nobis__transact_certify(struct nobis *nb, const char *file,
                        unsigned line, unsigned flags,
                        M_Command *command, M_Reply *reply,
                        M_KeyID auth_key);
#define nobis_transact_certify(nb, f, c, r, a) \
  nobis__transact_certify((nb), __FILE__, __LINE__, (f), (c), (r), (a))

/**
 * Reclaim resources used by a transaction reply.  This is necessary
 * only when nobis_transact succeeds with Status_OK but is safe
 * regardless of the return value.
 *
 * @param nb nCore abstraction
 * @param reply nCore reply structure to reclaim
 * @return nCore abstraction provided by first parameter */
struct nobis *
nobis_freereply(struct nobis *nb, M_Reply *reply);

/**
 * Reclaim memory used by a byte block pointer.
 *
 * @param nb nCore abstraction
 * @param bb byte block to reclaim
 * @return nCore abstraction provided by first parameter */
struct nobis *
nobis_freebb(struct nobis *nb, M_ByteBlock *bb);

/**
 * Create a key ticket for a given object.  The seew parameter can be
 * zero, in which case the ticket can be redeemed anywhere.  Otherwise
 * it is only redeemable in the specified SEE world.  On success the
 * destination byte block must be reclaimed, as with
 * NF_Free_ByteBlock.  Regardless of the return status the byte block
 * is as safe to free as it was before the function was called.
 *
 * @param nb nCore abstraction
 * @param dest location to store key ticket
 * @param seew optional key handle for SEE world for limiting ticket
 * @param obj key handle to serve as the source of the ticket
 * @return Status_OK or a failure code from messages-a-en.h */
M_Status
nobis__ticket_create(struct nobis *nb, const char *file, unsigned line,
                     M_ByteBlock *dest, M_KeyID seew, unsigned flags,
                     M_KeyID obj);
#define nobis_ticket_create(nb, d, w, f, o)                     \
  nobis__ticket_create((nb), __FILE__, __LINE__, (d), (w), (f), (o))

/**
 * Convert an opaque key ticket into a key handle.
 *
 * @param nb nCore abstraction
 * @param dest location in which to store key handle
 * @param module indicates which HSM the ticket is associated with
 * @param ticket key ticket to redeem
 * @return Status_OK or a failure code from messages-a-en.h */
M_Status
nobis__ticket_redeem(struct nobis *nb, const char *file, unsigned line,
                     M_KeyID *dest, M_ModuleID module,
                     const M_ByteBlock *ticket);
#define nobis_ticket_redeem(nb, d, m, t) \
  nobis__ticket_redeem((nb), __FILE__, __LINE__, (d), (m), (t))


/**
 * Collect the authorization necessary to perform operations
 * restricted in StrictFIPS140 security worlds.
 *
 * @param nb nCore abstraction
 * @param module which HSM to collect FIPS auth for
 * @param slot which slot on the HSM to read a card from
 * @return Status_OK or a failure code from messages-a-en.h */
M_Status
nobis_fips_auth(struct nobis *nb, M_ModuleID module, M_SlotID slot);

/**
 * Prepare StrictFIPS140 authorization credentials for transport to
 * another client or SEE world.  The seew paramter may be zero in
 * which case any client or SEE machine can unpack credentials.
 *
 * @param nb nCore abstraction
 * @param module which HSM to pack FIPS auth for
 * @param seew optional key handle for SEE world for limiting tickets
 * @param bb location in which to store packed FIPS authorization
 * @return Status_OK or a failure code from messages-a-en.h */
M_Status
nobis_fips_pack(struct nobis *nb, M_ModuleID module, M_KeyID seew,
                M_ByteBlock *bb);

/**
 * Accept StrictFIPS140 authorization credentials from another
 * client or SEE world.
 *
 * @param nb nCore abstraction
 * @param module which HSM to unpack FIPS auth for
 * @param bb packed FIPS authorization created by nobis_fips_pack
 * @return Status_OK or a failure code from messages-a-en.h */
M_Status
nobis_fips_unpack(struct nobis *nb, M_ModuleID module, M_ByteBlock *bb);

/**
 * Convert a byte block to an arbitrary precision integer.  The bn
 * pointer must be released by calling nobis_bignum_cleanup when it is
 * no longer needed.  This is safe to do regardless of the return
 * status but only necessary on Status_OK.
 *
 * @param nb nCore abstraction
 * @param bn location for storing arbitrary precision integer
 * @param in value of integer in big endian order (byte zero is
 * the most significant).
 * @return Status_OK or a failure code from messages-a-en.h */
M_Status
nobis_bignum_create(struct nobis *nb, M_Bignum *bn,
                    const M_ByteBlock *in);

/**
 * Convert a byte block to an arbitrary precision integer.  The bn
 * pointer must be released by calling nobis_bignum_cleanup when it is
 * no longer needed.  This is safe to do regardless of the return
 * status but only necessary on Status_OK.
 *
 * @param nb nCore abstraction.
 * @param bn location for storing arbitrary precision integer.
 * @param in value of integer in little endian order (byte zero is
 * the least significant).
 * @return Status_OK or a failure code from messages-a-en.h. */
M_Status
nobis_bignum_create_fromLE(struct nobis *nb, M_Bignum *bn,
                    const M_ByteBlock *in);

/**
 * Reclaim resources associated with an arbitrary precision integer.
 * This is necessary only when nobis_bignum_create succeeds with
 * Status_OK.
 *
 * @param nb nCore abstraction
 * @param bn arbitrary precision integer to reclaim
 * @return Status_OK or a failure code from messages-a-en.h */
void
nobis_bignum_cleanup(struct nobis *nb, M_Bignum *bn);

/**
 * Return the size in bytes of the given bignum */
unsigned
nobis_bignum_size(const M_Bignum *bn);

/**
 * Populate a byte block with the contents of an arbitrary precision
 * integer.  Results will be stored in big endian order, which means
 * the first byte will be the most significant and the last byte will
 * be the least.  Memory for the byte block is dynamically allocated
 * and should be released using nobis_freebb() when no longer needed.
 * There may be one or more leading zero bytes.
 *
 * @param nb nCore abstraction
 * @param bn arbitrary precision integer to convert
 * @param out location in which to store integer in big endian order
 * (byte zero is the most significant).
 * @return Status_OK or a failure code from messages-a-en.h */
M_Status
nobis_bignum_bytes(struct nobis *nb, const M_Bignum *bn,
                   M_ByteBlock *out);

/**
 * Populate a byte block with the contents of an arbitrary precision
 * integer, prepending enough zero bytes to enforce a minimum size.
 * This minimum size can be helpful when hashing, for example.
 * Results will be stored in big endian order, which means the first
 * byte will be the most significant and the last byte will be the
 * least.  Memory for the byte block is dynamically allocated and
 * should be released using nobis_freebb() when no longer needed.
 * There may be one or more leading zero bytes.
 *
 * @param nb nCore abstraction
 * @param bn arbitrary precision integer to convert
 * @param min smallest number of bytes in output (zeros prepended)
 * @param out location in which to store integer in big endian order
 * (byte zero is the most significant).
 * @return Status_OK or a failure code from messages-a-en.h */
M_Status
nobis_bignum_bytes_minimum(struct nobis *nb, const M_Bignum *bn,
                           unsigned min, M_ByteBlock *out);


/**
 * Compare two arbitrary precision integers and return zero iff
 * the two are the same.  Note that values other than zero are
 * reserved for future use and not guaranteed to mean anything
 * other than that the numbers are different.
 *
 * @param nb nCore abstraction
 * @param bn_a one arbitrary precision integer
 * @param bn_a another arbitrary precision integer
 * @return 0 iff the two input numbers are the same */
int
nobis_bignum_cmp(const M_Bignum *bn_a, const M_Bignum *bn_b);

/**
 * Populate a byte block with the contents of an arbitrary precision
 * integer.  Results will be stored in little endian order, which means
 * the first byte will be the least significant and the last byte will
 * be the most.  Memory for the byte block is dynamically allocated
 * and should be released using nobis_free() when no longer needed.
 * There may be one or more trailing zero bytes.
 *
 * @param nb nCore abstraction.
 * @param bn arbitrary precision integer to convert.
 * @param out location in which to store integer in little endian order
 * (byte zero is the least significant).
 * @param minsize minmum size of output buffer; padded with zeros if required
 * @return Status_OK or a failure code from messages-a-en.h. */
M_Status
nobis_bignum_bytes_LE(struct nobis *nb, const M_Bignum *bn,
                   M_ByteBlock *out, size_t minsize);

/**
 * Test a M_Bignum to determine if it is odd or even.
 *
 * @param nb nCore abstraction.
 * @param bn arbitrary precision integer to test.
 * @return 0 if Bignum is even; non-zero otherwise
 */
int
nobis_bignum_is_odd(struct nobis *nb, const M_Bignum *bn);

/* ================================================================== */
/* Some features are impractical to implement using SEElib and so are
 * defined here for use in host and bsdlib programs. */
#ifndef NF_CROSSCC

/**
 * Conversion function for NFOPT that uses automatically generated
 * enumerations as command line arguments through NF_LookupString.
 *
 * @param i NFOPT processing structure
 * @return zero iff option parsing has been successful */
int nfopt_nflookup(const nfopt_info *i);

/**
 * Creates a command line argument based on an automatically generated
 * enumeration such as M_Mech or M_KeyType. */
#define NFOPT_NFLOOKUP(o, opt, arg, p, tab, help) \
  { o, opt, arg, nfopt_nflookup, p, help, tab },

/**
 * Conversion function for NFOPT that creates hex decoded byte blocks.
 *
 * @param i NFOPT processing structure
 * @return zero iff option parsing has been successful */
int nfopt_byteblock(const nfopt_info *i);

/**
 * Creates a command line argument that hex decodes the supplied option
 * into a byte block. */
#define NFOPT_BYTEBLOCK(o, opt, arg, p, help) \
  { o, opt, arg, nfopt_byteblock, p, help },

/* ------------------------------------------------------------------ */

/**
 * Collect a key info structure associated with a named key.
 * If the ident contains a comma (',') then everything before the
 * comma is used as the appname and everything after is used as
 * an ident.  The supplied appname is used otherwise.  Iff this
 * function returns Status_OK it is necessary to free the NFKM_Key
 * structure by calling nobis_sworld_freekeyinfo().
 *
 * @param nb nCore abstraction
 * @param kinfo location in which to store key information pointer
 * @param flags settings to use for lookup
 * @param appname type of key to search for if ident doesn't specify
 * @param ident formatted label of key to find
 * @return Status_OK or a failure code from messages-a-en.h */
M_Status
nobis_sworld_keyinfo(struct nobis *nb, NFKM_Key **kinfo, unsigned flags,
                     const char *appname, const char *ident, ...);

/**
 * Reclaim resoures associated with a key info structure.
 *
 * @param nb nCore abstraction
 * @param kinfo key information pointer to reclaim */
void
nobis_sworld_freekeyinfo(struct nobis *nb, NFKM_Key *kinfo);

/**
 * Gather the data structure necessary for loading the logical token
 * associated with a named card set or soft card.  A name can either
 * be the human readable string representation or a hex encoded hash
 * of the logical token.
 *
 * @param nb nCore abstraction
 * @param name label or hexidecimal encoded hash of token
 * @param cs location to store pointer to card set, if applicable
 * @param sc location to store pointer to soft card, if applicable
 * @return Status_OK or a failure code from messages-a-en.h */
M_Status
nobis_sworld_tokenstruct(struct nobis *nb, const char *name,
                         const NFKM_CardSet **cs,
                         const NFKM_SoftCard **sc);

/**
 * Load the logical token associated with an operator card set,
 * prompting for cards and pass phrases on the standard input and
 * error streams if necessary.
 *
 * @param nb nCore abstraction
 * @param cs card set to load
 * @param module HSM to use
 * @param slot slot on HSM to use for reading cards
 * @param token location to store loaded logical token
 * @return Status_OK or a failure code from messages-a-en.h */
M_Status
nobis_sworld_cstoken(struct nobis *nb, const NFKM_CardSet* cs,
                     M_ModuleID module, M_SlotID slot, M_KeyID* token);

/**
 * Load the logical token associated with a softcard, prompting for
 * the pass phrase on the standard input and error streams.
 *
 * @param nb nCore abstraction
 * @param sc soft card to load
 * @param module HSM to use
 * @param token location to store loaded logical token
 * @return Status_OK or a failure code from messages-a-en.h */
M_Status
nobis_sworld_sctoken(struct nobis *nb, const NFKM_SoftCard* sc,
                     M_ModuleID module, M_KeyID* token);

/**
 * Create an access control list suitable for a private key to be
 * stored in a security world.  If seehash is not NULL the ACL
 * is bound to the SEE integrity key specified by its value.
 *
 * @param nb nCore abstraction
 * @param flags controls whether the key is recoverable
 * @param op_base controls which operations a key can perform
 * @param token name or hexidecimal encoded hash of logical token
 * @param seeii ident of SEE integrity key to bind to
 * @param pa_uselimit Per-authorization use limit.
 * @param aclprv location in which to store generated ACL
 * @return Status_OK or a failure code from messages-a-en.h */
M_Status
nobis_sworld_makeacl(struct nobis *nb, M_Word flags, M_Word op_base,
                     const char *token, const char *seeii, M_Word pa_uselimit,
                     M_ACL *aclprv);

/**
 * Store a key or key pair permanently in the security world.  Pass
 * NULL for the ModuleCert if this key was imported.  Pass NULL for
 * seeident if binding if no SEE integrity key controls operations for
 * the stored key.  If this function succeeds it should be possible to
 * load the key using a mechanism such as nobis_sworld_loadkp.
 *
 * @param nb nCore abstraction
 * @param flags controls whether a recovery blob is created
 * @param module HSM to use
 * @param slot slot on HSM to use for reading cards
 * @param kpub key handle of public key or zero if absent
 * @param kprv key handle of private key
 * @param mc optional module generation certificate
 * @param nentries number of extra data entries
 * @param entries array of extra data entries
 * @param token name or hexidecimal encoded hash of logical token
 * @param seeident label of SEE integrity key to bind to
 * @param appname type of key if ident doesn't specify
 * @param ident formatted label of key to store
 * @return Status_OK or a failure code from messages-a-en.h */
M_Status
nobis_sworld_storekp(struct nobis *nb, M_Word flags,
                     M_ModuleID module, M_SlotID slot,
                     M_KeyID kpub, M_KeyID kprv, M_ModuleCert *mc,
                     int nentries, M_KeyMgmtFileEntry *entries,
                     const char *token, const char *seeident,
                     const char *appname, const char *ident, ...);

enum {
  nobis_keyi_missingok  = (1 << 0), /* Quiet when key not found */
  nobis_keyi_quietpub   = (1 << 1), /* Quiet about missing pub */
  nobis_keyi_quietprv   = (1 << 2), /* Quiet about missing pub */
  nobis_keyi_quiet = (nobis_keyi_quietprv | nobis_keyi_quietpub |
                      nobis_keyi_missingok),
  nobis_keyi_nocardset  = (1 << 3), /* Don't load card sets */
  nobis_keyi_nosoftcard = (1 << 4), /* Don't load soft cards */
  nobis_keyi_notoken = nobis_keyi_nocardset | nobis_keyi_nosoftcard,
};

/**
 * Load a key handle for the logical token necessary to load the
 * specified key. Only if this routine returns Status_OK is it
 * necessary to destroy the token, as with nobis_destroy().  This
 * routine is safe to use with module protected keys, but the token
 * parameter will be set to zero in that case.
 *
 * @param nb nCore abstraction
 * @param flags setting from nobis_keyi enumeration
 * @param module HSM to use
 * @param slot slot on HSM to use for reading cards
 * @param kinfo key information structure for key to load
 * @param token location in which to store loaded logical token
 * @return Status_OK or a failure code from messages-a-en.h */
M_Status
nobis_sworld_keytoken(struct nobis *nb, unsigned flags,
                      M_ModuleID module, M_SlotID slot,
                      NFKM_Key *kinfo, M_KeyID *token);

/**
 * Load a key or key pair from the security world.  If the ident
 * string begins with an appname followed by a comma this overrides
 * the appname parameter.  The ident can also be parameterized as
 * a printf style string.  Here is an example:
 *     "seeinteg,test-key-%u"
 * This loads a seeinteg key (regardless of the appname parameter
 * value) with an ident ending in a non-negative number.
 *
 * @param nb nCore abstraction
 * @param flags settings for loading key pair
 * @param kpub optional location in which to store public key handle
 * @param kprv optional location in which to store private key handle
 * @param kinfo optional location in which to store key information
 * pointer associated with key.
 * @param module HSM to use
 * @param slot slot on HSM to use for reading cards
 * @param appname type of key if ident doesn't specify
 * @param ident formatted label of key to load
 * @return Status_OK or a failure code from messages-a-en.h */
M_Status
nobis_sworld_loadkp(struct nobis *nb, unsigned flags,
                    M_KeyID *kpub, M_KeyID *kprv, NFKM_Key **kinfo,
                    M_ModuleID module, M_SlotID slot,
                    const char *appname, const char *ident, ...);

/**
 * Load the logical token associated with one or more administrator
 * card set keys, prompting for cards and pass phrases on the standard
 * input and error streams if necessary.  An integer value -1 must be
 * the final argument to this function.  The NFKM_loadadminkeys_begin
 * function must have already been successfully called on the lakh
 * parameter or the results are undefined.  It is always necessary to
 * call NFKM_loadadminkeys_done() on the lakh parameter afterward, but
 * note that this will disable any administrator keys that have been
 * loaded.  Therefore it is NOT necessary to destroy any keys or
 * tokens subsequently fetched from the handle (unless stolen).
 *
 * @param nb nCore abstraction
 * @param module HSM to use
 * @param slot slot on HSM to use for reading cards
 * @param lakh state to use for loading operation
 * @param tokenspec list of tokens to load (terminated by -1)
 * @return Status_OK or a failure code from messages-a-en.h */
M_Status
nobis_sworld_admin_load(struct nobis *nb, int module, int slot,
                        NFKM_LoadAdminKeysHandle *lakh,
                        int tokenspec, ...);

/**
 * Load the logical token associated with one or more administrator
 * card set keys, prompting for cards and pass phrases on the standard
 * input and error streams if necessary.  An integer value -1 must be
 * the final argument to this function.  The lakh parameter is
 * automatically initialized.  If (and only if this function succeeds,
 * it must be reclaimed by calling NFKM_loadadminkeys_done()
 * afterward, but note that this will disable any administrator keys
 * that have been loaded.  Therefore it is NOT necessary to destroy
 * any keys or tokens subsequently fetched from the handle (unless
 * stolen).
 *
 * @param nb nCore abstraction
 * @param module HSM to use
 * @param slot slot on HSM to use for reading cards
 * @param lakh state to use for loading operation
 * @param tokenspec list of tokens to load (terminated by -1)
 * @return Status_OK or a failure code from messages-a-en.h */
M_Status
nobis_sworld_admin_setup(struct nobis *nb, int module, int slot,
                         NFKM_LoadAdminKeysHandle *lakh,
                         int tokenspec, ...);

/**
 * Create a merged key identifier across an arbitrary number of
 * nShield HSMs for fail over and load sharing.  To use all available
 * modules, specify zero for the n_modules parameter.  To use only
 * a specified subset, pass an array as the modules parameter and
 * set n_modules to the size of the array.
 *
 * Some blobs require a logical token for authorization.  This can be
 * supplied on a per module basis but loading and managing these
 * tokens is outside the scope of this routine.
 *
 * @param nb nCore abstraction
 * @param mergedkey merged key identifier output
 * @param blob protected HSM key bytes
 * @param token optional and possibly merged authorization
 * @param number of logical tokens provided
 * @param modules optional array of module identifiers
 * @param number of modules to use
 * @return Status_OK or a failure code from messages-a-en.h */
M_Status
nobis__merge_loadblob(struct nobis *nb, const char *file, int line,
                      M_KeyID *mergedkey, const M_ByteBlock *blob,
                      M_KeyID token,
                      M_ModuleID *modules, unsigned n_modules);
#define nobis_merge_loadblob(nb, mk, b, t, m, nm) \
  nobis__merge_loadblob(nb, __FILE__, __LINE__, mk, b, t, m, nm)

/**
 * Reclaim resources used by a merged key.
 *
 * @param nb nCore abstraction
 * @param mergedkey merged key identifier
 * @return Status_OK or a failure code from messages-a-en.h */
M_Status
nobis__merge_destroy(struct nobis *nb, const char *file, int line,
                     M_KeyID mergedkey);
#define nobis_merge_destroy(nb, mk) \
  nobis__merge_destroy(nb, __FILE__, __LINE__, mk)

/**
 * Open a file using a format string for the name.
 *
 * @param mode controls for opening the file (see man fopen(3))
 * @param fname formatted name of the file to open
 * @return a pointer to a FILE or NULL if the operation failed */
FILE *
nobis_fopenf(const char *mode, const char *fname, ...);

/**
 * Open a file and read its contents into a byte block.  The
 * destination byte block is at least as safe to free as it was when
 * passed in, regardless of the return status, but is necessary to
 * free only on success.
 *
 * @param nb nCore abstraction
 * @param dest location in which to store file contents
 * @param fname formatted name of the file to read
 * @return Status_OK or a failure code from messages-a-en.h */
M_Status
nobis_file2bb(struct nobis *nb, M_ByteBlock *dest,
              const char *fname, ...);

/**
 * Open a file and write the contents of a byte block into it.  If
 * mode is NULL the value "wb" is used (the only other useful value is
 * probably "ab").
 *
 * @param nb nCore abstraction
 * @param src contents to write into file
 * @param mode controls for opening the file (see man fopen(3))
 * @param fname formatted name of the file to read
 * @return Status_OK or a failure code from messages-a-en.h */
M_Status
nobis_bb2file(struct nobis *nb, M_ByteBlock *src, const char *mode,
              const char *fname, ...);

/**
 * Establish an instance of an SEE world for a machine that has
 * already been loaded.
 *
 * @param nb nCore abstraction
 * @param module HSM to use
 * @param slot slot on HSM to use for reading cards
 * @param debug set to non-zero to enable SEE trace debugging
 * @param user_data contents of SEE user data to load
 * @param initstat location to store SEE initialization status
 * @param seew location to store SEE world key handle
 * @return Status_OK or a failure code from messages-a-en.h */
M_Status
nobis_seew_create(struct nobis *nb, M_ModuleID module, M_SlotID slot,
                  int debug, const M_ByteBlock* user_data,
                  M_Word *initstat, M_KeyID *seew);

/**
 * Get a handle to a published SEE object
 *
 * @param nb nCore abstration.
 * @param module HSM to use.
 * @param name name of published object to get from hardserver
 * @param seew location to store SEE world key handle.
 * @return Status_OK or a failure code from messages-a-en.h. */
M_Status
nobis_seew_getpublished(struct nobis *nb, M_ModuleID module,
                        const char *name, M_KeyID *seew);

/**
 * Copy the contents of the debug buffer of an SEE world to a file.
 *
 * @param nb nCore abstraction
 * @param seew location to store SEE world key handle
 * @param f file to which to write trace data
 * @return Status_OK or a failure code from messages-a-en.h */
M_Status
nobis_seew_trace(struct nobis *nb, M_KeyID seew, FILE *f);

#endif

/**
 * Push a block of bytes into a hardware security module.  This is
 * useful primarily for loading SEE machines and creating SEE worlds.
 *
 * @param nb nCore abstraction
 * @param module HSM to use
 * @param enckey optional SEE confidentiality key for decrypting buffer
 * @param iv optional  initialization vector for decrypting buffer
 * @param bb contents to be loaded into a buffer
 * @param buffid location to store buffer key handle
 * @return Status_OK or a failure code from messages-a-en.h */
M_Status
nobis__loadbuffer(struct nobis *nb, const char *file, unsigned line,
                  M_ModuleID module, M_KeyID enckey,
                  const M_ByteBlock *bb, M_KeyID *buffid);
#define nobis_loadbuffer(nb, m, e, bb, o) \
  nobis__loadbuffer((nb), __FILE__, __LINE__, (m), (e), (bb), (o))

/**
 * Create a DKTemplate key with nested data from the supplied
 * ACL and userdata.
 *
 * @param nb nCore abstraction
 * @param module HSM to use
 * @param acl access control list for template key
 * @param ad application data to embed in key
 * @param mech mechanism to permit for DeriveKey
 * @param out location to store generated key handle
 * @return Status_OK or a failure code from messages-a-en.h */
M_Status
nobis_acl_template(struct nobis *nb, M_ModuleID module, const M_ACL *acl,
                   const M_AppData* ad, M_DeriveMech dmech, M_KeyID* out);

/**
 * Generate a WrapKey suitable for tasks which are not security
 * critical, such as FIPS import.  On success the caller is
 * responsible for destroying wrap_key contents.
 *
 * @param nb nCore abstraction
 * @param module HSM to use
 * @param wrap_key output key handle
 * @return Status_OK or a failure code from messages-a-en.h */
M_Status
nobis_generate_wrapper(struct nobis *nb, M_ModuleID module,
                       M_KeyID *wrap_key);

/**
 * Import a public key.  This is simple and efficient but will not
 * work for private keys in a StrictFIPS140 security world.
 *
 * @param nb nCore abstraction
 * @param module HSM to use
 * @param kdpub key material for public key
 * @param aclpub access control list for public key (optional)
 * @param adpub application data for public key (optional)
 * @param kpub location to store imported public key
 * @return Status_OK or a failure code from messages-a-en.h */
M_Status
nobis__import_public(struct nobis *nb, const char *file, unsigned line,
                     M_ModuleID module,
                     const M_KeyData *kdpub, const M_ACL *aclpub,
                     const M_AppData *adpub, M_KeyID *kpub);
#define nobis_import_public(nb, m, kd, acl, ad, k) \
  nobis__import_public(nb, __FILE__, __LINE__, m, kd, acl, ad, k)

/**
 * Import a private key in a manner that satisifies the requirements
 * of the FIPS 140 standard at level 3 and above.
 *
 * @param nb nCore abstraction
 * @param module HSM to use
 * @param kdprv key material for private key
 * @param aclprv access control list for private key
 * @param adprv application data for private key
 * @param kprv location to store imported private key
 * @return Status_OK or a failure code from messages-a-en.h */
M_Status
nobis_import_private(struct nobis *nb, M_ModuleID module,
                     M_KeyData *kdprv, M_ACL *aclprv,
                     M_AppData *adprv, M_KeyID *kprv);

/**
 * Import a private key in a manner that satisifies the requirements
 * of the FIPS 140 standard at level 3 and above.  The
 * nobis_import_private() routine generates an ephemeral template and
 * key, which would make it inefficient to use with large groups of
 * keys.  This routine exposes more of the implementation details to
 * make more efficient batch operations possible.
 *
 * @param nb nCore abstraction
 * @param module HSM to use
 * @param kdprv key material for private key
 * @param tplt template key to use for DeriveKey
 * @param wrap wrap key to use for DeriveKey
 * @param kprv location to store imported private key
 * @return Status_OK or a failure code from messages-a-en.h */
M_Status
nobis_import_private_bulk(struct nobis *nb, M_ModuleID module,
                          M_KeyData *kdprv, M_KeyID tplt,
                          M_KeyID wrap, M_KeyID *kprv);

/**
 * Export a key from the Security World using another key to encrypt
 * it for transport to some other system.
 *
 * @param nb nCore abstraction
 * @param module HSM to use
 * @param base key to be exported
 * @param wrap key to encrypt exported key
 * @param dmech mechanism for key derivation
 * @param dkparams parameters for derivation mechanism
 * @param outbb byte block in which to store exported key
 * @return Status_OK or a failure code from messages-a-en.h */
M_Status
nobis__export(struct nobis *nb, const char *file, unsigned line,
              M_ModuleID module, M_KeyID base, M_KeyID wrap,
              M_DeriveMech dmech, M_DeriveMech__DKParams *dkparams,
              M_ByteBlock *outbb);
#define nobis_export(nb, m, b, w, d, p, o) \
  nobis__export((nb), __FILE__, __LINE__, (m), (b), (w), (d), (p), (o))

/**
 * Retrieves key material.  Successful use of this funciton requires
 * that the base and wrap keys have appropriate DeriveKey permission
 * in their access control lists.
 *
 * @param nb nCore abstraction.
 * @param module HSM to use.
 * @param base_key key handle to be exported.
 * @param wrap_key key with which to wrap base key.
 * @param kdata location to store exported key material.
 * @return Status_OK or a failure code from messages-a-en.h. */
M_Status
nobis__export_private(struct nobis *nb, const char *file, unsigned line,
                      M_ModuleID module, M_KeyID base_key, M_KeyID wrap_key,
                      M_KeyData *kdata);
#define nobis_export_private(nb, m, b, w, d) \
  nobis__export_private((nb), __FILE__, __LINE__, (m), (b), (w), (d))

/**
 * Export a "public" key (or any key that allows ExportAsPlain)
 *   uses nobis_tx_signers so this will work when the SEE
 *   App has the special privialge of ExportAsPlain
 *
 * @param nb nCore abstraction.
 * @param key key handle to be exported.
 * @param kdata location to store exported key material.
 * @return Status_OK or a failure code from messages-a-en.h. */
M_Status
nobis__export_public(struct nobis *nb, const char *file, unsigned line,
                     M_KeyID key, M_KeyData *kdata);
#define nobis_export_public(nb, k, d) \
  nobis__export_public((nb), __FILE__, __LINE__, (k), (d))

/**
 * Free M_KeyData memory using an NFast_AppHandle.
 *
 * @param nb nCore abstraction.
 * @param reply KeyData structure to reclaim. */
void
nobis_freeKeyData(struct nobis *nb, M_KeyData *kd);

/**
 * Create a fixed sized digest of an arbitrary sized byte block,
 * working around internal limits.  Setting the chunk size to zero
 * indicates that this routine should choose the largest reasonable
 * size rather than relying on the caller to specify something.  On
 * success the output byte block will be used to store the result.  If
 * -- and only if -- out->ptr is not NULL and out->len is large enough
 * the value will be copied.  Otherwise a new block of memory will be
 * allocated and must be reclaimed by the caller using
 * NF_Free_ByteBlock.  On failure the out byte block is not changed.
 *
 * @param nb nCore abstraction
 * @param module HSM to use
 * @param mech mechanism to permit for creating digest
 * @param chunk divide input into chunks of this size (zero for auto)
 * @param in data to be fed into digest
 * @param out location to store generated digest
 * @return Status_OK or a failure code from messages-a-en.h */
M_Status
nobis__digest(struct nobis *nb, const char *file, unsigned line,
              M_ModuleID module, M_Mech mech, unsigned chunk,
              const M_ByteBlock *in, M_ByteBlock *out);
#define nobis_digest(nb, m, e, c, i, o) \
  nobis__digest(nb, __FILE__, __LINE__, m, e, c, i, o)

typedef M_Status (*nobis_hashfn_t)
(struct nobis *nb, const M_ByteBlock *input, M_ByteBlock *output);

M_Status
nobis_sha256(struct nobis *nb, const M_ByteBlock *input,
             M_ByteBlock *output);

/**
 * Generate a mask value of length equal to output->len using the
 * supplied digest function.  If output->ptr is not NULL then
 * data is copied into it.  Otherwise a block of bytes large enough
 * is allocated and must be reclaimed by the caller.
 *
 * @param nb nCore abstraction
 * @param hashfn cryptographic hash function used for byte generation
 * @param input random nonce used as input to mask generation
 * @param output location to store generated mask (allocated if NULL)
 * @return Status_OK or a failure code from messages-a-en.h */
M_Status
nobis_mgf1(struct nobis *nb, nobis_hashfn_t hashfn,
           const M_ByteBlock *input, M_ByteBlock *output);

/**
 * Encrypt an arbitrary sized byte block, working around internal
 * limits.  Setting the chunk size to zero indicates that this routine
 * should choose the largest reasonable size rather than relying on
 * the caller to specify something.  On success the out byte block
 * must be reclaimed by the caller using NF_Free_ByteBlock.  On
 * failure the out byte block is not changed.
 *
 * @param nb nCore abstraction
 * @param module HSM to use
 * @param mech mechanism to permit for creating digest
 * @param chunk divide input into chunks of this size (zero for auto)
 * @param in data to be fed into digest
 * @param out location to store generated digest
 * @return Status_OK or a failure code from messages-a-en.h */
M_Status
nobis__encrypt(struct nobis *nb, const char *file, unsigned line,
               M_KeyID kpub, M_Mech mech, unsigned chunk,
               M_IV *iv, const M_ByteBlock *in, M_ByteBlock *out);
#define nobis_encrypt(nb, kpub, mech, chunk, iv, in, out) \
  nobis__encrypt(nb, __FILE__, __LINE__, kpub, mech, chunk, iv, in, out)

/**
 * Perform an RSA operation.  OAEP, PKCS#1 v1.5 and raw (no padding)
 * are supported.  The out byte block pointer needs to be freed unless
 * the verify operation is specified, in which case it is expected to
 * be signature input while the in byte block is plain text.
 *
 * @param nb nCore abstraction
 * @param flags settings for operation and digest
 * @param k RSA key handle to use
 * @param in data to be fed into RSA operation
 * @param out data output from RSA operation (more input for verify)
 * @return Status_OK or a failure code from messages-a-en.h */
enum {
  nobis_rsa_sign    = 0,
  nobis_rsa_decrypt = 1,
  nobis_rsa_encrypt = 2,
  nobis_rsa_verify  = 3,
  nobis_rsa_maskop  = 0x0F,

  nobis_rsa_raw      = 0x10,
  nobis_rsa_md5      = 0x20,
  nobis_rsa_sha1     = 0x30,
  nobis_rsa_sha224   = 0x40,
  nobis_rsa_sha256   = 0x50,
  nobis_rsa_sha384   = 0x60,
  nobis_rsa_sha512   = 0x70,
  nobis_rsa_maskhash = 0xFF0,

  nobis_rsa_signers = 0x1000,
  nobis_rsa_oaep    = 0x2000,
};
M_Status
nobis_rsa(struct nobis *nb, unsigned flags, M_KeyID k,
          const M_ByteBlock *in, M_ByteBlock *out);

/**
 * Returns the appropriate nobis_rsa flags for a given mechanism
 *
 * @param nb nCore abstraction
 * @param mech mechanism desired
 * @return flags to use with nobis_rsa */
unsigned
nobis_rsa_mechflags(struct nobis *nb, M_Mech mech);

/**
 * Collect generic information about a key.  Each of type, nbits and
 * hash may be NULL, in which case no information is returned.
 *
 * @param nb nCore abstraction
 * @param k key for which informatio is requested
 * @param type optional location to store key type
 * @param nbits optional location to store key size in bits
 * @param hash optional location to store key hash
 * @return Status_OK or a failure code from messages-a-en.h */
M_Status
nobis__keyinfo(struct nobis *nb, const char *file, unsigned line,
               M_KeyID k, M_KeyType *type, M_Word *nbits,
               M_KeyHash *hash);
#define nobis_keyinfo(nb, k, t, n, h) \
  nobis__keyinfo((nb), __FILE__, __LINE__, (k), (t), (n), (h))

/**
 * Read appdata of a key
 *
 * @param nb nCore abstraction.
 * @param k key for which informatio is requested.
 * @param appdata optional location to the appdata.
 * @return Status_OK or a failure code from messages-a-en.h. */
M_Status
nobis__getappdata(struct nobis *nb, const char *file, unsigned line,
               M_KeyID k, M_AppData* appdata);
#define nobis_getappdata(nb, k, a) \
  nobis__getappdata((nb), __FILE__, __LINE__, (k), (a))

/**
 * Return non-zero if and only if the key type is the public portion
 * of an asymmetric pair.  If the k parameter is non-zero its key type
 * is queried and used instead of the kt parameter.
 *
 * @param nb nCore abstraction
 * @param kt optional type of key (otherwise retrieved from k)
 * @param k key handle to query (may be zero if kt is non-zero)
 * @return Status_OK or a failure code from messages-a-en.h */
int
nobis_is_public(struct nobis *nb, M_KeyType kt, M_KeyID k);

/**
 * Return non-zero if and only if the key type is part of an
 * asymmetric pair.  If the k parameter is non-zero its key type is
 * queried and used instead of the kt parameter.
 *
 * @param nb nCore abstraction
 * @param kt optional type of key (otherwise retrieved from k)
 * @param k key handle to query (may be zero if kt is non-zero)
 * @return Status_OK or a failure code from messages-a-en.h */
int
nobis_is_asymm(struct nobis *nb, M_KeyType kt, M_KeyID k);

/**
 * Release resources associated with a key handle.
 *
 * @param nb nCore abstraction
 * @param k key handle to destroy (may be zero)
 * @return Status_OK or a failure code from messages-a-en.h */
M_Status
nobis__destroy(struct nobis *nb, const char *file, unsigned line,
               M_KeyID k);
#define nobis_destroy(nb, k) \
  nobis__destroy((nb), __FILE__, __LINE__, (k))

/**
 * Overwrite the contents of a block with randomly generated bytes
 * created using the random number generator of an HSM.
 *
 * @param nb nCore abstraction
 * @param bb byte block in which to write random bytes
 * @return Status_OK or a failure code from messages-a-en.h */
M_Status
nobis_random(struct nobis *nb, M_ByteBlock *bb);

/**
 * Generate a random integer between zero and one less than the limit
 * provided using the hardware random number generator on an HSM.
 *
 * @param nb nCore abstraction
 * @param limit one greater than the maximum allowable result
 * @param value location to store result
 * @return Status_OK or a failure code from messages-a-en.h */
M_Status
nobis_randint(struct nobis *nb, unsigned limit, unsigned *value);

/**
 * Reallocate memory using an NFast_AppHandle.
 *
 * @param nb nCore abstraction
 * @param ptr previously allocated pointer
 * @param size desired number of bytes
 * @return allocated bytes or NULL if size is zero or no memory */
void *
nobis__realloc(struct nobis *nb, const char *file, unsigned line,
               void *ptr, unsigned size);
#define nobis_realloc(nb, ptr, size) \
  nobis__realloc((nb), __FILE__, __LINE__, (ptr), (size))

/**
 * Allocate memory using an NFast_AppHandle.
 *
 * @param nb nCore abstraction
 * @param size desired number of bytes
 * @return allocated bytes or NULL if no memory */
void *
nobis__malloc(struct nobis *nb, const char *file, unsigned line,
              unsigned size);
#define nobis_malloc(nb, size) \
  nobis__malloc((nb), __FILE__, __LINE__, (size))

/**
 * Free memory using an NFast_AppHandle.
 *
 * @param nb nCore abstraction
 * @param ptr previously allocated pointer */
void
nobis_free(struct nobis *nb, void *ptr);

/**
 * Append the contents of one byte block to another.  This routine
 * reallocates memory, so the bb parameter must have been alloced
 * by nobis_malloc or similar and must be reclaimed by the caller,
 * for example using nobis_free().
 *
 * @param nb nCore abstraction
 * @param bb byte block to append to
 * @param data contents appended to bb
 * @return Status_OK or a failure code from messages-a-en.h */
M_Status
nobis_bb_append(struct nobis *nb, M_ByteBlock *bb,
                const M_ByteBlock *data);

/**
 * Establish a context for use with NF_Free_* functions.  The final
 * parameter is also the return value.
 *
 * @param nb nCore abstraction
 * @param fctx context to populate
 * @return supplied context pointer */
NF_Free_Context *
nobis_setctx_free(struct nobis *nb, NF_Free_Context *fctx);

/**
 * Establish a context for use with NF_Marshal_* functions.  The final
 * parameter is also the return value.
 *
 * @param nb nCore abstraction
 * @param ptr bytes in which to store marshalled structures
 * @param len number of bytes available at ptr
 * @param mctx context to populate
 * @return supplied context pointer */
NF_Marshal_Context *
nobis_setctx_marsh(struct nobis *nb, unsigned char *ptr, unsigned len,
                     NF_Marshal_Context *mctx);

/**
 * Establish a context for use with NF_Unmarshal_* functions.  The
 * final parameter is also the return value.
 *
 * @param nb nCore abstraction
 * @param ptr bytes from which to retrieve marshalled structures
 * @param len number of bytes available at ptr
 * @param uctx context to populate
 * @return supplied context pointer */
NF_Unmarshal_Context *
nobis_setctx_unmar(struct nobis *nb, const unsigned char *ptr,
                     unsigned len, NF_Unmarshal_Context *uctx);

/**
 * Establish a context for use with NF_Marshal_* functions.  The final
 * parameter is also the return value.
 *
 * @param nb nCore abstraction
 * @param bb bytes to which to store marshalled structures
 * @param mctx context to populate
 * @return supplied context pointer */
NF_Marshal_Context *
nobis_setctx_marbb(struct nobis *nb, M_ByteBlock *bb,
                   NF_Marshal_Context *mctx);

/**
 * Establish a context for use with NF_Unmarshal_* functions.  The
 * final parameter is also the return value.
 *
 * @param nb nCore abstraction
 * @param bb bytes from which to retrieve marshalled structures
 * @param uctx context to populate
 * @return supplied context pointer */
NF_Unmarshal_Context *
nobis_setctx_unmarbb(struct nobis *nb, const M_ByteBlock *bb,
                     NF_Unmarshal_Context *uctx);

/**
 * Establish a context for use with NF_Print_* functions.  The final
 * parameter is also the return value.
 *
 * @param nb nCore abstraction
 * @param f file to which printed values are sent
 * @param pctx context to populate
 * @return supplied context pointer */
NF_Print_Context *
nobis_setctx_print(struct nobis *nb, FILE *f, NF_Print_Context *pctx);

/**
 * Create a function that marshals a messages type using dynamically
 * allocated memory.  On successful return the pointer of the
 * destination byte block must be passed to nobis_free() when no
 * longer needed.
 *
 * @param nb nCore abstraction
 * @param dest location to store allocated bytes
 * @param arg_##type structure to pack
 * @return supplied context pointer */
#define NOBIS_MARSHAL_FUNCTION(type)                                   \
  static M_Status                                                      \
  nobis_marshal_##type(struct nobis *nb, M_ByteBlock *dest,            \
                       const M_##type *arg_##type)                     \
  {                                                                    \
    M_Status result;                                                   \
    NF_Marshal_Context mctx;                                           \
    nobis_setctx_marsh(nb, NULL, INT_MAX, &mctx);                      \
    if (!(result = NOBIS_CHK_STATUS                                    \
          (nb, NF_Marshal_##type, (&mctx, arg_##type)))) {             \
      unsigned size = INT_MAX - mctx.remain;                           \
      unsigned char *bytes = nobis_malloc(nb, size);                   \
      if (bytes) {                                                     \
        nobis_setctx_marsh(nb, bytes, size, &mctx);                    \
        if (!(result = NOBIS_CHK_STATUS                                \
              (nb, NF_Marshal_##type, (&mctx, arg_##type)))) {         \
          dest->ptr = bytes;                                           \
          dest->len = size;                                            \
        } else nobis_free(nb, bytes);                                  \
      } else result = Status_OSErrorErrno;                             \
    }                                                                  \
    return result;                                                     \
  }

/**
 * Create a function that prints a messages type.
 *
 * @param nb nCore abstraction
 * @param stream file to print to
 * @param indent number of characters to skipt at start of line
 * @param arg_##type structure to print */
#define NOBIS_PRINT_FUNCTION(type)                                     \
  static void                                                          \
  nobis_print_##type(struct nobis *nb, FILE *stream, M_Word indent,    \
                     const M_##type *arg_##type)                       \
  {                                                                    \
    NF_Print_Context ctx;                                              \
    nobis_setctx_print((nb), stream ? stream : NOBIS_TRACE_FILE, &ctx);          \
    NF_Print_##type(&ctx, indent, arg_##type);                         \
  }

/**
 * Convert a block of binary bytes into a hexidecimal encoded string.
 * The string is allocated and must be reclaimed with nobis_free() when
 * no longer needed.
 *
 * @param nb nCore abstraction
 * @param flags settings for conversion
 * @param limit maximum number of converted bytes to write
 * @param src characters from which to read
 * @return allocated string representing hex encoded src */
enum {
  nobis_hex_lean = (1 << 2) /* no extra characters */,
  nobis_hex_lines = (1 << 3) /* include newlines every 32 bytes */,
};
char *
nobis_hexify(struct nobis *nb, unsigned flags, unsigned limit,
             const M_ByteBlock *src);

/**
 * Convert a hexidecimal encoded string into a block of binary bytes.
 * No more than limit bytes will be written, though if there are more
 * valid characters in src Status_BufferFull is returned.  If the
 * filled parameter is non-NULL it is set to the number of bytes which
 * have been written.
 *
 * @param nb nCore abstraction
 * @param flags settings for conversion
 * @param dest location in which to write converted bytes
 * @param limit maximum number of converted bytes to write
 * @param optional filled location to store number of bytes written
 * @param src characters from which to read
 * @return Status_OK or a failure code from messages-a-en.h */
enum {
  nobis_uhex_quiet  = (1 << 0) /* errors are not printed */,
  nobis_uhex_ignore = (1 << 1) /* invalid characters skipped */,
};
M_Status
nobis_unhexify(struct nobis *nb, unsigned flags, unsigned char *dest,
               unsigned limit, unsigned *filled, const char *src);

/**
 * Convert a hexidecimal encoded string to an M_ByteBlock.  This
 * function allocates data on success so the caller must reclaim the
 * memory when no longer needed (for example, using nobis_freebb).
 *
 * @param nb nCore abstraction
 * @param flags settings for conversion
 * @param bb destination byte block to allocate and fill
 * @param hexvalue characters from which to read
 * @return Status_OK or a failure code from messages-a-en.h */
M_Status
nobis_unhexbb(struct nobis *nb, unsigned flags,
              M_ByteBlock *bb, const char *hexvalue);

/**
 * Convert a hexidecimal encoded string to an M_Bignum.  This function
 * allocates data on success so the caller must reclaim the memory
 * when no longer needed (for example, using nobis_bignum_cleanup).
 *
 * @param nb nCore abstraction
 * @param flags settings for conversion
 * @param bb destination byte block to allocate and fill
 * @param hexvalue characters from which to read
 * @return Status_OK or a failure code from messages-a-en.h */
M_Status
nobis_unhexbignum(struct nobis *nb, unsigned flags,
                  M_Bignum *bn, const char *hexvalue);

/* ------------------------------------------------------------------ */
/* The following functions are useful for reporting errors in a
 * functional style that makes code more straightforward.  NULL is
 * explicitly permitted for the value of (struct nobis *) in these
 * functions.  At present they send output to the standard error
 * stream but in the future might do something more sophisticated. */

/**
 * Write a formatted failure message.
 *
 * @param nb nCore abstraction
 * @param context describes what failed (usually __func__)
 * @param status value to describe and to return
 * @param message formatted string to describe failure
 * @return Status_OK or a failure code from messages-a-en.h */
M_Status
nobis_fail_vmsg(struct nobis *nb, const char *context, M_Status status,
                const char *message, va_list args);
M_Status
nobis_fail_msg(struct nobis *nb, const char *context, M_Status status,
               const char *message, ...);

#define nobis_failmsg(nb, rc, message, ...) \
  nobis_fail_msg(nb, __func__, rc, message, ## __VA_ARGS__)

/**
 * Print a failure message iff status is not Status_OK.
 *
 * @param nb nCore abstraction
 * @param context name of function to check
 * @param status result of operation to check
 * @param fn name of function called as part of operation
 * @return value of status */
M_Status
nobis_check_status(struct nobis *nb, const char *file, unsigned line,
                   M_Status status, const char *fn);
#define NOBIS_CHK_STATUS(nb, fn, args) \
  nobis_check_status(nb, __FILE__, __LINE__, fn args, #fn)
#define NOBIS_CHK_TRANS(nb, trans, fn, args) \
  nobis_check_status(nb, __FILE__, __LINE__, trans(fn args), #fn)

/**
 * Print a failure message using strerror(3) iff a result is less than
 * zero.
 *
 * @param nb nCore abstraction
 * @param context name of function to check
 * @param result return value of a system process
 * @param fn name of function called as part of operation
 * @return value of result */
int
nobis_check_errno(struct nobis *nb, const char *context,
                  int result, const char *fn);
#define NOBIS_CHK_ERRNO(nb, fn, args) \
  nobis_check_errno(nb, __func__, fn args, #fn)

/**
 * Print a failure message iff a pointer is NULL.
 *
 * @param nb nCore abstraction
 * @param ptr result of operation
 * @param fn name of function called as part of operation
 * @param message formated message to print on failure
 * @return value of ptr */
void *
nobis_check_null(struct nobis *nb, void *ptr, const char *fn,
                 const char *message, ...);
#ifdef __GNUC__
/* Some older versions of GCC don't support leaving out variadic
 * macro arguments unless the ## operator is used. */
# define NOBIS_CHK_NULLFN(nb, fn, args, message, ...) \
  nobis_check_null(nb, fn args, #fn, message, ## __VA_ARGS__)
# define NOBIS_CHK_NULL(nb, ptr, fn, message, ...) \
  nobis_check_null(nb, ptr, #fn, message, ## __VA_ARGS__)
#else
# define NOBIS_CHK_NULLFN(nb, fn, args, message, ...) \
  nobis_check_null(nb, fn args, #fn, message, __VA_ARGS__)
# define NOBIS_CHK_NULL(nb, ptr, fn, message, ...) \
  nobis_check_null(nb, ptr, #fn, message, __VA_ARGS__)
#endif

/**
 * Reverse a byte block in increments of span.  Setting span to one
 * simple inverts the order of the bytes.  Setting it to four swaps
 * thirty-two bit words.  The byte block is modified in place but also
 * returned for use in larger expressions.
 *
 * @param nb nCore abstraction
 * @param span number of bytes to move in each reverse operation
 * @param bb bytes to reverse
 * @return value of bb */
M_ByteBlock *
nobis_reversebb(struct nobis *nb, unsigned span, M_ByteBlock *bb);

/**
 * Display an arbitrary precision integer in big endian format.  If
 * limit is non-zero no more than that many characters will be
 * printed.
 *
 * @param nb nCore abstraction
 * @param bn bignum to print
 * @param limit maximum number of bytes to print (unlimited if zero)
 * @param message formatted message to preceed bytes
 * @return byte block that was printed */
const M_Bignum *
nobis_printbn(struct nobis *nb, const M_Bignum *bn, unsigned limit,
              const char *message, ...);

/**
 * Display a formatted byte block.  If limit is non-zero no more than
 * that many characters will be printed.
 *
 * @param nb nCore abstraction
 * @param bb byte block to print
 * @param limit maximum number of bytes to print (unlimited if zero)
 * @param message formatted message to preceed bytes
 * @return byte block that was printed */
const M_ByteBlock *
nobis_printbb(struct nobis *nb, const M_ByteBlock *bb, unsigned limit,
              const char *message, ...);

/**
 * Display a key hash.
 *
 * @param nb nCore abstraction
 * @param kh key hash to print
 * @param message formatted message to preceed bytes
 * @return key hash that was printed */
const M_KeyHash *
nobis_printkh(struct nobis *nb, const M_KeyHash *kh,
              const char *message, ...);

/**
 * Display a collection of bytes in hexidecimal format.
 *
 * @param nb nCore abstraction
 * @param bytes bytes to print
 * @param length number of bytes to print
 * @param out stream to write to
 * @param message formatted message to preceed bytes
 * @return bytes */
const unsigned char *
nobis_print_bytes(struct nobis *nb, const unsigned char *bytes,
                  unsigned length, const char *message, ...);

enum {
  nobis_printki_acl = (1 << 0),
};

/**
 * Display basic information about a key.
 *
 * @param nb nCore abstraction
 * @param k key to be queried
 * @param message formatted message to preceed key
 * @return Status_OK or a failure code from messages-a-en.h */
M_Status
nobis_print_keyinfo(struct nobis *nb, M_KeyID k, unsigned flags,
                    const char *message, ...);

/**
 * Calclulate the parity of a byte.
 *
 * @param b byte to check
 * @return non-zero iff b has odd parity */
int
nobis_parity_isodd(unsigned char b);

/**
 * Alter the final bit of each byte if necessary for odd parity.
 *
 * @param bb byte block to modify */
void
nobis_parity_setodd(M_ByteBlock* bb);

/* get the Mech Hash based on the Mech */
M_Mech nobis_getHashMech( M_Mech mech);

#ifdef __cplusplus
}
#endif
#endif /* NOBIS_H */
