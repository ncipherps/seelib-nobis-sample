/* sample-marshal.h
 * Copyright (C) 2015-2018 nCipher Security, LLC.  All rights reserved.
 * ---------------------------------------------------------------------
 * M_Sample* records and function prototoypes
 * Used to marshal/unmarshal/free Command/Reply data for SEEJobs to/from
 * the Sample SEE Machine.
 * ------------------------------------------------------------------ */
#ifndef SAMPLE_MARSHAL_H
#define SAMPLE_MARSHAL_H

/* trace messages always emitted */
#define TRACE(x, ...) fprintf(NOBIS_TRACE_FILE, (x), ##__VA_ARGS__)

/* DBGMSG by default not emitted */
#ifdef MACHINE_DEBUG
#define DBGMSG(x, ...) fprintf(NOBIS_TRACE_FILE, (x), ##__VA_ARGS__)
#else
#define DBGMSG(x, ...)
#endif

/* This is close to the larget job possible.  This size allows
 * the EchoBlock job to include a 255KiB buffere in the Cmd and Reply
 * Note:  due to possible perforamcne issues with JobFragementation,
 * it is often good to limit job payload to 7KiB so the entire M_Command
 * and M_Reply is less than 8KiB; also for impath it is good to stay
 * well short of 8KiB to allow additional impath overhead
 */
#define JOB_LIMIT 1024*255+20

/* SEE Operation/Task identifiers */
typedef enum M_SampleOp {
  SampleOp_ErrorReturn        = 0,
  SampleOp_MachineVersion     = 1,
  SampleOp_NoOp               = 2,
  SampleOp_FIPSandInit        = 3,
  SampleOp_EchoBlock          = 4,
  SampleOp_PushBlock          = 5,
  SampleOp_PullBlock          = 6,
  SampleOp_PushPullBlock      = 7,
  SampleOp_NoOpTransact       = 8,
} M_SampleOp;

typedef enum M_SampleRep {
  SampleRep_Success           = 0,
  SampleRep_InitFailed        = 1,
  SampleRep_nCoreFailed       = 2,
  SampleRep_FIPSFailed        = 3,
  SampleRep_MarshalFailed     = 4,
  SampleRep_UnmarshalFailed   = 5,
  SampleRep_UnknownCommand    = 6,
  SampleRep_NotYetImplemented = 7,
  SampleRep_CoreRTInitFailed  = 8,
} M_SampleRep;

/* Records for SEE Commands */
typedef struct M_SampleJobCmd M_SampleJobCmd;
typedef union M_SampleOp__Args M_SampleOp__Args;

typedef struct M_SampleOp_FIPSandInit_Args M_SampleOp_FIPSandInit_Args;
struct M_SampleOp_FIPSandInit_Args {
  M_ByteBlock packed;
};

typedef struct M_SampleOp_EchoBlock_Args M_SampleOp_EchoBlock_Args;
struct M_SampleOp_EchoBlock_Args {
  M_ByteBlock block;
};

typedef struct M_SampleOp_PushBlock_Args M_SampleOp_PushBlock_Args;
struct M_SampleOp_PushBlock_Args {
  M_ByteBlock block;
};

typedef struct M_SampleOp_PullBlock_Args M_SampleOp_PullBlock_Args;
struct M_SampleOp_PullBlock_Args {
  M_Word size;
};

typedef struct M_SampleOp_PushPullBlock_Args M_SampleOp_PushPullBlock_Args;
struct M_SampleOp_PushPullBlock_Args {
  M_ByteBlock block;
  M_Word size;
};

typedef struct M_SampleOp_NoOpTransact_Args M_SampleOp_NoOpTransact_Args;
struct M_SampleOp_NoOpTransact_Args {
  M_Word count;
};

union M_SampleOp__Args {
  M_SampleOp_FIPSandInit_Args fips;
  M_SampleOp_EchoBlock_Args echoblock;
  M_SampleOp_PushBlock_Args pushblock;
  M_SampleOp_PullBlock_Args pullblock;
  M_SampleOp_PushPullBlock_Args pushpullblock;
  M_SampleOp_NoOpTransact_Args nooptransact;
};

struct M_SampleJobCmd {
  M_SampleOp op;
  union M_SampleOp__Args args;
};

/* Records for SEE Replies */
typedef struct M_SampleJobReply M_SampleJobReply;
typedef union M_SampleOp__Reply M_SampleOp__Reply;

typedef struct M_SampleOp_MachineVersion_Reply M_SampleOp_MachineVersion_Reply;
struct M_SampleOp_MachineVersion_Reply {
    M_Word major;
    M_Word minor;
    M_Word patch;
};

typedef struct M_SampleOp_EchoBlock_Reply M_SampleOp_EchoBlock_Reply;
struct M_SampleOp_EchoBlock_Reply {
  M_ByteBlock block;
};

typedef struct M_SampleOp_PullBlock_Reply M_SampleOp_PullBlock_Reply;
struct M_SampleOp_PullBlock_Reply {
  M_ByteBlock block;
};

typedef struct M_SampleOp_PushPullBlock_Reply M_SampleOp_PushPullBlock_Reply;
struct M_SampleOp_PushPullBlock_Reply {
  M_ByteBlock block;
};

union M_SampleOp__Reply {
    M_SampleOp_MachineVersion_Reply machineversion;
    M_SampleOp_EchoBlock_Reply echoblock;
    M_SampleOp_PullBlock_Reply pullblock;
    M_SampleOp_PushPullBlock_Reply pushpullblock;
};

struct M_SampleJobReply {
  M_SampleRep rep;
  M_SampleOp op;
  M_Status intstat;
  union M_SampleOp__Reply reply;
};

/* User data record */
#define UD_MAGICHEADER  0x00000000 // should be set to project specific value
#define UD_CURRENTVER   0
typedef struct M_SampleUserData M_SampleUserData;
struct M_SampleUserData {
  M_Word      magic;
  M_Word      ver;
  M_Hash      hkm;
  M_Hash      hkmwk;
};

/* public marshal/unmarshal/free functions */
extern int SampleJob_MarshalCmd(struct nobis *nb,
        const M_SampleJobCmd *pCmd, unsigned char *buf, int *len_io);

extern int SampleJob_MarshalReply(struct nobis *nb,
        const M_SampleJobReply *pReply, unsigned char *buf, int *len_io);

extern int SampleJob_UnmarshalCmd(struct nobis *nb,
        const unsigned char *in, int in_len, M_SampleJobCmd *pCmd);

extern int SampleJob_UnmarshalReply(struct nobis *nb,
        const unsigned char *in, int in_len, M_SampleJobReply *pReply);

extern void SampleJob_FreeCmd(struct nobis *nb, M_SampleJobCmd *pCmd);

extern void SampleJob_FreeReply(struct nobis *nb, M_SampleJobReply *pReply);

extern int SampleUserData_Marshal(struct nobis *nb,
        const M_SampleUserData *pData, unsigned char *buf, M_Word *len_io);
extern int SampleUserData_Unmarshal(struct nobis *nb,
        const unsigned char *in, int in_len, M_SampleUserData *pData);
extern void SampleUserData_Free(struct nobis *nb, M_SampleUserData *pData);
extern void SampleUserData_Print(struct nobis *nb, FILE *stream,
        M_Word indent, const M_SampleUserData *pData);

extern const char * SampleStr_Op(M_SampleOp job);
extern const char * SampleStr_Rep(M_SampleRep rep);

#endif
