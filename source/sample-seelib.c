/* sample-seelib.c
 * Copyright (C) 2015-2018 nCipher Security, LLC.  All rights reserved.
 * ---------------------------------------------------------------------
 * Main module for this SEE machine.  This does the required setup to
 * process and reply to SEE jobs.  Processing for most jobs is executed
 * by functions in the sample-shared.c module.
 * ------------------------------------------------------------------ */

#include <stdlib.h>
#include <string.h>
#include "nobis.h"
#include "sample-marshal.h"
#include "sample-core.h"

#define USE_PROCESSORTHREADS
#define PROCESS_STACKSIZE 16384

#define VER_MAJOR 0
#define VER_MINOR 1
#define VER_PATCH 1

typedef struct ProcessThreadCtx {
  struct nobis *nb;
  M_SampleUserData *ud;
} ProcessThreadCtx;

M_SampleRep initstatus;
struct nobis nb;
M_SampleUserData ud;
ProcessThreadCtx g_processThreadCtx;


#ifdef USE_PROCESSORTHREADS
// Multiple threads do not always increase performance for SEE machines
static struct ProcessThreadCtx *Process_Init( SEElib_ProcessContext *pC )
{
  if (pC->iobuf_maxlen < JOB_LIMIT) {
    pC->iobuf_maxlen = JOB_LIMIT;
    pC->iobuf = (unsigned char *)realloc(pC->iobuf, pC->iobuf_maxlen);
    if (pC->iobuf == NULL)
      abort();
  }
  return &g_processThreadCtx;
}
#endif

static int
initialize_system(struct nobis *nb, M_SampleUserData *ud, int datalen)
{
  int rc;
  unsigned char *buf = NULL;
  if ((buf = nobis_malloc(nb, datalen))) {
    if (!(rc = NOBIS_CHK_STATUS(nb, SEElib_ReadUserData, (0, buf, datalen)))) {
      rc = NOBIS_CHK_STATUS(nb, SampleUserData_Unmarshal,
                            (nb, buf, datalen, ud));
    }
  } else rc = NOBIS_STATUS_NOMEM;
  nobis_free(nb, buf);
  return rc;
}

static int Op_MachineVersion(M_SampleJobCmd *cmd, M_SampleJobReply *r, ProcessThreadCtx *uc)
{
  r->rep = SampleRep_Success;
  r->reply.machineversion.major = VER_MAJOR;
  r->reply.machineversion.minor = VER_MINOR;
  r->reply.machineversion.patch = VER_PATCH;
  return Status_OK;
}

static int Op_NoOp(M_SampleJobCmd *cmd, M_SampleJobReply *reply, ProcessThreadCtx *uc)
{
  return Status_OK;
}

static int Op_FIPSandInit(M_SampleJobCmd *cmd, M_SampleJobReply *r, ProcessThreadCtx *uc)
{
  if ((r->intstat = nobis_fips_unpack(uc->nb, 0, &cmd->args.fips.packed)))
    r->rep = SampleRep_FIPSFailed;
  else if ((r->intstat = sample_core_runtimeinit(uc->nb)))
    r->rep = SampleRep_CoreRTInitFailed;
  return Status_OK;
}

static int Op_EchoBlock(M_SampleJobCmd *cmd, M_SampleJobReply *r, ProcessThreadCtx *uc)
{
  r->intstat = SampleEchoBlock(uc->nb, &cmd->args.echoblock,
      &r->reply.echoblock, &r->rep);

  return Status_OK;
}

static int Op_PushBlock(M_SampleJobCmd *cmd, M_SampleJobReply *r, ProcessThreadCtx *uc)
{
  r->intstat = SamplePushBlock(uc->nb, &cmd->args.pushblock,
      &r->rep);

  return Status_OK;
}

static int Op_PullBlock(M_SampleJobCmd *cmd, M_SampleJobReply *r, ProcessThreadCtx *uc)
{
  r->intstat = SamplePullBlock(uc->nb, &cmd->args.pullblock,
      &r->reply.pullblock, &r->rep);

  return Status_OK;
}

static int Op_PushPullBlock(M_SampleJobCmd *cmd, M_SampleJobReply *r, ProcessThreadCtx *uc)
{
  r->intstat = SamplePushPullBlock(uc->nb, &cmd->args.pushpullblock,
      &r->reply.pushpullblock, &r->rep);

  return Status_OK;
}

static int Op_NoOpTransact(M_SampleJobCmd *cmd, M_SampleJobReply *r, ProcessThreadCtx *uc)
{
  r->intstat = SampleNoOpTransact(uc->nb, &cmd->args.nooptransact,
      &r->rep);

  return Status_OK;
}

static int Process_Fn( SEElib_ProcessContext *pC, M_Word tag, int in_len )
{
  int stat;
  M_SampleJobCmd cmd;
  M_SampleJobReply reply = {0};

  /* Check application initialization failure.  This allows the SEE
     application to startup but all jobs will get this error message. */
  if (initstatus == SampleRep_InitFailed) {
    TRACE("Applicaiton init failed; SEE app is non-operational\n");
    reply.rep = SampleRep_InitFailed;
    reply.op = SampleOp_ErrorReturn;
    in_len = pC->iobuf_maxlen;
    SampleJob_MarshalReply(&nb, &reply, pC->iobuf, &in_len);
    return in_len;
  }

  stat = SampleJob_UnmarshalCmd(&nb, pC->iobuf, in_len, &cmd);
  if (stat == Status_OK) {
    reply.op = cmd.op;
    switch (cmd.op){
    case SampleOp_MachineVersion:
      stat = Op_MachineVersion(&cmd, &reply, pC->uc);
      break;
    case SampleOp_NoOp:
      stat = Op_NoOp(&cmd, &reply, pC->uc);
      break;
    case SampleOp_FIPSandInit:
      stat = Op_FIPSandInit(&cmd, &reply, pC->uc);
      break;
    case SampleOp_EchoBlock:
      stat = Op_EchoBlock(&cmd, &reply, pC->uc);
      break;
    case SampleOp_PushBlock:
      stat = Op_PushBlock(&cmd, &reply, pC->uc);
      break;
    case SampleOp_PullBlock:
      stat = Op_PullBlock(&cmd, &reply, pC->uc);
      break;
    case SampleOp_PushPullBlock:
      stat = Op_PushPullBlock(&cmd, &reply, pC->uc);
      break;
    case SampleOp_NoOpTransact:
      stat = Op_NoOpTransact(&cmd, &reply, pC->uc);
      break;
    case SampleOp_ErrorReturn:
    default:
      reply.rep = SampleRep_UnknownCommand;
      reply.op = SampleOp_ErrorReturn;
      break;
    }
  } else {
    TRACE("SampleJob_UnmarshalCmd failure: %d\n", stat);
    reply.rep = SampleRep_UnmarshalFailed;
    reply.op = SampleOp_ErrorReturn;
    stat = Status_OK;
  }

  in_len = pC->iobuf_maxlen;
  if (stat != Status_OK)
    SampleJob_FreeReply(pC->uc->nb, &reply);
  else
    stat = SampleJob_MarshalReply(&nb, &reply, pC->iobuf, &in_len);
  if (stat != Status_OK) {
    reply.rep = SampleRep_MarshalFailed;
    reply.op = SampleOp_ErrorReturn;
    in_len = pC->iobuf_maxlen;
    stat = SampleJob_MarshalReply(&nb, &reply, pC->iobuf, &in_len);
  }

  SampleJob_FreeCmd(pC->uc->nb, &cmd);
  SampleJob_FreeReply(pC->uc->nb, &reply);
  return in_len;
}

int main ( void )
{
  int n;
  //JOB_LIMIT moved to sample-marshal.h

  SEElib_init();
  TRACE("Main code starts\n");
  TRACE("SEElib_init() completed\n");
  SEElib_StartTransactListener();

  n = SEElib_GetUserDataLen();
  if (n) {
    TRACE("Started with %d bytes of userdata\n", n);
    if (!nobis_setup(&nb, 0, "sample-seelib")) {
      if (initialize_system(&nb, &ud, n)) {
        TRACE("initialize_system() failed, SEE app is non-operational\n");
        initstatus = SampleRep_InitFailed;
      }
    } else {
      TRACE("nobis_setup() failed, exiting\n");
      SEElib_InitComplete(SampleRep_InitFailed);
      exit(SampleRep_InitFailed);
    }
    SEElib_ReleaseUserData();

    if (sample_core_init(&nb, 0)) {
      TRACE("sample_core_init() failed, SEE app is non-operational\n");
      initstatus = SampleRep_InitFailed;
    }
  } else {TRACE("Started without userdata\n"); SEElib_InitComplete(2); exit(2);}

  g_processThreadCtx.nb = &nb;
  g_processThreadCtx.ud = &ud;

#ifdef USE_PROCESSORTHREADS
  n = SEElib_RecProcessThreads();
  TRACE("Creating %d threads\n", n);
  if ( 0 != SEElib_StartProcessorThreads(n, PROCESS_STACKSIZE,
      Process_Init, Process_Fn) ) {
    TRACE(( "Thread startup failed, exiting\n" ));
    SEElib_InitComplete(SampleRep_InitFailed);
    exit(1);
  }
  SEElib_InitComplete(0);
#else
  SEElib_InitComplete(0);

  unsigned char* buffer = malloc(JOB_LIMIT);
  for (;;) {
    SEElib_ProcessContext ctx = {&g_processThreadCtx, buffer, JOB_LIMIT};
    M_Word length = JOB_LIMIT;
    M_Word tag;
    if (!NOBIS_CHK_STATUS(&nb, SEElib_AwaitJobEx,
        (&tag, buffer, &length, 0))) {
      if (length > 0) {
        length = Process_Fn(&ctx, tag, length);
      }
      SEElib_ReturnJob(tag, buffer, length);
    }
  }
  free(buffer);
  sample_core_cleanup(&nb);
  SampleUserData_Free(&nb, &ud);
  nobis_cleanup(&nb);
#endif
  TRACE("Main thread about to exit\n");
  ExitThread(0);
  return 0;
}
