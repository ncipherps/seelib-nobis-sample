import nfpython as nf
import perf_common as pc
import perf

class test_derive_key(perf.PerfTest):
  description = 'NISTKDF CTR AES CMAC'
  name = 'derive'
  # unit = '1k blocks'
  cases = [ () ]
  def __init__(self):
    pass
  def start_test(self, c):
    kgp = nf.KeyGenParams(['Rijndael', 32])
    acl = pc.opacl('Encrypt','Decrypt','Sign','Verify')
    tkd = nf.KeyData(['DKTemplate', nf.AppData(), nf.marshal(acl)])
    key = pc.generatekey(ctx, self.kgp, pc.opacl('Encrypt','Decrypt'))
    
    context = nf.ByteBlock('context', fromraw=True)
    label = nf.ByteBlock('label', fromraw=True)
    self.cmd = nf.Command(['DeriveKey', 0, 'NISTKDFmCTRpRijndaelCMACr32',
        [tdk, key], 0, 256, 'Rijndael', context, label])

  def req_reps(self): return 1024 * 16 # 16MB
  discard_reply = True

tests = perf.find_tests('perf_derive', 'Derive', 'derive')
