# Copyright (C) 2014 nCipher Security, LLC.  All rights reserved.
# This example source code is provided for your information and assistance.
# Note that there is NO WARRANTY.

import nfpython as nf
import optparse

op = optparse.OptionParser(version="%prog 1.0")
op.add_option('-p','--pubobj', dest='pubname',
              metavar='PUBOJB', help='Published object for SEE application.')
opts, args=op.parse_args()

if opts.pubname is None:
  op.error('Publisehd object name must be specified.')

pubname=opts.pubname

conn = nf.connection(needworldinfo=False)
cmd = nf.Command(['NewEnquiry', 0, 0, 1, 0])
modulecount = conn.transact(cmd).reply.module
for i in range(modulecount):
  mod = i + 1
  try:
    # call hardserver to get the published object and on success,
    # also confirm it contains an object; could contain data
    cmd = nf.Command(['GetPublishedObject', 0, 0, pubname, mod])
    rep = conn.transact(cmd).reply
    print(rep)
    if rep.flags.isset('object_present'):
      w = rep.object
    else:
      print("FAIL: Module #%d: pubobj '%s' '%s'" %
            (mod, pubname, "pub name exist but does not contain an object"))
      continue
  except nf.NFStatusError as e:
    # handle exception from GetPublishedObject
    if e.status == 'NotAvailable':
      print("FAIL: Module #%d: pubobj '%s' '%s'" % (mod, pubname, e.status))
      continue
    else:
      raise
  
  # we have a publihsed object; now use GetWhichModule to confirm the
  # M_KeyID is still considered valid by the hardserver
  cmd = nf.Command(['GetWhichModule', 0, w])
  r = conn.transact(cmd, ignorestatus=True)
  if r.status == 'OK':
    print("Module #%d: %s %s" % (mod, r.cmd, r.status))
  else:
    print("FAIL: %s" % repr(r))

  # Confirm that GetKeyInfo returns InvalidParameter.  This is confirming
  # that the M_KeyID is not for a "key" object.  We are assuming if it
  # is not a key then it is a SEEWorld.  However, it could be a buffer,
  # channel, or logical token.
  # TODO:  confirm !buffer, !channel, !logicaltoken.
  cmd = nf.Command(['GetKeyInfo', 0, w])
  r = conn.transact(cmd, ignorestatus=True)
  if r.status == 'InvalidParameter':
    print("Module #%d: %s %s" % (mod, cmd.cmd, "expected value"))
  else:
    print("FAIL: Module #%d: %s" % (mod, repr(r)))
