/* sample-marshal.c
 * Copyright (C) 2015-2018 nCipher Security, LLC.  All rights reserved.
 * ---------------------------------------------------------------------
 * Manually generated marshal/unmarshal/free functions for M_Sample* records
 * Used to marshal/unmarshal/free Command/Reply data for SEEJobs to/from
 * the Sample SEE Machine.
 * ------------------------------------------------------------------ */

#include <string.h>
#include "nobis.h"
#include "sample-marshal.h"

static int Marshal_SampleOp(NF_Marshal_Context *c, const M_SampleOp *msg)
{
  M_Word w = *msg;
  return NF_Marshal_Word(c, &w);
}

static int Unmarshal_SampleOp(NF_Unmarshal_Context *c, M_SampleOp *msg)
{
  int rc;
  M_Word w;
  rc = NF_Unmarshal_Word(c, &w);
  if (rc == Status_OK)
    *msg = (M_SampleOp)w;
  return rc;
}

static int Marshal_SampleRep(NF_Marshal_Context *c, const M_SampleRep *msg)
{
  M_Word w = *msg;
  return NF_Marshal_Word(c, &w);
}

static int Unmarshal_SampleRep(NF_Unmarshal_Context *c, M_SampleRep *msg)
{
  int rc;
  M_Word w;
  rc = NF_Unmarshal_Word(c, &w);
  if (rc == Status_OK)
    *msg = (M_SampleOp)w;
  return rc;
}

/* NF_Marshal_vec_Word is not exported, created sample of generated msgs code */
/* static int NF_Marshal_vec_Word(NF_Marshal_Context *c, const M_vec_Word msg, int n)
{
  M_Word w = n;
  int i, rc = 0;

  if ((rc = NF_Marshal_Word(c, &w)) != 0)
    return (rc);
  for (i = 0; i < n; i++) {
    if ((rc = NF_Marshal_Word(c, &msg[i])) != 0)
      break;
  }
  return (rc);
} */

/* NF_Unmarshal_vec_Word is not exported, created sample of generated msgs code */
/* static int NF_Unmarshal_vec_Word(NF_Unmarshal_Context *c, M_vec_Word *msg, int *nn)
{
  size_t i;
  int rc = 0;
  M_vec_Word v;
  M_Word n;

  if ((rc = NF_Unmarshal_Word(c, &n)) != 0 || !n)
    return (rc);
  if (n > INT_MAX / sizeof(v[0]))
    return (NFErr_TableTooLarge("<unknown-type>", "<unknown-member>", "Word", n, INT_MAX / sizeof(v[0]), c->u));

  if ((v = (M_vec_Word)NF_Malloc(n * sizeof(v[0]), c->u)) == 0)
    return (MARSHERR_MALLOCFAIL);
  *msg = v;
  for (i = 0; i < n; i++) {
    if ((rc = NF_Unmarshal_Word(c, &v[i])) != 0)
      break;
  }
  *nn = (int)i;
  return (rc);
} */

/* NF_Free_vec_Word is not exported, created sample of generated msgs code */
/* static int NF_Free_vec_Word(NF_Free_Context *c, M_vec_Word *msg, int *n)
{
  M_vec_Word v = *msg;
  int rc = 0;

  if (!*n)
    return (0);
  if (!rc && (rc = NF_Free(v, c->u)) == 0)
    *msg = 0;
  return (rc);
} */

static int Marshal_SampleJobCmd(NF_Marshal_Context* c, const M_SampleJobCmd *pCmd)
{
  int rc;
  rc = Marshal_SampleOp(c, &pCmd->op);
  if (rc) return rc;

  switch (pCmd->op){
  case SampleOp_MachineVersion:
    break;
  case SampleOp_NoOp:
    break;
  case SampleOp_FIPSandInit:
    (rc=NF_Marshal_ByteBlock(c, &pCmd->args.fips.packed));
    break;
  case SampleOp_EchoBlock:
    (rc=NF_Marshal_ByteBlock(c, &pCmd->args.echoblock.block));
    break;
  case SampleOp_PushBlock:
    (rc=NF_Marshal_ByteBlock(c, &pCmd->args.pushblock.block));
    break;
  case SampleOp_PullBlock:
    (rc=NF_Marshal_Word(c, &pCmd->args.pullblock.size));
    break;
  case SampleOp_PushPullBlock:
    (void)((rc=NF_Marshal_ByteBlock(c, &pCmd->args.pushpullblock.block))
    ||(rc=NF_Marshal_Word(c, &pCmd->args.pushpullblock.size)));
    break;
  case SampleOp_NoOpTransact:
    (rc=NF_Marshal_Word(c, &pCmd->args.nooptransact.count));
    break;
  case SampleOp_ErrorReturn:
    break;
  }

  return rc;
}

static int Unmarshal_SampleJobCmd(NF_Unmarshal_Context* c, M_SampleJobCmd *pCmd)
{
  int rc;
  rc = Unmarshal_SampleOp(c, &pCmd->op);
  if (rc) return rc;

  switch (pCmd->op){
  case SampleOp_MachineVersion:
    break;
  case SampleOp_NoOp:
    break;
  case SampleOp_FIPSandInit:
    (rc=NF_Unmarshal_ByteBlock(c, &pCmd->args.fips.packed));
    break;
  case SampleOp_EchoBlock:
    (rc=NF_Unmarshal_ByteBlock(c, &pCmd->args.echoblock.block));
    break;
  case SampleOp_PushBlock:
    (rc=NF_Unmarshal_ByteBlock(c, &pCmd->args.pushblock.block));
    break;
  case SampleOp_PullBlock:
    (rc=NF_Unmarshal_Word(c, &pCmd->args.pullblock.size));
    break;
  case SampleOp_PushPullBlock:
    (void)((rc=NF_Unmarshal_ByteBlock(c, &pCmd->args.pushpullblock.block))
    ||(rc=NF_Unmarshal_Word(c, &pCmd->args.pushpullblock.size)));
    break;
  case SampleOp_NoOpTransact:
    (rc=NF_Unmarshal_Word(c, &pCmd->args.nooptransact.count));
    break;
  case SampleOp_ErrorReturn:
    break;
  }

  return rc;
}

static int Free_SampleJobCmd(NF_Free_Context *c, M_SampleJobCmd *pCmd)
{
  int rc = Status_OK;

  (void)c;
  switch (pCmd->op){
  case SampleOp_MachineVersion:
    break;
  case SampleOp_NoOp:
    break;
  case SampleOp_FIPSandInit:
    (rc=NF_Free_ByteBlock(c, &pCmd->args.fips.packed));
    break;
  case SampleOp_EchoBlock:
    (rc=NF_Free_ByteBlock(c, &pCmd->args.echoblock.block));
    break;
  case SampleOp_PushBlock:
    (rc=NF_Free_ByteBlock(c, &pCmd->args.pushblock.block));
    break;
  case SampleOp_PullBlock:
    break;
  case SampleOp_PushPullBlock:
    (rc=NF_Free_ByteBlock(c, &pCmd->args.pushpullblock.block));
    break;
  case SampleOp_NoOpTransact:
    break;
  case SampleOp_ErrorReturn:
    break;
  }
  return rc;
}

static int Marshal_SampleJobReply(NF_Marshal_Context* c, const M_SampleJobReply *pReply)
{
  int rc;
  (void)((rc=Marshal_SampleRep(c, &pReply->rep))
  || (rc=Marshal_SampleOp(c, &pReply->op))
  || (rc=NF_Marshal_Status(c, &pReply->intstat)));
  if (rc) return rc;

  if (pReply->rep) return 0;

  switch (pReply->op){
  case SampleOp_MachineVersion:
    (void)((rc=NF_Marshal_Word(c, &pReply->reply.machineversion.major))
    || (rc=NF_Marshal_Word(c, &pReply->reply.machineversion.minor))
    || (rc=NF_Marshal_Word(c, &pReply->reply.machineversion.patch)));
    break;
  case SampleOp_NoOp:
    break;
  case SampleOp_FIPSandInit:
    break;
  case SampleOp_EchoBlock:
    (rc=NF_Marshal_ByteBlock(c, &pReply->reply.echoblock.block));
    break;
  case SampleOp_PushBlock:
    break;
  case SampleOp_PullBlock:
    (rc=NF_Marshal_ByteBlock(c, &pReply->reply.pullblock.block));
    break;
  case SampleOp_PushPullBlock:
    (rc=NF_Marshal_ByteBlock(c, &pReply->reply.pullblock.block));
    break;
  case SampleOp_NoOpTransact:
    break;
  case SampleOp_ErrorReturn:
    break;
  }

  return rc;
}

static int Unmarshal_SampleJobReply(NF_Unmarshal_Context* c, M_SampleJobReply *pReply)
{
  int rc;
  (void)((rc=Unmarshal_SampleRep(c, &pReply->rep))
  || (rc=Unmarshal_SampleOp(c, &pReply->op))
  || (rc=NF_Unmarshal_Status(c, &pReply->intstat)));
  if (rc) return rc;

  if (pReply->rep) return 0;

  switch (pReply->op){
  case SampleOp_MachineVersion:
    (void)((rc = NF_Unmarshal_Word(c, &pReply->reply.machineversion.major))
    || (rc = NF_Unmarshal_Word(c, &pReply->reply.machineversion.minor))
    || (rc = NF_Unmarshal_Word(c, &pReply->reply.machineversion.patch)));
    break;
  case SampleOp_NoOp:
    break;
  case SampleOp_FIPSandInit:
    break;
  case SampleOp_EchoBlock:
    (rc=NF_Unmarshal_ByteBlock(c, &pReply->reply.echoblock.block));
    break;
  case SampleOp_PushBlock:
    break;
  case SampleOp_PullBlock:
    (rc=NF_Unmarshal_ByteBlock(c, &pReply->reply.pullblock.block));
    break;
  case SampleOp_PushPullBlock:
    (rc=NF_Unmarshal_ByteBlock(c, &pReply->reply.pushpullblock.block));
    break;
  case SampleOp_NoOpTransact:
    break;
  case SampleOp_ErrorReturn:
    break;
  }

  return rc;
}

static int Free_SampleJobReply(NF_Free_Context *c, M_SampleJobReply *pReply)
{
  int rc = Status_OK;

  (void)c;
  switch (pReply->op){
  case SampleOp_MachineVersion:
    break;
  case SampleOp_NoOp:
    break;
  case SampleOp_FIPSandInit:
    break;
  case SampleOp_EchoBlock:
    (rc=NF_Free_ByteBlock(c, &pReply->reply.echoblock.block));
    break;
  case SampleOp_PushBlock:
    break;
  case SampleOp_PullBlock:
    (rc=NF_Free_ByteBlock(c, &pReply->reply.pullblock.block));
    break;
  case SampleOp_PushPullBlock:
    (rc=NF_Free_ByteBlock(c, &pReply->reply.pushpullblock.block));
    break;
  case SampleOp_NoOpTransact:
    break;
  case SampleOp_ErrorReturn:
    break;
  }

  return rc;
}

int SampleJob_MarshalCmd(struct nobis *nb,
    const M_SampleJobCmd *pCmd, unsigned char *buf, int *len_io)
{
  int rc;
  NF_Marshal_Context mc;

  nobis_setctx_marsh(nb, buf, *len_io, &mc);
  rc=Marshal_SampleJobCmd(&mc, pCmd);

  *len_io = *len_io - mc.remain;
  return rc;
}

int SampleJob_MarshalReply(struct nobis *nb,
    const M_SampleJobReply *pReply, unsigned char *buf, int *len_io)
{
  int rc;
  NF_Marshal_Context mc;

  nobis_setctx_marsh(nb, buf, *len_io, &mc);
  rc=Marshal_SampleJobReply(&mc, pReply);

  *len_io = *len_io - mc.remain;
  return rc;
}

int SampleJob_UnmarshalCmd(struct nobis *nb,
    const unsigned char *in, int in_len, M_SampleJobCmd *pCmd)
{
  NF_Unmarshal_Context uc;

  nobis_setctx_unmar(nb, in, in_len, &uc);
  memset(pCmd, 0, sizeof(*pCmd));

  return Unmarshal_SampleJobCmd(&uc, pCmd);
}

int SampleJob_UnmarshalReply(struct nobis *nb,
    const unsigned char *in, int in_len, M_SampleJobReply *pReply)
{
  NF_Unmarshal_Context uc;

  nobis_setctx_unmar(nb, in, in_len, &uc);
  memset(pReply, 0, sizeof(*pReply));

  return Unmarshal_SampleJobReply(&uc, pReply);
}

void SampleJob_FreeCmd(struct nobis *nb, M_SampleJobCmd *pCmd)
{
  NF_Free_Context fc;

  nobis_setctx_free(nb, &fc);
  Free_SampleJobCmd(&fc, pCmd);
}

void SampleJob_FreeReply(struct nobis *nb, M_SampleJobReply *pReply)
{
  NF_Free_Context fc;

  nobis_setctx_free(nb, &fc);
  Free_SampleJobReply(&fc, pReply);
}

int SampleUserData_Marshal(struct nobis *nb,
        const M_SampleUserData *pData, unsigned char *buf, M_Word *len_io)
{
  int rc;
  NF_Marshal_Context c;

  nobis_setctx_marsh(nb, buf, *len_io, &c);

  if (pData->magic != UD_MAGICHEADER) {
    rc = Status_Malformed;
  } else if (pData->ver > UD_CURRENTVER) {
    rc = Status_NotAvailable;
  } else {
    (void)((rc=NF_Marshal_Word(&c, &pData->magic))
    || (rc=NF_Marshal_Word(&c, &pData->ver))
    || (rc=NF_Marshal_Hash(&c, &pData->hkm))
    || (rc=NF_Marshal_Hash(&c, &pData->hkmwk))
    );
  }

  if (!rc)
    *len_io = *len_io - c.remain;

  return rc;
}

int SampleUserData_Unmarshal(struct nobis *nb,
        const unsigned char *in, int in_len, M_SampleUserData *pData)
{
  int rc;
  NF_Unmarshal_Context c;

  nobis_setctx_unmar(nb, in, in_len, &c);
  memset(pData, 0, sizeof(*pData));

  (void)((rc=NF_Unmarshal_Word(&c, &pData->magic))
  || (rc=NF_Unmarshal_Word(&c, &pData->ver))
  || (rc=NF_Unmarshal_Hash(&c, &pData->hkm))
  || (rc=NF_Unmarshal_Hash(&c, &pData->hkmwk))
  );

  if (!rc) {
    if (pData->magic != UD_MAGICHEADER)
      rc = Status_Malformed;
    if (pData->ver > UD_CURRENTVER) {
      pData->ver = UD_CURRENTVER;
      rc = Status_NotAvailable;
    }
  }

  return rc;
}

void SampleUserData_Free(struct nobis *nb, M_SampleUserData *pData)
{
  NF_Free_Context c;

  nobis_setctx_free(nb, &c);
}

static int printlabel(FILE* stream, M_Word indent, const char* label)
{
  M_Word i;
  for (i = 0; i < indent; ++i) fputc(' ', stream);
  fputs(label, stream);
  return indent + (int)strlen(label);
}

void SampleUserData_Print(struct nobis *nb, FILE *stream,
        M_Word indent, const M_SampleUserData *pData)
{
  NF_Print_Context ctx;
  nobis_setctx_print((nb), stream ? stream : NOBIS_TRACE_FILE, &ctx);

  indent = printlabel(stream, indent, "UD");
  NF_Print_Word(&ctx, printlabel(stream, 0, ".magic"), &pData->magic);
  NF_Print_Word(&ctx, printlabel(stream, indent, ".ver"), &pData->ver);
  NF_Print_Hash(&ctx, printlabel(stream, indent, ".hkm"), &pData->hkm);
  NF_Print_Hash(&ctx, printlabel(stream, indent, ".hkmwk"), &pData->hkmwk);
}


const char * SampleStr_Op(M_SampleOp job)
{
  switch (job) {
  case SampleOp_ErrorReturn:          return "ErrorReturn";
  case SampleOp_MachineVersion:       return "MachineVersion";
  case SampleOp_NoOp:                 return "NoOp";
  case SampleOp_FIPSandInit:          return "FIPSandInit";
  case SampleOp_EchoBlock:            return "EchoBlock";
  case SampleOp_PushBlock:            return "PushBlock";
  case SampleOp_PullBlock:            return "PullBlock";
  case SampleOp_PushPullBlock:        return "PushPullBlock";
  case SampleOp_NoOpTransact:         return "NoOpTransact";
  default: return "UNKNOWN";
  }
}

const char * SampleStr_Rep(M_SampleRep rep)
{
  switch (rep) {
  case SampleRep_Success:             return "Success";
  case SampleRep_InitFailed:          return "InitFailed";
  case SampleRep_nCoreFailed:         return "nCoreFailed";
  case SampleRep_FIPSFailed:          return "FIPSFailed";
  case SampleRep_MarshalFailed:       return "MarshalFailed";
  case SampleRep_UnmarshalFailed:     return "UnmarshalFailed";
  case SampleRep_UnknownCommand:      return "UnknownCommand";
  case SampleRep_NotYetImplemented:   return "NotYetImplemented";
  case SampleRep_CoreRTInitFailed:    return "CoreRTInitFailed";
  default: return "UNKNOWN";
  }
}
