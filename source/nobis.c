/* nobis.c
 * Copyright (C) 2010-2017 nCipher Security, LLC.  All rights reserved.
 * ---------------------------------------------------------------------
 *
 * An abstraction intended to simplify development of applications
 * that access nCipher hardware security modules.  The name is taken
 * from a relatively obscure legal term:
 *   http://en.wikipedia.org/wiki/Coram_nobis */
/* :TODO: optionally support RQCard */
#include <string.h>
#include <errno.h>
#include <ctype.h>
#include <limits.h>
#include "nobis.h"

#ifndef NF_CROSSCC
# include <readpp.h>

# ifdef NDEBUG
#  error ncthread-nt.c fails at runtime with NDEBUG defined
# endif
# ifdef localtime_r
#  undef localtime_r
# endif
# include "ncthread-upcalls.h"

/* ------------------------------------------------------------------ */
/* Custom upcalls are defined here to avoid dependencies on example
 * source files.  These are as simple as possible to assist auditing. */
struct NFast_Bignum {
  M_ByteBlock bb;
};

static void
nobis__bignum_copy(unsigned char *dst, const unsigned char *src,
                   unsigned nbytes, int swapends, int swapwords)
{
  int inc;
  unsigned nwords;

  if (!swapends && !swapwords)
    memcpy(dst, src, nbytes);
  else {
    if (swapwords) {
      dst += (nbytes-4);
      inc = -4;
    } else inc = 4;

    nwords = nbytes >> 2;
    if (swapends) {
      while (nwords-- > 0) {
        dst[0] = src[3];
        dst[1] = src[2];
        dst[2] = src[1];
        dst[3] = src[0];
        dst += inc;
        src += 4;
      }
    } else {
      while (nwords-- > 0) {
        dst[0] = src[0];
        dst[1] = src[1];
        dst[2] = src[2];
        dst[3] = src[3];
        dst += inc;
        src += 4;
      }
    }
  }
}

static int
nobis__bignum_receive(struct NFast_Application *app,
                      struct NFast_Call_Context *cctx,
                      struct NFast_Transaction_Context *tctx,
                      M_Bignum *bignum, int nbytes,
                      const void *source,
                      int msbitfirst, int mswordfirst)
{
  struct NFast_Bignum *bn;
  if (nbytes & 3)
    return Status_Malformed;

  if (!(bn = NFastApp_Malloc(app, sizeof(*bn), cctx, tctx)))
    return NOBIS_STATUS_NOMEM;
  if (!(bn->bb.ptr = NFastApp_Malloc(app, nbytes, cctx, tctx))) {
    NFastApp_Free(app, bn, cctx, tctx);
    return NOBIS_STATUS_NOMEM;
  }

  nobis__bignum_copy(bn->bb.ptr, (const unsigned char *)source,
                     nbytes, 0, 0);

  bn->bb.len = nbytes;
  *bignum=bn;
  return Status_OK;
}

static int
nobis__bignum_sendlen(struct NFast_Application *app,
                      struct NFast_Call_Context *cctx,
                      struct NFast_Transaction_Context *tctx,
                      const M_Bignum *bignum, int *nbytes)
{
  if ((*bignum)->bb.len & 3)
    return Status_Malformed;
  *nbytes = (*bignum)->bb.len;
  return Status_OK;
}

static int
nobis__bignum_send(struct NFast_Application *app,
                   struct NFast_Call_Context *cctx,
                   struct NFast_Transaction_Context *tctx,
                   const M_Bignum *bignum, int nbytes,
                   void *dest, int msbitfirst, int mswordfirst)
{
  struct NFast_Bignum *bn = *bignum;

  if (bn->bb.len != (unsigned)nbytes)
    return Status_InvalidParameter;
  if (nbytes & 3)
    return Status_Malformed;
  nobis__bignum_copy((unsigned char *)dest, (*bignum)->bb.ptr, nbytes,
                     msbitfirst, mswordfirst);
  return Status_OK;
}

static void
nobis__bignum_free(struct NFast_Application *app,
                   struct NFast_Call_Context *cctx,
                   struct NFast_Transaction_Context *tctx,
                   M_Bignum *bignum)
{
  if (*bignum)
    NFastApp_Free(app, (*bignum)->bb.ptr, cctx, tctx);
  NFastApp_Free(app, (*bignum), cctx, tctx);
}

static int
nobis__bignum_format(struct NFast_Application *app,
                     struct NFast_Call_Context *cctx,
                     struct NFast_Transaction_Context *tctx,
                     int *msbitfirst_io, int *mswordfirst_io)
{
  *msbitfirst_io = *mswordfirst_io = 0;
  return Status_OK;
}

static M_Status
nobis__diag_noisy(NFKM_DiagnosticContextHandle ctx,
                  const char* format, va_list args) {
  nobis_fail_msg((struct nobis *)ctx, "NFKM_checkconsistency", 0,
                 format, args);
  return Status_Failed;
}

static M_Status
nobis__diag_quiet(NFKM_DiagnosticContextHandle ctx,
                  const char* format, va_list args) {
  return Status_OK; /* be quiet! */
}

/* ------------------------------------------------------------------ */

void *
nobis__realloc(struct nobis *nb, const char *file, unsigned line,
               void *ptr, unsigned size)
{
  return nb ? NOBIS_CHK_NULLFN
    (nb, NFastApp_Realloc, (nb->apph, ptr, size, NULL, NULL),
     "%s:%u: %s", file, line, strerror(errno)) :
    NOBIS_CHK_NULLFN(NULL, realloc, (ptr, size),
                     "%s:%u: %s", file, line, strerror(errno));
}

void *
nobis__malloc(struct nobis *nb, const char *file, unsigned line,
              unsigned size)
{
  return nb ? NOBIS_CHK_NULLFN
    (nb, NFastApp_Malloc, (nb->apph, size, NULL, NULL),
     "%s:%u: %s", file, line, strerror(errno)) :
    NOBIS_CHK_NULLFN(NULL, malloc, (size),
                     "%s:%u: %s", file, line, strerror(errno));
}

void
nobis_free(struct nobis *nb, void *ptr)
{ if (nb) NFastApp_Free(nb->apph, ptr, NULL, NULL); else free(ptr); }

static void nobis__cctx_to_ncthread(NFast_AppHandle app,
                                    struct NFast_Call_Context *cc,
                                    struct nf_lock_cctx **lcc_r)
{ *lcc_r = 0; }

const NFast_NewThreadUpcalls nobis__thread_upcalls = {
  &ncthread_upcalls,
  nobis__cctx_to_ncthread
};
NFast_BignumUpcalls nobis__bignum_upcalls = {
  nobis__bignum_receive,
  nobis__bignum_sendlen,
  nobis__bignum_send,
  nobis__bignum_free,
  nobis__bignum_format
};
NFastAppInitArgs nobis__appinit = {
  NFAPP_IF_BIGNUM|NFAPP_IF_NEWTHREAD,
  NULL, &nobis__bignum_upcalls, NULL,
  NULL, &nobis__thread_upcalls, NULL, NULL
};

#else
# include <stdlib.h>

void *
nobis__realloc(struct nobis *nb, const char *file, unsigned line,
               void *ptr, unsigned size)
{
  return NOBIS_CHK_NULLFN(nb, realloc, (ptr, size),
                          "%s:%u: %s", file, line, strerror(errno));
}
void *
nobis__malloc(struct nobis *nb, const char *file, unsigned line,
              unsigned size)
{
  return NOBIS_CHK_NULLFN(nb, malloc, (size),
                          "%s:%u: %s", file, line, strerror(errno));
}

void
nobis_free(struct nobis *nb, void *ptr)
{ (void)nb; free(ptr); }
#endif

M_Status
nobis_bb_append(struct nobis *nb, M_ByteBlock *bb,
                const M_ByteBlock *data)
{
  M_Status result = Status_OK;
  M_ByteBlock updated = { bb->len + data->len, NULL };

  if ((updated.ptr = nobis_realloc(nb, bb->ptr, updated.len))) {
    memcpy(updated.ptr + bb->len, data->ptr, data->len);
    *bb = updated;
  } else result = NOBIS_STATUS_NOMEM;
  return result;
}

/**
 * Sends a command to the hardserver or firmware without making any
 * report if the command fails. */
static M_Status
nobis__tx_raw(struct nobis *nb, M_Command *command, M_Reply *reply)
{
#ifndef NF_CROSSCC
  return NFastApp_Transact(nb->conn, NULL, command, reply, NULL);
#else
  return SEElib_Transact(command, reply);
#endif
}

/**
 * Sends a report if and only if the supplied command has failed. */
static M_Status
nobis__tx_report(struct nobis *nb, const char *file, unsigned line,
                 M_Command *command, M_Status status)
{
  if (status != Status_OK) {
    const char *fn =
#ifndef NF_CROSSCC
      "NFastApp_Transact";
#else
      "SEElib_Transact";
#endif
    nobis_fail_msg
      (nb, fn, status, "%s (%d) at %s:%u",
       NF_Lookup(command->cmd, NF_Cmd_enumtable),
       command->cmd, file, line);
  }
  return status;
}

/**
 * Sends a command to the hardserver or firmware and reports if the
 * command fails. */
static M_Status
nobis__tx_cooked(struct nobis *nb, const char *file, unsigned line,
                 M_Command *command, M_Reply *reply)
{
  M_Status result;
  result = nobis__tx_raw(nb, command, reply);
  nobis__tx_report(nb, file, line, command, result);
  return result;
}

/**
 * Sends a command to the hardserver or firmware using SEE world
 * signing keys if necessary.  This routine is similar to nobis__tx_raw
 * in that it makes no report if the command fails. */
static M_Status
nobis__tx_signers(struct nobis *nb, M_Command *command, M_Reply *reply)
{
  M_Status result = Status_Failed;
  M_Word old_flags = command->flags;
  M_CertificateList *old_certs = command->certs;
  if (nb->worldsigners.n_sigs) {
    int i;
    M_CertificateList certs;
    M_Certificate     cert;
    memset(&cert, 0, sizeof(cert));
    cert.type = CertType_SEECert;
    memset(&certs, 0, sizeof(certs));
    certs.n_certs = 1;
    certs.certs = &cert;
    command->certs = &certs;
    command->flags |= Command_flags_certs_present;
    assert(nb->worldsigners.sigs);
    for (i = 0; i < nb->worldsigners.n_sigs; ++i) {
      memcpy(cert.keyhash.bytes,
             nb->worldsigners.sigs[i].hash.bytes,
             sizeof(cert.keyhash.bytes));
      if (i > 0)
        nobis_freereply(nb, reply);
      result = nobis__tx_raw(nb, command, reply);
      if (result || (reply->status != Status_AccessDenied))
        break; /* stop on non-access related failure */
      else if (!result && !reply->status)
        break; /* stop on success */
    }
  } else result = nobis__tx_raw(nb, command, reply);
  command->flags = old_flags;
  command->certs = old_certs;
  return result;
}

static M_Status
nobis__signers_setup(struct nobis *nb)
{
  M_Status result = Status_OK;
  M_Command command = {0};
  M_Reply reply;

  command.cmd = Cmd_GetWorldSigners;

  result = nobis__tx_raw(nb, &command, &reply);
  if (!result && !reply.status) {
    /* steal the reply data */
    nb->worldsigners = reply.reply.getworldsigners;
    memset(&reply.reply.getworldsigners, 0,
           sizeof(reply.reply.getworldsigners));
  } else if ((result == Status_OK) &&
             (reply.status == Status_InvalidParameter))
    result = Status_OK; /* Outside of SEE */
  else if (result == Status_OK)
    result = nobis_fail_msg
      (nb, __func__, reply.status,
       "Cmd_GetWorldSigners failed: %s (%d)",
       NF_Lookup(reply.status, NF_Status_enumtable), reply.status);
  else nobis_fail_msg(nb, __func__, result,
                      "transaction failed: %s (%d)",
                      NF_Lookup(result, NF_Status_enumtable), result);
  nobis_freereply(nb, &reply);
  return result;
}

static void
nobis__signers_cleanup(struct nobis *nb)
{
  M_Reply r = {0};
  r.cmd = Cmd_GetWorldSigners;
  r.reply.getworldsigners = nb->worldsigners;
  nobis_freereply(nb, &r);
  nb->worldsigners = r.reply.getworldsigners;
}

#ifndef NF_CROSSCC
static M_Status
nobis__module_slot(struct nobis *nb, M_ModuleID module, M_SlotID slot,
                   NFKM_ModuleInfo **minfo, NFKM_SlotInfo **sinfo)
{
  int i, j;
  for (i = 0; i < nb->world->n_modules; ++i) {
    if ((!module || (nb->world->modules[i]->module == module)) &&
        (nb->world->modules[i]->state !=
         ModuleState_UnusedTableEntry)) {
      NFKM_ModuleInfo *tmp_minfo;
      if (!minfo)
        minfo = &tmp_minfo;
      *minfo = nb->world->modules[i];
      for (j = 0; j < (*minfo)->n_slots; ++j)
        if (((*minfo)->slots[j]->token.slot == slot) &&
            ((*minfo)->slots[j]->slotstate !=
             SlotState_UnusedTableEntry)) {
          *sinfo = (*minfo)->slots[j];
          return Status_OK;
        }
    }
  }
  return nobis_fail_msg
    (nb, __func__, Status_NotAvailable,
     "Unable to find slot %d on module %d\n", slot, module);
}

static M_Status
nobis__checkenv(struct nobis *nb, const char *env,
                char *buffer, unsigned length)
{
  M_Status result = Status_OK;
  char *value = NULL;
  unsigned ii;
  unsigned current = 0;
  unsigned plen = (nb && nb->progname) ? (unsigned)strlen(nb->progname) : 0;
  unsigned elen = (unsigned)strlen(env);

  /* Construct environment variable name */
  if (plen && (plen + 1 < length)) {
    strncpy(buffer, nb->progname, plen);
    buffer[plen] = '_';
    current += plen + 1;
  }
  strncpy(buffer + current, env, length - current - 1);
  buffer[current + elen] = 0;
  for (ii = 0; ii < current + elen + 1; ++ii)
    if (islower(buffer[ii]))
      buffer[ii] = toupper(buffer[ii]);
    else if (!isalpha(buffer[ii]) && !isdigit(buffer[ii]))
      buffer[ii] = '_';

  if ((value = getenv(buffer)) && *value)
    strncpy(buffer, value, length);
  else result = Status_NotAvailable;
  return result;
}

static M_Status
nobis__readpp(struct nobis *nb, const char* env,
              const char* prompt, M_Hash* pp)
{
  M_Status result = Status_OK;
  const char* errmsg;
  char buffer[1024];

  if ((env && !nobis__checkenv(nb, env, buffer, sizeof(buffer))) ||
      !(errmsg = nf_readpp(prompt, buffer, sizeof(buffer))))
    result = NFKM_hashpp
      (nb->apph, nb->conn, (unsigned char *)buffer, pp, NULL);
  else result = nobis_fail_msg(nb, __func__, Status_OperationFailed,
                               "%s", errmsg);
  return result;
}

static void
nobis__prompt(struct nobis *nb, const char *prompt, ...) {
  int c;
  va_list args;
  va_start(args, prompt);
  vfprintf(NOBIS_TRACE_FILE, prompt, args);
  va_end(args);
  fflush(NOBIS_TRACE_FILE);
  while ((c = fgetc(stdin)) != EOF)
    if (c == '\n') break;
}
#endif

M_Status
nobis_setup(struct nobis *nb, unsigned flags, const char *progname)
{
  M_Status result = Status_OK;
  memset(nb, 0, sizeof(*nb));
  nb->progname = progname;
  nb->flags = flags;

#ifndef NF_CROSSCC
  if (!(result = NOBIS_CHK_STATUS
        (nb, NFastApp_InitEx, (&nb->apph, &nobis__appinit, NULL)))) {
    /* NFKM_getinfo must be called before NFastApp_Connect in order
     * to make use of existing objects (for example when using the
     * preload utility). */
    if (!result && !(nb->flags & nobis_setup_nosworld))
      /* This would crash if nb->world != NULL. */
      result = NOBIS_CHK_STATUS(nb, NFKM_getinfo,
                                (nb->apph, &nb->world, NULL));
    if (!result && (nb->flags & nobis_setup_checknfkm))
      result = NOBIS_CHK_STATUS
        (nb, NFKM_checkconsistency,
         (nb->apph, (NFKM_DiagnosticContextHandle)nb,
          nobis__diag_quiet, nobis__diag_quiet, nobis__diag_noisy,
          NULL));

    if (!result &&
        !(result = NOBIS_CHK_STATUS
          (nb, NFastApp_Connect,
           (nb->apph, &nb->conn, (nb->flags & nobis_setup_privconn) ?
            NFastApp_ConnectionFlags_Privileged : 0, NULL)))) {

      if (!(nb->udata = NOBIS_CHK_NULLFN
           (nb, NFastApp_AllocUD,
            (nb->apph, NULL, NULL, 0), "no memory.")))
        result = NOBIS_STATUS_NOMEM;
      if (result)
        NOBIS_CHK_STATUS(nb, NFastApp_Disconnect, (nb->conn, NULL));
    }
    if (result && nb->world)
      NFKM_freeinfo(nb->apph, &nb->world, NULL);
    if (result)
      NFastApp_Finish(nb->apph, NULL);
  }
#endif
  if (!result)
    result = nobis__signers_setup(nb);
  if (result)
    nobis_cleanup(nb);
  return result;
}

enum nobis_fipsf {
  nobis_fipsf_destroy = (1<<0),
};

struct nobis_fips {
  M_ModuleID module;
  unsigned flags;
  M_KeyID lt;
  M_KeyID key;
#ifndef NF_CROSSCC
  NFKM_FIPS140AuthHandle fipsh;
#endif
  M_CertificateList *certs;
};

static M_Status
nobis__fips_ensure(struct nobis *nb)
{
  M_Status result = Status_OK;
  if (nb->n_fips >= nb->m_fips) {
    struct nobis_fips *newfips;
    unsigned newmax = nb->n_fips + 1;
    newfips = nobis_realloc
      (nb, nb->fips, newmax * sizeof(*nb->fips));
    if (newfips) {
      nb->fips   = newfips;
      nb->m_fips = newmax;
    } else result = NOBIS_STATUS_NOMEM;
  }
  return result;
}

static void
nobis__fips_cleanup(struct nobis *nb, struct nobis_fips *fips)
{
  if (fips) {
    if (fips->certs) {
      NF_Free_Context fctx;
      nobis_setctx_free(nb, &fctx);
      NF_Free_CertificateList(&fctx, fips->certs);
      nobis_free(nb, fips->certs);
    }
    if (fips->flags & nobis_fipsf_destroy) {
      nobis_destroy(nb, fips->key);
      nobis_destroy(nb, fips->lt);
    }
#ifndef NF_CROSSCC
    if (fips->fipsh)
      NFKM_freefips140auth(nb->apph, nb->conn, fips->fipsh, NULL);
#endif
  } else {
    unsigned index;
    for (index = nb->n_fips; index; --index) {
      nobis__fips_cleanup(nb, &nb->fips[index - 1]);
      --nb->n_fips;
    }
    nobis_free(nb, nb->fips);
    nb->fips = NULL; nb->n_fips = nb->m_fips = 0;
  }
}

M_Status
nobis_fips_auth(struct nobis *nb, M_ModuleID module, M_SlotID slot)
{
  M_Status result;
#ifndef NF_CROSSCC
  NFKM_FIPS140AuthHandle fipsh = NULL;
  NFKM_SlotInfo *sinfo = NULL;
  unsigned index;

  /* No need for authorization in a non-StrictFIPS140 security world */
  if (!(nb->world->flags & WorldInfo_flags_StrictFIPS140))
    return Status_OK;

  /* No need to get authorization if already available */
  for (index = 0; index < nb->n_fips; ++index)
    if (nb->fips[index].module == module)
      return Status_OK;

  if (slot == UINT_MAX) {
    M_Command command = {0};
    M_Reply reply;

    slot = 0;
    command.cmd = Cmd_GetSlotList;
    command.args.getslotlist.module = module;
    if (!(result = nobis_transact(nb, 0, &command, &reply))) {
      int ii;

      for (ii = 0; ii < reply.reply.getslotlist.n_slots; ++ii)
        if ((reply.reply.getslotlist.slots[ii].type ==
             SlotType_SmartCard) &&
            (reply.reply.getslotlist.slots[ii].token.ic > 0))
          slot = ii;
      nobis_freereply(nb, &reply);
    }
  }

  if ((result = nobis__module_slot(nb, module, slot, NULL, &sinfo)))
    return result;
  for (;;) {
    if ((result = NFKM_fips140auth
         (nb->apph, nb->conn, sinfo, &fipsh, NULL)) ==
        Status_PhysTokenNotPresent) {
      nobis__prompt(nb, "Module %d slot %d: %s.\n  "
                    "Please insert any operator card and press enter.",
                    module, slot, NF_Lookup(SlotState_Empty,
                                            NF_SlotState_enumtable));
      if ((result = NOBIS_CHK_STATUS(nb, NFKM_getinfo,
                                     (nb->apph, &nb->world, NULL))))
        break;
    } else break;
  }

  if (!result) {
    if (!(result = nobis__fips_ensure(nb))) {
      M_Word flags = 0;
      struct nobis_fips *fips = &nb->fips[nb->n_fips];
      memset(fips, 0, sizeof(*fips));
      if (!(result = NOBIS_CHK_STATUS
            (nb, NFKM_newkey_makeauth,
             (nb->apph, nb->world, &flags,
              &fips->certs, fipsh, NULL)))) {
        fips->module = module;
        fips->fipsh = fipsh;
        fips->key = fips->fipsh ? fips->fipsh->key : 0;
        fips->lt  = fips->fipsh ? fips->fipsh->lt  : 0;
        ++nb->n_fips;
      }
    }
    if (result)
      NFKM_freefips140auth(nb->apph, nb->conn, fipsh, NULL);
  }
#else
  result = Status_NotAvailable;
#endif
  return result;
}

M_Status
nobis_fips_pack(struct nobis *nb, M_ModuleID module, M_KeyID seew,
                M_ByteBlock *bb)
{
  M_Status result = Status_OK;
  struct nobis_fips *fips = NULL;
  unsigned index;

  for (index = 0; index < nb->n_fips; ++index)
    if (nb->fips[index].module == module) {
      fips = &nb->fips[index];
      break;
    }

  if (!fips || !fips->certs) {
    bb->len = 0;
  } else {
    M_ByteBlock ticket_key = { 0, NULL };
    M_ByteBlock ticket_lt = { 0, NULL };
    NF_Free_Context fctx;
    NF_Marshal_Context mctx;
    nobis_setctx_marbb(nb, bb, &mctx);
    nobis_setctx_free(nb, &fctx);
    if (!(result = NOBIS_CHK_STATUS
          (nb, NF_Marshal_CertificateList,
           (&mctx, fips->certs))) &&
        !(result = nobis_ticket_create
          (nb, &ticket_key, seew, 0, fips->key)) &&
        !(result = nobis_ticket_create
          (nb, &ticket_lt, seew, 0, fips->lt)) &&
        !(result = NOBIS_CHK_STATUS
          (nb, NF_Marshal_ByteBlock, (&mctx, &ticket_key))) &&
        !(result = NOBIS_CHK_STATUS
          (nb, NF_Marshal_ByteBlock, (&mctx, &ticket_lt)))) {
      bb->len -= mctx.remain;
    }
    NF_Free_ByteBlock(&fctx, &ticket_key);
    NF_Free_ByteBlock(&fctx, &ticket_lt);
  }
  return result;
}

static M_Certificate *
nobis__find_keycert(M_CertificateList *certs)
{
  int i;
  for (i = 0; i < certs->n_certs; ++i) {
    M_Certificate *cert = certs->certs + i;
    if (cert->type == CertType_SigningKey)
      return cert;
  }
  return NULL;
}

M_Status
nobis_fips_unpack(struct nobis *nb, M_ModuleID module, M_ByteBlock *bb)
{
  M_Status result = Status_OK;
  unsigned index;

  for (index = 0; index < nb->n_fips; ++index)
    if (nb->fips[index].module == module)
      return result; /* No need to unpack if already present */

  if (bb->len > 0) {
    M_CertificateList *certs;
    if ((certs = nobis_malloc(nb, sizeof(M_CertificateList)))) {
      M_ByteBlock ticket_key = { 0, NULL };
      M_ByteBlock ticket_lt =  { 0, NULL };
      NF_Free_Context fctx;
      NF_Unmarshal_Context uctx;
      nobis_setctx_unmar(nb, bb->ptr, bb->len, &uctx);
      nobis_setctx_free(nb, &fctx);
      memset(certs, 0, sizeof(*certs));

      if (!(result = NOBIS_CHK_STATUS(nb, NF_Unmarshal_CertificateList,
                                      (&uctx, certs))) &&
          !(result = NOBIS_CHK_STATUS(nb, NF_Unmarshal_ByteBlock,
                                      (&uctx, &ticket_key))) &&
          !(result = NOBIS_CHK_STATUS(nb, NF_Unmarshal_ByteBlock,
                                      (&uctx, &ticket_lt)))) {
        M_Certificate *keycert;
        if ((keycert = nobis__find_keycert(certs))) {
          M_KeyID fips_key = 0, fips_lt = 0;
          if (!(result = nobis_ticket_redeem
                (nb, &fips_key, module, &ticket_key)) &&
              !(result = nobis_ticket_redeem
                (nb, &fips_lt, module, &ticket_lt)) &&
              !(result = nobis__fips_ensure(nb))) {
            nb->fips[nb->n_fips].certs = certs;
            nb->fips[nb->n_fips].key =
              keycert->body.signingkey.key = fips_key;
            nb->fips[nb->n_fips].lt = fips_lt;
            nb->fips[nb->n_fips].flags = nobis_fipsf_destroy;
            ++nb->n_fips;

            fips_key = fips_lt = 0;
            certs = NULL;
          }
          nobis_destroy(nb, fips_key);
          nobis_destroy(nb, fips_lt);
        }
      }
      NF_Free_ByteBlock(&fctx, &ticket_key);
      NF_Free_ByteBlock(&fctx, &ticket_lt);
      if (certs) {
        printf("DEBUG %s:%u \n", __FILE__, __LINE__);
        NF_Free_CertificateList(&fctx, certs);
        nobis_free(nb, certs);
      }
    } else return NOBIS_STATUS_NOMEM;
  }
  return result;
}

void
nobis_cleanup(struct nobis *nb)
{
  nobis__signers_cleanup(nb);
  nobis__fips_cleanup(nb, NULL);
#ifndef NF_CROSSCC
  if (nb->udata)
    NFastApp_FreeUD(nb->udata);
  if (nb->world)
    NFKM_freeinfo(nb->apph, &nb->world, NULL);
  NOBIS_CHK_STATUS(nb, NFastApp_Disconnect, (nb->conn, NULL));
  NFastApp_Finish(nb->apph, NULL);
#endif
}

M_Status
nobis__transact(struct nobis *nb, const char *file, unsigned line,
                unsigned flags, M_Command *command, M_Reply *reply)
{
  M_Status result = Status_OK;

  if ((flags & nobis_tx_fips) && (nb->n_fips > 0)) {
    struct nobis_fips *fips = NULL;
    unsigned index;
    M_ModuleID module = 0;
    switch (command->cmd) {
    case Cmd_GenerateKey:
      module = command->args.generatekey.module;
      break;
    case Cmd_GenerateKeyPair:
      module = command->args.generatekeypair.module;
      break;
    case Cmd_Import:
      module = command->args.import.module;
      break;
    default:
      /* :TODO: retry with each fips if module == 0 */
      module = nb->fips[0].module;
    }

    for (index = 0; index < nb->n_fips; ++index)
      if (nb->fips[index].module == module)
        fips = &nb->fips[index];
    if (fips) {
      M_Word old_flags = command->flags;
      M_CertificateList *old_certs = command->certs;

      command->certs = fips->certs;
      command->flags |= Command_flags_certs_present;
      result = (flags & nobis_tx_quiet) ?
        nobis__tx_raw(nb, command, reply) :
        nobis__tx_cooked(nb, file, line, command, reply);
      command->certs = old_certs;
      command->flags = old_flags;
    }
  } else if (flags & nobis_tx_signers) {
    result = nobis__tx_signers(nb, command, reply);
    if (!(flags & nobis_tx_quiet))
      nobis__tx_report(nb, file, line, command, result);
  } else if (flags & nobis_tx_quiet)
    result = nobis__tx_raw(nb, command, reply);
  else result = nobis__tx_cooked(nb, file, line, command, reply);

  if (result) {
    nobis_freereply(nb, reply);
    memset(&reply, 0, sizeof(reply));
  } else if (reply->status) {
    char errbuf[128];
    NFast_StrError(errbuf, sizeof errbuf, reply->status,
                   &reply->errorinfo);
    result = (flags & nobis_tx_quiet) ? reply->status : nobis_fail_msg
      (nb, NF_Lookup(command->cmd, NF_Cmd_enumtable),
       reply->status, "reply status %s (%d) at %s:%u",
       errbuf, reply->status, file, line);
    nobis_freereply(nb, reply);
    memset(&reply, 0, sizeof(reply));
  }
  if (flags & nobis_tx_freecmd)
#ifndef NF_CROSSCC
    NFastApp_Free_Command(nb->apph, NULL, NULL, command);
#else
    SEElib_FreeCommand(command);
#endif
  return result;
}

M_Status
nobis__transact_certify(struct nobis *nb, const char *file,
                        unsigned line, unsigned flags,
                        M_Command *command, M_Reply *reply,
                        M_KeyID auth_key)
{
  M_Status result = Status_OK;
  M_CertificateList *certs;

  if (!auth_key)
    return nobis__transact(nb, file, line, flags, command, reply);

  if ((certs = nobis_malloc(nb, sizeof(M_CertificateList)))) {
    certs->n_certs = 1;
    if ((certs->certs = nobis_malloc
         (nb, sizeof(M_Certificate) * certs->n_certs))) {
      M_Command cmdinfo;
      M_Reply reply_inner;

      memset(&cmdinfo, 0, sizeof(cmdinfo));
      cmdinfo.cmd = Cmd_GetKeyInfo;
      cmdinfo.args.getkeyinfo.key = auth_key;
      if (!(result = nobis__transact
            (nb, file, line, 0, &cmdinfo, &reply_inner))) {
        certs->certs[0].keyhash = reply_inner.reply.getkeyinfo.hash;
        certs->certs[0].type = CertType_SigningKey;
        certs->certs[0].body.signingkey.key = auth_key;
        nobis_freereply(nb, &reply_inner);

        command->certs = certs;
        command->flags |= Command_flags_certs_present;
        result = nobis__transact(nb, file, line, flags, command, reply);
        if (flags & nobis_tx_freecmd)
          certs = NULL;
        command->certs = NULL;
      }
      if (certs)
        nobis_free(nb, certs->certs);
    } else result = Status_OSErrorErrno;
    nobis_free(nb, certs);
    command->certs = NULL;
  } else result = Status_OSErrorErrno;
  return result;
}

struct nobis *
nobis_freereply(struct nobis *nb, M_Reply *reply)
{
#ifndef NF_CROSSCC
  NFastApp_Free_Reply(nb->apph, NULL, NULL, reply);
#else
  SEElib_FreeReply(reply);
#endif
  return nb;
}

struct nobis *
nobis_freebb(struct nobis *nb, M_ByteBlock *bb)
{
  NF_Free_Context fctx;
  nobis_setctx_free(nb, &fctx);
  NF_Free_ByteBlock(&fctx, bb);
  return nb;
}

M_Status
nobis__ticket_create(struct nobis *nb, const char *file, unsigned line,
                     M_ByteBlock *dest, M_KeyID seew, unsigned flags,
                     M_KeyID obj)
{
  M_Status  result;
  M_Command command = {0};
  M_Reply   reply;
  command.cmd = Cmd_GetTicket;
  command.args.getticket.flags |= flags;
  command.args.getticket.obj = obj;
  if (seew) {
    command.args.getticket.dest = TicketDestination_NamedSEEWorld;
    command.args.getticket.destspec.namedseeworld.world = seew;
  } else command.args.getticket.dest = TicketDestination_Any;
  if (!(result = nobis__transact
        (nb, file, line, 0, &command, &reply))) {
    if (dest) {
      *dest = reply.reply.getticket.ticket;
      memset(&reply.reply.getticket.ticket, 0,
             sizeof(reply.reply.getticket.ticket));
    }
    nobis_freereply(nb, &reply);
  }
  return result;
}

M_Status
nobis__ticket_redeem(struct nobis *nb, const char *file, unsigned line,
                     M_KeyID *dest, M_ModuleID module,
                     const M_ByteBlock *ticket)
{
  M_Status  result;
  M_Command command = {0};
  M_Reply   reply;
  command.cmd = Cmd_RedeemTicket;
  command.args.redeemticket.ticket = *ticket;
  command.args.redeemticket.module = module;
  if (!(result = nobis__transact
        (nb, file, line, 0, &command, &reply))) {
    if (dest)
      *dest = reply.reply.redeemticket.obj;
    else nobis_destroy(nb, reply.reply.redeemticket.obj);
    nobis_freereply(nb, &reply);
  }
  return result;
}

M_Status
nobis_bignum_create(struct nobis *nb, M_Bignum *bn,
                    const M_ByteBlock *in)
{
  M_Status result = Status_OK;
  unsigned i;
  if ((*bn = nobis_malloc(nb, sizeof(struct NFast_Bignum)))) {
    /* SEElib requires little endian and four byte boundaries. */
    unsigned len = (4 * ((in->len + 3) / 4));
    if (((*bn)->bb.ptr = nobis_malloc(nb, len))) {
      (*bn)->bb.len = len;
      for (i = 0; i < (*bn)->bb.len; ++i)
        (*bn)->bb.ptr[i] = (i < in->len) ? in->ptr[in->len - i - 1] : 0;
    } else {
      result = NOBIS_STATUS_NOMEM;
      nobis_free(nb, *bn);
      *bn = NULL;
    }
  } else result = NOBIS_STATUS_NOMEM;
  return result;
}


M_Status
nobis_bignum_create_fromLE(struct nobis *nb, M_Bignum *bn,
                    const M_ByteBlock *in)
{
  M_Status result = Status_OK;
  if ((*bn = nobis_malloc(nb, sizeof(struct NFast_Bignum)))) {
    /* SEElib requires little endian and four byte boundaries. */
    unsigned len = (4 * ((in->len + 3) / 4));
    if (((*bn)->bb.ptr = nobis_malloc(nb, len))) {
      (*bn)->bb.len = len;
      memset( (*bn)->bb.ptr, 0x00, (*bn)->bb.len);
      memcpy( (*bn)->bb.ptr, in->ptr, in->len);
    } else {
      result = NOBIS_STATUS_NOMEM;
      nobis_free(nb, *bn);
      *bn = NULL;
    }
  } else result = NOBIS_STATUS_NOMEM;
  return result;
}

void
nobis_bignum_cleanup(struct nobis *nb, M_Bignum *bn)
{
  if (*bn)
    nobis_free(nb, (*bn)->bb.ptr);
  nobis_free(nb, *bn);
}

unsigned
nobis_bignum_size(const M_Bignum *bn)
{
  return bn ? (*bn)->bb.len : 0;
}

M_Status
nobis_bignum_bytes_minimum(struct nobis *nb, const M_Bignum *bn,
                           unsigned min, M_ByteBlock *out)
{
  M_Status result = Status_OK;
  unsigned extra = 0;
  if ((*bn)->bb.len < min)
    extra = min - (*bn)->bb.len;
  if ((out->ptr = nobis_malloc
       (nb, out->len = ((*bn)->bb.len + extra)))) {
    unsigned i;
    for (i = 0; i < extra; ++i)
      out->ptr[i] = 0;
    for (i = 0; i < (*bn)->bb.len; ++i)
      out->ptr[i + extra] = (*bn)->bb.ptr[(*bn)->bb.len - 1 - i];
  } else result = NOBIS_STATUS_NOMEM;
  return result;
}

M_Status
nobis_bignum_bytes(struct nobis *nb, const M_Bignum *bn,
                   M_ByteBlock *out)
{ return nobis_bignum_bytes_minimum(nb, bn, 0, out); }

int
nobis_bignum_cmp(const M_Bignum *bn_a, const M_Bignum *bn_b)
{
  /* At present this only considers exact matches and does not
   * necessarily consistently report whether one bignum is
   * greater than the other.  Also, if bignums differ only by
   * extra trailing zeros they may be reported as unequal when
   * in fact they represent the same value.  It's good enough
   * for the purpose it's used for today but could be improved. */
  return ((*bn_a)->bb.len == (*bn_b)->bb.len) ?
    memcmp((*bn_a)->bb.ptr, (*bn_b)->bb.ptr, (*bn_a)->bb.len) :
    ((*bn_a)->bb.len > (*bn_b)->bb.len ? 1 : -1);
}

M_Status
nobis_bignum_bytes_LE(struct nobis *nb, const M_Bignum *bn,
                   M_ByteBlock *out, size_t minsize)
{
  M_Status result = Status_OK;
  out->len = (*bn)->bb.len;
  if (out->len < minsize)
    out->len = (unsigned)minsize;
  if ((out->ptr = nobis_malloc(nb, out->len))) {
    memset(out->ptr, 0, out->len);
    memcpy(out->ptr, (*bn)->bb.ptr, (*bn)->bb.len);
  }
  else result = NOBIS_STATUS_NOMEM;
  return result;
}

int
nobis_bignum_is_odd(struct nobis *nb, const M_Bignum *bn)
{
  if ((*bn)->bb.len && (*bn)->bb.ptr)
    return (*bn)->bb.ptr[0] & 1;
  return 0;
}

#ifndef NF_CROSSCC

int nfopt_nflookup(const nfopt_info *i)
{
   const NF_ValInfo *v = (NF_ValInfo *)i->o->args;
   M_Word *result = i->o->p;

   if (NF_LookupString(i->arg, result, v)) {
     nfopt_wordwrap w = { 8, 0, 0, 0, 0, 0, {0} };

     nfopt_printl(NOBIS_TRACE_FILE, &w, "$p: unknown $0 `$1'.\n",
                  i, i->o->argname, i->arg, (char *)0);
     return -1;
   }
   return 0;
}

int nfopt_byteblock(const nfopt_info *i)
{
  M_ByteBlock *result = i->o->p;

  if (nobis_unhexbb(NULL, 0, result, i->arg)) {
    nfopt_wordwrap w = { 8, 0, 0, 0, 0, 0, {0} };

    nfopt_printl(NOBIS_TRACE_FILE, &w, "$p: hex conversion failed for $0 `$1'.\n",
                 i, i->o->argname, i->arg, (char *)0);
    return -1;
  }
  return 0;
}

/* On some platforms a va_list can't be used more than once, which
 * means this function can't practically compute the length of the
 * variadic arguments.  That's why the caller must pass this value
 * as ident_len. */
static M_Status
nobis__sworld_keyinfo(struct nobis *nb, NFKM_Key **kinfo,
                      unsigned flags, const char *appname,
                      unsigned ident_len, const char *ident,
                      va_list args)
{
  M_Status result = Status_OK;
  char *ident_ptr;
  if ((ident_ptr = nobis_malloc(nb, ident_len + 1))) {
    NFKM_KeyIdent kident = { (char*)appname, ident_ptr };
    char *split;

    vsnprintf(ident_ptr, ident_len + 1, ident, args);
    if ((split = strchr(ident_ptr, ','))) {
      *split = '\0';
      kident.ident   = ++split;
      kident.appname = ident_ptr;
    }

    if (kinfo) {
      if (!(result = NOBIS_CHK_STATUS
            (nb, NFKM_findkey, (nb->apph, kident, kinfo, NULL)))) {
        if (!*kinfo) {
          if (!(flags & nobis_keyi_missingok))
            NOBIS_CHK_NULL(nb, *kinfo, NFKM_findkey,
                           "no `%s' key with ident `%s' found.",
                           kident.appname, kident.ident);
          result = Status_NotAvailable;
        }
      }
    } else { /* NULL kinfo means check for existance only. */
      NFKM_Key *ktemp;
      if (!(result = NOBIS_CHK_STATUS
            (nb, NFKM_findkey, (nb->apph, kident, &ktemp, NULL)))) {
        if (ktemp)
          NFKM_freekey(nb->apph, ktemp, NULL);
        else result = Status_NotAvailable;
      }
    }
    nobis_free(nb, ident_ptr);
  } else result = NOBIS_STATUS_NOMEM;
  return result;
}

M_Status
nobis_sworld_keyinfo(struct nobis *nb, NFKM_Key **kinfo, unsigned flags,
                     const char *appname, const char *ident, ...)
{
  M_Status result;
  unsigned ident_len;
  va_list args;

  va_start(args, ident);
  ident_len = vsnprintf(NULL, 0, ident, args);
  va_end(args);

  va_start(args, ident);
  result = nobis__sworld_keyinfo
    (nb, kinfo, flags, appname, ident_len, ident, args);
  va_end(args);
  return result;
}

void
nobis_sworld_freekeyinfo(struct nobis *nb, NFKM_Key *kinfo)
{ NFKM_freekey(nb->apph, kinfo, NULL); }

static M_Status
nobis__sworld_admin_load(struct nobis *nb, int module, int slot,
                         NFKM_LoadAdminKeysHandle *lakh,
                         int tokenspec, va_list args) {
  /* NOTE: this doesn't currently work with existing objects.  Making
   * it do so could improve security because if preloading
   * administrator cards is supported it should be possible to enable
   * debugging for the rest of the program withouth exposing pass
   * phrase hashes for such cards. */
  M_Status result;
  NFKM_ModuleInfo* minfo = NULL;
  NFKM_SlotInfo*   sinfo = NULL;
  int *tokens = NULL;
  unsigned tokens_used = 0, tokens_limit = 0;

  for (;;) { /* process variable arguments */
    if (tokens_used + 1 >= tokens_limit) {
      int *tokens_next = nobis_realloc
        (nb, tokens, sizeof(int) *
         (tokens_limit = tokens_limit ? (2 * tokens_limit) : 4));
      if (!tokens_next) {
        nobis_free(nb, tokens);
        return Status_OSErrorErrno;
      } else tokens = tokens_next;
    }
    tokens[tokens_used++] = tokenspec;
    if (tokenspec < 0)
      break;
    tokenspec = va_arg(args, int);
  }

  if (!(result = NOBIS_CHK_STATUS
        (nb, NFKM_loadadminkeys_selecttokens,
         (*lakh, tokens)))) {
    int remaining;

    if (!(result = NOBIS_CHK_STATUS
          (nb, NFKM_loadadminkeys_loadtokens,
           (*lakh, &remaining)))) {
      while (remaining > 0) {
        if (!(result = NOBIS_CHK_STATUS
              (nb, NFKM_getinfo,
               (nb->apph, &nb->world, NULL)))) {
          if (!nobis__module_slot
              (nb, module, slot, &minfo, &sinfo) &&
              (sinfo->slotstate == SlotState_Admin)) {
            if (sinfo->flags & SlotInfo_flags_Passphrase) {
              do {
                M_Hash pp;
                result = nobis__readpp
                  (nb, NULL, "Please enter the administrator card "
                   "pass phrase: ", &pp);
                if (result)
                  break;
                result = NFKM_loadadminkeys_nextcard
                  (*lakh, sinfo, &pp, &remaining);
              } while (result == Status_DecryptFailed);
            } else result = NFKM_loadadminkeys_nextcard
                     (*lakh, sinfo, NULL, &remaining);
            if (result && (result != Status_IncorrectToken)) {
              nobis_check_status(nb, __FILE__, __LINE__, result,
                                 "NFKM_loadadminkeys_nextcard");
              break;
            } else if (result == Status_IncorrectToken)
              nobis__prompt(nb, "This card had either already been "
                            "read or is not a member of the\n  "
                            "administrator card set.\n"
                            "  Please insert another administrator "
                            "card in\n  module %d slot %d and "
                            "press enter.", module, slot);
            else if (remaining > 0)
              nobis__prompt(nb, "Please insert the next "
                            "administrator card in\n  module %d "
                            "slot %d and press enter.",
                            module, slot);
          } else nobis__prompt
                   (nb, "Module %d slot %d: %s.\n  "
                    "Please insert an administrator "
                    "card and press enter.", module, slot,
                    NF_Lookup(sinfo->slotstate,
                              NF_SlotState_enumtable));
        } else break;
      }
    }
  }
  nobis_free(nb, tokens);
  return result;
}

M_Status
nobis_sworld_admin_load(struct nobis *nb, int module, int slot,
                        NFKM_LoadAdminKeysHandle *lakh,
                        int tokenspec, ...) {
  M_Status result;
  va_list args;
  va_start(args, tokenspec);
  result = nobis__sworld_admin_load
    (nb, module, slot, lakh, tokenspec, args);
  va_end(args);
  return result;
}

M_Status
nobis_sworld_admin_setup(struct nobis *nb, int module, int slot,
                         NFKM_LoadAdminKeysHandle *lakh,
                         int tokenspec, ...) {
  M_Status result;
  NFKM_ModuleInfo* minfo = NULL;
  NFKM_SlotInfo*   sinfo = NULL;
  if (!(result = nobis__module_slot
        (nb, module, slot, &minfo, &sinfo))) {
    if (!(result = NOBIS_CHK_STATUS
          (nb, NFKM_loadadminkeys_begin,
           (nb->apph, nb->conn, lakh, minfo, NULL)))) {
      va_list args;
      va_start(args, tokenspec);
      result = nobis__sworld_admin_load
        (nb, module, slot, lakh, tokenspec, args);
      va_end(args);
    }
  }
  if (result)
    NFKM_loadadminkeys_done(*lakh);
  return result;
}

M_Status
nobis_sworld_cstoken(struct nobis *nb, const NFKM_CardSet* cs,
                     M_ModuleID module, M_SlotID slot, M_KeyID* token)
{
  M_Status result;
  NFKM_ModuleInfo* minfo = NULL;
  NFKM_SlotInfo*   sinfo = NULL;

  result = NFKM_findexistingobject
    (nb->world, module, &cs->hkltu, token);
  if (result == Status_KeyNotLoaded) {
    if (!(result = nobis__module_slot
          (nb, module, slot, &minfo, &sinfo))) {
      NFKM_LoadCSHandle state;
      if (!(result = NOBIS_CHK_STATUS
            (nb, NFKM_loadcardset_begin,
             (nb->apph, nb->conn, minfo, cs, &state, NULL)))) {
        int remaining = 1;
        do {
          if (!(result = NOBIS_CHK_STATUS
                (nb, NFKM_getinfo, (nb->apph, &nb->world, NULL)))) {
            if (!nobis__module_slot(nb, module, slot, &minfo, &sinfo) &&
                (sinfo->slotstate == SlotState_Operator)) {
              if (sinfo->flags & SlotInfo_flags_Passphrase) {
                do {
                  M_Hash pp;
                  result = nobis__readpp
                    (nb, NULL, "Please enter the pass phrase for this "
                     "operator card: ", &pp);
                  if (result)
                    break;
                  result = NFKM_loadcardset_nextcard
                    (state, sinfo, &pp, &remaining);
                } while (result == Status_DecryptFailed);
              } else {
                result = NFKM_loadcardset_nextcard
                  (state, sinfo, NULL, &remaining);
              }
              if (result && result != Status_IncorrectToken) {
                nobis_check_status(nb, __FILE__, __LINE__, result,
                                   "NFKM_loadcardset_nextcard");
                break;
              } else if (result == Status_IncorrectToken) {
                nobis__prompt(nb, "This card is not a member of the "
                              "\"%s\" card set.\nPlease insert a "
                              "card from \"%s\"\nin "
                              "module %d slot %d and "
                              "press enter.", cs->name, cs->name,
                              module, slot);
              } else if (remaining > 0)
                nobis__prompt(nb, "Please insert another "
                              "card from \"%s\"\nin "
                              "module %d slot %d and "
                              "press enter.", cs->name, module, slot);
            } else nobis__prompt(nb, "Module %d slot %d: %s.\n  "
                                 "Please insert an operator card "
                                 "from \"%s\" and press enter.",
                                 module, slot,
                                 NF_Lookup(sinfo->slotstate,
                                           NF_SlotState_enumtable),
                                 cs->name);
          } else break;
        } while (remaining > 0);

        if (!result)
          result = NOBIS_CHK_STATUS
            (nb, NFKM_loadcardset_done, (state, token));
        else NFKM_loadcardset_abort(state);
      }
    }
  } else nobis_check_status(nb, __FILE__, __LINE__, result,
                            "NFKM_findexistingobject");
  return result;
}

M_Status
nobis_sworld_sctoken(struct nobis *nb, const NFKM_SoftCard* sc,
                     M_ModuleID module, M_KeyID* token)
{
  M_Status result;
  result = NFKM_findexistingobject
    (nb->world, module, &sc->hkltu, token);
  if (result == Status_KeyNotLoaded) {
    NFKM_ModuleInfo *minfo = NULL;
    int i;
    for (i = 0; i < nb->world->n_modules; ++i)
      if ((!module || (nb->world->modules[i]->module == module)) &&
          (nb->world->modules[i]->state !=
           ModuleState_UnusedTableEntry))
        minfo = nb->world->modules[i];
    if (minfo) {
      M_Hash pp;
      do {
        result = nobis__readpp
          (nb, "softcard", "Please enter soft card pass phrase: ", &pp);
        if (result)
          break;
        result = NFKM_loadsoftcard(nb->apph, nb->conn, minfo,
                                   sc, &pp, token, NULL);
        if (result && result != Status_DecryptFailed)
          nobis_check_status(nb, __FILE__, __LINE__, result,
                             "NFKM_loadsoftcard");
      } while (result == Status_DecryptFailed);
    } else result = nobis_fail_msg
             (nb, "nobis__module_slot", Status_NotAvailable,
              "Unable to find module %d\n", module);
  } else nobis_check_status(nb, __FILE__, __LINE__, result,
                            "NFKM_findexistingobject");
  return result;
}

M_Status
nobis_sworld_tokenstruct(struct nobis *nb, const char *name,
                         const NFKM_CardSet **cs,
                         const NFKM_SoftCard **sc)
{
  M_Status result;
  NFKM_CardSetIdent *csis;
  M_Hash htoken;
  int ncsis;

  *cs = NULL;
  *sc = NULL;
  if (!(result = NOBIS_CHK_STATUS(nb, NFKM_listcardsets,
                                  (nb->apph, &ncsis, &csis, NULL))))
    for (; ncsis; --ncsis) {
      if (!(result = NOBIS_CHK_STATUS(nb, NFKM_findcardset,
                                      (nb->apph, &csis[ncsis - 1],
                                       (NFKM_CardSet**)cs, NULL)))) {
        if (strcmp(name, (*cs)->name)) {
          NFKM_freecardset(nb->apph, (NFKM_CardSet*)*cs, NULL);
          *cs = NULL;
        } else return result;
      } else return result;
    }
  if ((result = NOBIS_CHK_STATUS(nb, NFKM_findsoftcard_name,
                                 (nb->apph, name,
                                  (NFKM_SoftCard**)sc, NULL))) || *sc)
    return result;
  if (!nobis_unhexify(nb, nobis_uhex_quiet, htoken.bytes,
                      sizeof(htoken.bytes), NULL, name)) {
    NFKM_CardSetIdent csi;
    NFKM_SoftCardIdent sci;

    memcpy(csi.bytes, htoken.bytes, sizeof(csi.bytes));
    if ((result = NOBIS_CHK_STATUS
         (nb, NFKM_findcardset,
          (nb->apph, &csi, (NFKM_CardSet**)cs, NULL))) || *cs)
      return result;
    memcpy(sci.bytes, htoken.bytes, sizeof(sci.bytes));
    if ((result = NOBIS_CHK_STATUS(nb, NFKM_findsoftcard_hash,
                                   (nb->apph, &sci,
                                    (NFKM_SoftCard**)sc, NULL))) || *sc)
      return result;
  }
  return nobis_fail_msg
    (nb, __func__, Status_NotAvailable, "`%s' not found.", name);
}

M_Status
nobis_sworld_makeacl(struct nobis *nb, M_Word flags, M_Word op_base,
                     const char *token, const char *seeii, M_Word pa_uselimit,
                     M_ACL *aclprv)
{
  M_Status result = Status_OK;
  M_KeyHashAndMech seeintegkham;
  NFKM_MakeACLParams params;
  memset(&params, 0, sizeof(params));
  params.f = flags;
  params.op_base = op_base;

  if (pa_uselimit != 0) {
    params.pa_uselimit = pa_uselimit;
    params.f |= NFKM_NKF_PerAuthUseLimit;
  }

  if (token && (result = nobis_sworld_tokenstruct
                (nb, token, &params.cs, &params.sc)))
    return result;
  if (params.cs)
    params.f |= NFKM_NKF_ProtectionCardSet;
  else if (params.sc)
    params.f |= NFKM_NKF_ProtectionPassPhrase;
  else params.f |= NFKM_NKF_ProtectionModule;

  if (seeii) {
    NFKM_Key *seekinfo = NULL;
    if (!(result = nobis_sworld_keyinfo
          (nb, &seekinfo, 0, "seeinteg", "%s", seeii))) {
      memset(&seeintegkham, 0, sizeof(seeintegkham));
      seeintegkham.mech = Mech_Any;
      seeintegkham.hash = seekinfo->hash;
      params.seeintegkham = &seeintegkham;
      params.f |= NFKM_NKF_SEEAppKeyHashAndMech;
    }
  }
  if (!result)
    result = NOBIS_CHK_STATUS
      (nb, NFKM_newkey_makeaclx,
       (nb->apph, nb->conn, nb->world,
        &params, aclprv, NULL));
  NFKM_freecardset(nb->apph, (NFKM_CardSet*)params.cs, NULL);
  NFKM_freesoftcard(nb->apph, (NFKM_SoftCard*)params.sc, NULL);
  return result;
}

M_Status
nobis_sworld_storekp(struct nobis *nb, M_Word flags,
                     M_ModuleID module, M_SlotID slot,
                     M_KeyID kpub, M_KeyID kprv, M_ModuleCert *mc,
                     int nentries, M_KeyMgmtFileEntry *entries,
                     const char *token, const char *seeident,
                     const char *appname, const char *ident, ...)
{
  M_Status result = Status_OK;
  unsigned index;
  NFKM_MakeBlobsParams params;
  NF_Free_Context fctx;
  nobis_setctx_free(nb, &fctx);

  memset(&params, 0, sizeof(params));
  params.kpriv = kprv;
  params.kpub  = kpub;
  params.f = flags;
  for (index = 0; index < nb->n_fips; ++index) {
    params.fips  = nb->fips[index].fipsh;
    if (nb->fips[index].module == module)
        break;
  }

  if (token && (result = nobis_sworld_tokenstruct
                (nb, token, &params.cs, &params.sc)))
    return result;
  if (params.cs) {
    params.f |= NFKM_NKF_ProtectionCardSet;
    result = nobis_sworld_cstoken
      (nb, params.cs, module, slot, &params.lt);
  } else if (params.sc) {
    params.f |= NFKM_NKF_ProtectionPassPhrase;
    result = nobis_sworld_sctoken
      (nb, params.sc, module, &params.lt);
  } else params.f |= NFKM_NKF_ProtectionModule;

  if (!result) {
    M_ByteBlock identbb;
    va_list args;

    va_start(args, ident);
    identbb.len = vsnprintf(NULL, 0, ident, args);
    va_end(args);
    if ((identbb.ptr = nobis_malloc(nb, identbb.len + 1))) {
      NFKM_Key kinfo;
      NFKM_ModuleInfo *minfo = NULL;
      int i;
      char *split;
      memset(&kinfo, 0, sizeof(kinfo));
      kinfo.v = 8;
      kinfo.appname = (char*)appname;
      kinfo.ident   = (char*)identbb.ptr;
      kinfo.n_entries = nentries;
      kinfo.entries   = entries;

      va_start(args, ident);
      vsnprintf((char*)identbb.ptr, identbb.len + 1, ident, args);
      va_end(args);
      if ((split = strchr((char*)identbb.ptr, ','))) {
        *split = '\0';
        kinfo.ident   = ++split;
        kinfo.appname = (char*)identbb.ptr;
      }

      for (i = 0; i < nb->world->n_modules; ++i)
        if ((!module || (nb->world->modules[i]->module == module)) &&
            (nb->world->modules[i]->state !=
             ModuleState_UnusedTableEntry)) {
          minfo = nb->world->modules[i];
          break;
        }

      if (seeident)
        kinfo.identseeinteg = (char*)seeident;
      if (!mc || !minfo ||
          !(result = NOBIS_CHK_STATUS
            (nb, NFKM_newkey_writecert,
             (nb->apph, nb->conn, minfo,
              kprv, mc, &kinfo, NULL)))) {
        if (!(result = NOBIS_CHK_STATUS
              (nb, NFKM_newkey_makeblobsx,
               (nb->apph, nb->conn, nb->world,
                &params, &kinfo, NULL)))) {
          result = NOBIS_CHK_STATUS(nb, NFKM_recordkey,
                                    (nb->apph, &kinfo, NULL));
          nobis_freebb(nb, &kinfo.privblobrecov);
          nobis_freebb(nb, &kinfo.privblob);
          nobis_freebb(nb, &kinfo.pubblob);
        }
        NFKM_freecert(nb->apph, &kinfo, NULL);
      }
      nobis_free(nb, identbb.ptr);
    } else result = NOBIS_STATUS_NOMEM;
  }
  NFKM_freecardset(nb->apph, (NFKM_CardSet*)params.cs, NULL);
  NFKM_freesoftcard(nb->apph, (NFKM_SoftCard*)params.sc, NULL);
  nobis_destroy(nb, params.lt);
  return result;
}

M_Status
nobis_sworld_keytoken(struct nobis *nb, unsigned flags,
                      M_ModuleID module, M_SlotID slot,
                      NFKM_Key *kinfo, M_KeyID *token)
{
  M_Status result = Status_OK;
  if (!(flags & nobis_keyi_nosoftcard) &&
      kinfo->flags & Key_flags_ProtectionPassPhrase) {
    NFKM_SoftCard *sc;
    if (!(result = NOBIS_CHK_STATUS
          (nb, NFKM_findsoftcard_hash,
           (nb->apph, &kinfo->softcard, &sc, NULL)))) {
      result = nobis_sworld_sctoken(nb, sc, module, token);
      NFKM_freesoftcard(nb->apph, sc, NULL);
    }
  } else if (!(flags & nobis_keyi_nocardset) &&
             kinfo->flags & Key_flags_ProtectionCardSet) {
    NFKM_CardSet *cs;
    if (!(result = NOBIS_CHK_STATUS
          (nb, NFKM_findcardset,
           (nb->apph, &kinfo->cardset, &cs, NULL)))) {
      result = nobis_sworld_cstoken(nb, cs, module, slot, token);
      NFKM_freecardset(nb->apph, cs, NULL);
    }
  } else *token = 0;
  return result;
}

M_Status
nobis_sworld_loadkp(struct nobis *nb, unsigned flags,
                    M_KeyID *kpub, M_KeyID *kprv, NFKM_Key **kinfo,
                    M_ModuleID module, M_SlotID slot,
                    const char *appname, const char *ident, ...)
{
  M_Status result;
  unsigned ident_len;
  va_list args;

  va_start(args, ident);
  ident_len = vsnprintf(NULL, 0, ident, args);
  va_end(args);

  if (ident_len > 0) {
    NFKM_Key *k_info = NULL;
    va_start(args, ident);
    if (!(result = nobis__sworld_keyinfo
          (nb, &k_info, flags, appname, ident_len, ident, args))) {
      if (!result && kprv) {
        *kprv = 0;
        result = NFKM_findexistingobject
          (nb->world, module, &k_info->hash, kprv);
        if (result == Status_KeyNotLoaded) {
          if (k_info->privblob.ptr) {
            M_KeyID token = 0;
            if (!(result = nobis_sworld_keytoken
                  (nb, flags, module, slot, k_info, &token)))
              result = NOBIS_CHK_STATUS
                (nb, NFKM_cmd_loadblob,
                 (nb->apph, nb->conn, module, &k_info->privblob,
                  token, kprv, NULL, NULL));
            /* TODO: create a cache of tokens which were not fetched
             * using existing objects and destroy them when the
             * nobis object gets cleaned up.  For not leaking the
             * tokens seems better than detroying them and rendering
             * non-persistent cardset protected keys useless. */
          } else if (!(flags & nobis_keyi_quietprv))
            NOBIS_CHK_NULL(nb, k_info->privblob.ptr,
                           NFKM_cmd_loadblob,
                           "no private blob available.");
          result = Status_OK;
        } else if (!result) {
          /* Existing objects get duplicated so that subsequent
           * requests to load them won't return destroyed handles. */
          M_Command command = {0};
          M_Reply   reply;
          command.cmd = Cmd_Duplicate;
          command.args.duplicate.key = *kprv;
          if (!(result = nobis_transact(nb, 0, &command, &reply))) {
            *kprv = reply.reply.duplicate.newkey;
            nobis_freereply(nb, &reply);
          }
        } else nobis_check_status(nb, __FILE__, __LINE__, result,
                                  "NFKM_findexistingobject");
      } else if (kprv) *kprv = 0;

      if (!result && kpub) {
        *kpub = 0;
        if (k_info->pubblob.ptr) {
          result = NOBIS_CHK_STATUS
            (nb, NFKM_cmd_loadblob,
             (nb->apph, nb->conn, module, &k_info->pubblob, 0,
              kpub, NULL, NULL));
        } else if (!(flags & nobis_keyi_quietpub))
          NOBIS_CHK_NULL(nb, k_info->pubblob.ptr, NFKM_cmd_loadblob,
                         "no public blob available.");
      } else if (kpub) *kpub = 0;

      if (result) {
        if (kpub)
          nobis_destroy(nb, *kpub);
        if (kprv)
          nobis_destroy(nb, *kprv);
      }
      if (!result && kinfo)
        *kinfo = k_info;
      else nobis_sworld_freekeyinfo(nb, k_info);
    }
    va_end(args);
  } else result = nobis_fail_msg(nb, __func__, Status_InvalidParameter,
                                 "empty ident not allowed.");
  return result;
}

static M_Status
nobis__cmd_mergekeys(struct nobis *nb, const char *file, unsigned line,
                     M_KeyID *mergedkey, M_KeyID *keys, unsigned n_keys)
{
  M_Status result;
  M_Command command = {0};
  M_Reply   reply;
  command.cmd = Cmd_MergeKeyIDs;
  command.args.mergekeyids.n_keys = n_keys;
  command.args.mergekeyids.keys = keys;
  if (!(result = nobis__transact
        (nb, file, line, 0, &command, &reply))) {
    if (mergedkey)
      *mergedkey = reply.reply.mergekeyids.newkey;
    else nobis_destroy(nb, reply.reply.mergekeyids.newkey);
    nobis_freereply(nb, &reply);
  }
  return result;
}

M_Status
nobis__merge_loadblob(struct nobis *nb, const char *file, int line,
                      M_KeyID *mergedkey, const M_ByteBlock *blob,
                      M_KeyID token,
                      M_ModuleID *modules, unsigned n_modules)
{
  M_Status result = Status_OK;
  M_ModuleID *alloc_modules = NULL;
  M_KeyID *keys = NULL;
  unsigned n_keys = n_modules;
  unsigned index;

  /* When n_modules is zero, attempt to construct a list of modules to
   * use, preferably using the NFKM_WorldInfo structure in the nobis
   * abstraction but if that's absent use Cmd_NewEnquiry commands to
   * exclude failed or absent modules. */
  if (!n_keys) {
    if (nb->world) {
      if ((alloc_modules = nobis_malloc
           (nb, sizeof(M_ModuleID) * n_keys))) {
          modules = alloc_modules;
          for (index = 0; (int)index < nb->world->n_modules; ++index)
            if (nb->world->modules[index]->state == ModuleState_Usable)
              modules[n_keys++] = nb->world->modules[index]->module;
      } else result = NOBIS_STATUS_NOMEM;
    } else {
      M_Command command = {0};
      M_Reply   reply;
      command.cmd= Cmd_NewEnquiry;
      command.args.newenquiry.module = 0;
      command.args.newenquiry.version = EnqVer_Six;
      if (!(result = nobis__transact
            (nb, file, line, 0, &command, &reply))) {
        n_keys = reply.reply.newenquiry.module;
        nobis_freereply(nb, &reply);

        if ((alloc_modules = nobis_malloc
             (nb, sizeof(M_ModuleID) * n_keys))) {
          unsigned count = 0;
          modules = alloc_modules;

          for (index = 0; !result && index < n_keys; ++index) {
            memset(&command, 0, sizeof(command));
            command.cmd= Cmd_NewEnquiry;
            command.args.newenquiry.module = index;
            command.args.newenquiry.version = EnqVer_Six;
            if (!(result = nobis__transact
                  (nb, file, line, 0, &command, &reply))) {
              if (!(reply.reply.newenquiry.flags &
                    (Cmd_NewEnquiry_Reply_flags_Failed |
                     Cmd_NewEnquiry_Reply_flags_NotPresent)))
                modules[count++] = index;
              nobis_freereply(nb, &reply);
            }
          }
          n_keys = count;
        } else result = NOBIS_STATUS_NOMEM;
      }
    }
  }

  /* Attempt to load the blob on each module not specifically know
   * by the NFKM_WorldInfo structure to be unusable (for example
   * because it's not in the correct security world). */
  if (!result) {
    if ((keys = nobis_malloc(nb, sizeof(*keys) * n_modules))) {
      unsigned count = 0;
      for (index = 0; !result && index < n_keys; ++index)
        result = NOBIS_CHK_STATUS
          (nb, NFKM_cmd_loadblob,
           (nb->apph, nb->conn,
            (modules && n_modules) ? modules[index] : index,
            blob, token, &keys[count++], NULL, NULL));

      if (!result)
        result = nobis__cmd_mergekeys
          (nb, file, line, mergedkey, keys, count);
      nobis_free(nb, keys);
    } else result = nobis_check_status
             (nb, file, line, NOBIS_STATUS_NOMEM, __func__);
  }
  nobis_free(nb, alloc_modules);
  return result;
}

M_Status
nobis__merge_destroy(struct nobis *nb, const char *file, int line,
                     M_KeyID mergedkey)
{
  M_Status result = Status_OK;
  if (mergedkey) {
    M_Command command = {0};
    M_Reply   reply;
    command.cmd = Cmd_UpdateMergedKey;
    command.args.updatemergedkey.flags =
      Cmd_UpdateMergedKey_Args_flags_ListWorking |
      Cmd_UpdateMergedKey_Args_flags_ListNonworking;
    command.args.updatemergedkey.mkey = mergedkey;
    if (!(result = nobis__transact
          (nb, file, line, 0, &command, &reply))) {
      unsigned index;
      unsigned n_keys = reply.reply.updatemergedkey.n_keys;
      M_KeyID *keys = reply.reply.updatemergedkey.keys;
      nobis__destroy(nb, file, line, mergedkey);
      for (index = 0; index < n_keys; ++index)
        nobis__destroy(nb, file, line, keys[index]);
      nobis_freereply(nb, &reply);
    }
  }
  return result;
}

static FILE *
nobis__vfopenf(const char *mode, unsigned fname_len,
               const char *fname, va_list args)
{
  FILE *result = NULL;
  if (fname_len > 0) {
    char *buffer = malloc(fname_len + 1);
    if (buffer) {
      vsnprintf(buffer, fname_len + 1, fname, args);
      result = fopen(buffer, mode);
      free(buffer);
    } else perror("malloc");
  }
  return result;
}

FILE *
nobis_fopenf(const char *mode, const char *fname, ...)
{
  FILE *result = NULL;
  int fname_len;
  va_list args;
  va_start(args, fname);
  fname_len = vsnprintf(NULL, 0, fname, args);
  va_end(args);

  va_start(args, fname);
  result = nobis__vfopenf(mode, fname_len, fname, args);
  va_end(args);
  return result;
}

M_Status
nobis_file2bb(struct nobis *nb, M_ByteBlock *dest,
              const char *fname, ...)
{
  M_Status result = Status_OK;
  FILE *in = NULL;
  int fname_len;
  va_list args;
  va_start(args, fname);
  fname_len = vsnprintf(NULL, 0, fname, args);
  va_end(args);

  va_start(args, fname);
  in = nobis__vfopenf("rb", fname_len, fname, args);
  va_end(args);
  if (in) {
    fseek(in, 0L, SEEK_END);
    dest->len = ftell(in);
    fseek(in, 0L, SEEK_SET);
    if ((dest->ptr = nobis_malloc(nb, dest->len))) {
      if (fread(dest->ptr, 1, dest->len, in) != dest->len)
        result = nobis_fail_msg
          (nb, __func__, Status_OSErrorErrno,
           "fread `%s': %s", fname, strerror(errno));
    } else result = NOBIS_STATUS_NOMEM;
    if (result) {
      nobis_free(nb, dest->ptr);
      dest->ptr = NULL;
      dest->len = 0;
    }
    fclose(in);
  } else result = nobis_fail_msg
           (nb, __func__, Status_OSErrorErrno,
            "fopen `%s': %s", fname, strerror(errno));
  /* TODO: correct the above to deal with formatted file names */
  return result;
}

M_Status
nobis_bb2file(struct nobis *nb, M_ByteBlock *src, const char *mode,
              const char *fname, ...)
{
  M_Status result = Status_OK;
  FILE *out = NULL;
  int fname_len;
  char *fname_buf;
  va_list args;
  va_start(args, fname);
  fname_len = vsnprintf(NULL, 0, fname, args);
  va_end(args);

  if (fname_len < 0)
    fname_len = 1024;
  if ((fname_buf = nobis_malloc(nb, fname_len + 1))) {
    va_start(args, fname);
    vsnprintf(fname_buf, fname_len + 1, fname, args);
    va_end(args);

    if ((out = nobis_fopenf(mode ? mode : "wb", "%s", fname_buf))) {
      if (fwrite(src->ptr, 1, src->len, out) != src->len)
        result = nobis_fail_msg
          (nb, __func__, Status_OSErrorErrno,
           "fwrite `%s': %s", fname_buf, strerror(errno));
      fclose(out);
    } else result = nobis_fail_msg
             (nb, __func__, Status_OSErrorErrno,
              "fopen `%s': %s", fname_buf, strerror(errno));
    nobis_free(nb, fname_buf);
  } else result = NOBIS_STATUS_NOMEM;
  return result;
}

/* The lakh parameter must either be a NULL pointer or must have
 * already been initialized with NFKM_loadadminkeys_begin(). */
static M_Status
nobis__dsee_certs(struct nobis *nb, M_ModuleID module, M_SlotID slot,
                  NFKM_LoadAdminKeysHandle *lakh,
                  M_CertificateList** certs_out) {
  M_Status result = Status_OK;

  if (!lakh || nb->world->flags & WorldInfo_flags_SEEDebugForAll) {
    *certs_out = NULL;
  } else {
    M_Certificate* current = NULL;

    if (nb->world->flags & WorldInfo_flags_SEEDebug) {
      if (!nobis_sworld_admin_load
          (nb, module, slot, lakh, NFKM_LTDSEE, -1)) {
        M_KeyID kdsee;
        if (!(result = NOBIS_CHK_STATUS
              (nb, NFKM_loadadminkeys_getkey,
               (*lakh, NFKM_KDSEE, &kdsee)))) {
          if (!(result = NOBIS_CHK_STATUS
                (nb, NFKM_cert_add, (nb->apph, 2, &current, 0,
                                     certs_out, NULL)))) {
            if (!(result = NOBIS_CHK_STATUS
                  (nb, NFKM_cert_setdelg,
                   (nb->apph, nb->world, current + 1,
                    KeyMgmtEntType_CertDelgDSEEbNSO, NULL)))) {
              current->keyhash = nb->world->hkdsee;
              current->type    = CertType_SigningKey;
              current->body.signingkey.key = kdsee;
              (*certs_out)->n_certs += 2;
            }
            if (result)
              NFastApp_Free_CertificateList
                (nb->apph, NULL, NULL, *certs_out);
          }
        }
      }
    } else {
      if (!(result = nobis_sworld_admin_load
            (nb, module, slot, lakh, NFKM_LTNSO, -1))) {
        M_KeyID kdsee;
        if (!(result = NOBIS_CHK_STATUS
              (nb, NFKM_loadadminkeys_getkey,
               (*lakh, NFKM_KDSEE, &kdsee)))) {
          if (!(result = NOBIS_CHK_STATUS
                (nb, NFKM_cert_add,
                 (nb->apph, 2, &current, 0, certs_out, NULL)))) {
            current->keyhash = nb->world->hknso;
            current->type    = CertType_SigningKey;
            current->body.signingkey.key = kdsee;
            (*certs_out)->n_certs += 1;
          }
        }
      }
    }
  }
  return result;
}

M_Status
nobis_seew_create(struct nobis *nb, M_ModuleID module, M_SlotID slot,
                  int debug, const M_ByteBlock* user_data,
                  M_Word *initstat, M_KeyID *seew) {
  M_Status result;
  M_CertificateList* certs = NULL;
  NFKM_ModuleInfo* minfo = NULL;
  NFKM_SlotInfo*   sinfo = NULL;

  if (!(result = nobis__module_slot
        (nb, module, slot, &minfo, &sinfo))) {
    NFKM_LoadAdminKeysHandle lakh;
    if (!(result = NOBIS_CHK_STATUS
          (nb, NFKM_loadadminkeys_begin,
           (nb->apph, nb->conn, &lakh, minfo, NULL)))) {
      if (!debug || (!(result = nobis__dsee_certs
                       (nb, module, slot, &lakh, &certs)))) {
        M_KeyID ud;
        if (!(result = nobis_loadbuffer
              (nb, module, 0, user_data, &ud))) {
          M_Command command;
          M_Reply   reply;

          memset(&command, 0, sizeof(command));
          command.cmd = Cmd_CreateSEEWorld;
          command.args.createseeworld.buffer = ud;
          if (debug) {
            command.args.createseeworld.flags |=
              Cmd_CreateSEEWorld_Args_flags_EnableDebug;
            if (certs != NULL) {
              command.certs = certs;
              command.flags |= Command_flags_certs_present;
              certs = NULL;
            }
          }
          if (!(result = nobis_transact(nb, nobis_tx_freecmd,
                                        &command, &reply))) {
            if (initstat)
              *initstat = reply.reply.createseeworld.initstatus;
            *seew = reply.reply.createseeworld.worldid;
          }
          nobis_destroy(nb, ud);
        }
        if (certs)
          NFastApp_Free_CertificateList(nb->apph, NULL, NULL, certs);
      }
      NFKM_loadadminkeys_done(lakh);
    }
  }
  return result;
}

M_Status
nobis_seew_getpublished(struct nobis *nb, M_ModuleID module,
                        const char *name, M_KeyID *seew)
{
  M_Status result;
  M_KeyID id = 0;
  M_Command command = {0};
  M_Reply   reply = {0};

  command.cmd = Cmd_GetPublishedObject;
  command.args.getpublishedobject.module = module;
  command.args.getpublishedobject.name.ptr = (char*)name;

  result = nobis_transact(nb, 0, &command, &reply);
  if (!result) {
    if (reply.reply.getpublishedobject.flags
        & Cmd_GetPublishedObject_Reply_flags_object_present)
      id = *reply.reply.getpublishedobject.object;
    else
      result = Status_SEEWorldNotPresent;
    nobis_freereply(nb, &reply);

    if (!result) {
       /* publish oject found.  make sure it is really loaded; when
        * a module is cleared invalid object may still be published */
      memset(&command, 0, sizeof(command));
      memset(&reply, 0, sizeof(reply));
      command.cmd = Cmd_GetWhichModule;
      command.args.getwhichmodule.key = id;
      result = nobis_transact(nb, 0, &command, &reply);
      if (!result && !(result = reply.status)) {
        *seew = id;
        nobis_freereply(nb, &reply);
      } else {
        nobis_destroy(nb, id);
      }
    }
  }

  return result;
}

M_Status
nobis_seew_trace(struct nobis *nb, M_KeyID seew, FILE *f)
{
  M_Status result;
  M_Command command;
  M_Reply   reply;

  memset(&command, 0, sizeof(command));
  command.cmd = Cmd_TraceSEEWorld;
  command.args.traceseeworld.worldid = seew;
  if (!(result = nobis_transact(nb, 0, &command, &reply))) {
    fwrite(reply.reply.traceseeworld.data.ptr,
           reply.reply.traceseeworld.data.len, 1, f);
    if (reply.reply.traceseeworld.flags &
        Cmd_TraceSEEWorld_Reply_flags_Failed)
      nobis_fail_msg(nb, "SEE", 0, "trace reports machine failed");
    nobis_freereply(nb, &reply);
  }
  return result;
}

#endif

enum { nobis__chunk_size = 6 * 1024 };

M_Status
nobis__loadbuffer(struct nobis *nb, const char *file, unsigned line,
                  M_ModuleID module, M_KeyID enckey,
                  const M_ByteBlock *bb, M_KeyID *buffid)
{
  M_Status result = Status_OK;
  M_Command command = {0};
  M_Reply reply;
  unsigned written = 0;
  M_EncryptionParams eparams;
  M_CipherText ctext = {0};
  NF_Unmarshal_Context uctx;
  NF_Free_Context fctx;
  nobis_setctx_unmarbb(nb, bb, &uctx);
  nobis_setctx_free(nb, &fctx);

  command.cmd = Cmd_CreateBuffer;
  command.args.createbuffer.module = module;
  command.args.createbuffer.size = bb->len;
  if (enckey && !(result = NOBIS_CHK_STATUS
                  (nb, NF_Unmarshal_CipherText, (&uctx, &ctext)))) {
    command.args.createbuffer.flags =
      Cmd_CreateBuffer_Args_flags_params_present;
    command.args.createbuffer.params = &eparams;

    memset(&eparams, 0, sizeof(eparams));
    eparams.key = enckey;
    eparams.iv.mech = ctext.mech;
    eparams.iv.iv = ctext.iv;
    bb = &ctext.data.generic64.cipher;
  }

  if (!result &&
      !(result = nobis__transact
        (nb, file, line, 0, &command, &reply))) {
    *buffid = reply.reply.createbuffer.id;
    nobis_freereply(nb, &reply);

    while (written < bb->len) {
      unsigned chunk = nobis__chunk_size;
      memset(&command, 0, sizeof(command));
      command.cmd= Cmd_LoadBuffer;
      command.args.loadbuffer.id = *buffid;
      if (chunk >= bb->len - written) {
        chunk = bb->len - written;
        command.args.loadbuffer.flags |=
          Cmd_LoadBuffer_Args_flags_Final;
      }
      command.args.loadbuffer.chunk.ptr = bb->ptr + written;
      command.args.loadbuffer.chunk.len = chunk;

      if ((result = nobis__transact
           (nb, file, line, 0, &command, &reply)))
        break;
      nobis_freereply(nb, &reply);
      written += chunk;
    }

    if (result)
      nobis__destroy(nb, file, line, *buffid);
  }
  NF_Free_CipherText(&fctx, &ctext);
  return result;
}

NOBIS_MARSHAL_FUNCTION(ACL)
NOBIS_MARSHAL_FUNCTION(KeyData)
NOBIS_MARSHAL_FUNCTION(CipherText)

M_Status
nobis_acl_template(struct nobis *nb, M_ModuleID module, const M_ACL *acl,
                   const M_AppData* ad, M_DeriveMech dmech, M_KeyID* out)
{
  M_Status result = Status_OK;
  M_ByteBlock nested_acl;
  M_AppData appempty = {{0}};
  if (!ad)
    ad = &appempty;

  if (!(result = nobis_marshal_ACL(nb, &nested_acl, acl))) {
    M_ByteBlock marshappbb = { sizeof(*ad), (void*)ad };
    M_Command command = {0};
    M_Reply reply;
    M_Action actions[3] = {{0}};
    M_PermissionGroup group = {0};
    group.n_actions = sizeof(actions)/sizeof(*actions);
    group.actions = actions;

    // This is the most important purpose of this key
    actions[0].type = Act_DeriveKey;
    actions[0].details.derivekey.role = DeriveRole_TemplateKey;
    actions[0].details.derivekey.mech = dmech;

    // Permit probes of this ACL, which is as sensitive as this code
    actions[1].type = Act_OpPermissions;
    actions[1].details.oppermissions.perms =
      Act_OpPermissions_Details_perms_GetACL;

    // This permits a complex export and would be a serious problem
    // for key material, but a template "key" is actually an ACL
    // which is identified by its hash and is safe to reveal.
    actions[2].type = Act_MakeBlob;
    actions[2].details.makeblob.flags =
      Act_MakeBlob_Details_flags_AllowKmOnly |
      Act_MakeBlob_Details_flags_AllowNonKm0 |
      Act_MakeBlob_Details_flags_AllowNullKmToken;

    command.cmd = Cmd_Import;
    command.args.import.module = module;
    command.args.import.data.type = KeyType_DKTemplate;
    command.args.import.data.data.dktemplate.nested_acl = nested_acl;
    command.args.import.data.data.dktemplate.appdata = marshappbb;
    command.args.import.acl.n_groups = 1;
    command.args.import.acl.groups = &group;
    if (!(result = nobis_transact
          (nb, nobis_tx_fips, &command, &reply))) {
      *out = reply.reply.import.key;
      nobis_freereply(nb, &reply);
    }
    nobis_freebb(nb, &nested_acl);
  }

  return result;
}

M_Status
nobis_generate_wrapper(struct nobis *nb, M_ModuleID module,
                       M_KeyID *wrap_key)
{
  M_Status result;
  M_Command command;
  M_Reply reply;
  M_ACL acl;
  M_PermissionGroup group;
  M_Action actions[2];

  memset(&acl, 0, sizeof(acl));
  acl.n_groups = 1;
  acl.groups = &group;
  memset(&group, 0, sizeof(group));
  group.n_actions = sizeof(actions) / sizeof(*actions);
  group.actions = actions;
  memset(&actions, 0, sizeof(actions));
  actions[0].type = Act_DeriveKey;
  actions[0].details.derivekey.role = DeriveRole_WrapKey;
  actions[0].details.derivekey.mech = DeriveMech_Any;
  actions[1].type = Act_OpPermissions;
  actions[1].details.oppermissions.perms =
    Act_OpPermissions_Details_perms_Encrypt |
    Act_OpPermissions_Details_perms_Decrypt |
    Act_OpPermissions_Details_perms_GetACL;

  memset(&command, 0, sizeof(command));
  command.cmd = Cmd_GenerateKey;
  command.args.generatekey.module = module;
  command.args.generatekey.acl = acl;
  command.args.generatekey.params.type = KeyType_Rijndael;
  command.args.generatekey.params.params.random.lenbytes = 16;
  if (!(result = nobis_transact(nb, nobis_tx_fips, &command, &reply))) {
    *wrap_key = reply.reply.generatekey.key;
    nobis_freereply(nb, &reply);
  } else *wrap_key = 0;
  return result;
}

M_Status
nobis_import_private_bulk(struct nobis *nb, M_ModuleID module,
                          M_KeyData *kdprv, M_KeyID tplt,
                          M_KeyID wrap, M_KeyID *kprv)
{
  M_Status result = Status_OK;
  M_Command command;
  M_Reply reply;
  M_ByteBlock mdata = {0};
  M_Action actions[2] = {{0}};
  M_KeyID keys[] = {tplt, 0, wrap};
  M_PermissionGroup group = {0};
  if (kprv) *kprv = 0;

  if (!(result = nobis_marshal_KeyData(nb, &mdata, kdprv))) {
    /* ECB has none of its usual disadvantages here because
     * the key material should look random and there should be
     * no way for an attacker to get the cipher text without
     * also getting the key material itself.  The only reason
     * to encrypt at all is to satisfy the requirements
     * imposed by the FIPS 140 standard in StrictFIPS140
     * security worlds. */
    memset(&command, 0, sizeof(command));
    command.cmd = Cmd_Encrypt;
    command.args.encrypt.key = wrap;
    command.args.encrypt.mech = Mech_RijndaelmECBpPKCS5;
    command.args.encrypt.plain.type = PlainTextType_Bytes;
    command.args.encrypt.plain.data.bytes.data = mdata;
    if (!(result = nobis_transact(nb, 0, &command, &reply))) {
      nobis_free(nb, mdata.ptr); mdata.ptr = NULL;
      result = nobis_marshal_CipherText
        (nb, &mdata, &reply.reply.encrypt.cipher);
      nobis_freereply(nb, &reply);
      if (!result) {
        group.n_actions = 1;
        group.actions = actions;
        actions[0].type = Act_DeriveKey;
        actions[0].details.derivekey.role = DeriveRole_BaseKey;
        actions[0].details.derivekey.mech =
          DeriveMech_DecryptMarshalled;
        memset(&command, 0, sizeof(command));
        command.cmd = Cmd_Import;
        command.args.import.module = module;
        command.args.import.data.type = KeyType_Wrapped;
        command.args.import.data.data.random.k = mdata;
        command.args.import.acl.n_groups = 1;
        command.args.import.acl.groups = &group;
        if (!(result = nobis_transact
              (nb, nobis_tx_fips, &command, &reply))) {
          keys[1] = reply.reply.import.key;
          nobis_freereply(nb, &reply);

          memset(&command, 0, sizeof(command));
          command.cmd = Cmd_DeriveKey;
          command.args.derivekey.mech = DeriveMech_DecryptMarshalled;
          command.args.derivekey.n_keys =
            sizeof(keys) / sizeof(*keys);
          command.args.derivekey.keys = keys;
          if (!(result = nobis_transact(nb, 0, &command, &reply))) {
            if (kprv) *kprv = reply.reply.derivekey.key;
            else nobis_destroy(nb, reply.reply.derivekey.key);
            nobis_freereply(nb, &reply);
          }

          nobis_destroy(nb, keys[1]);
        }
      }
    }
    nobis_free(nb, mdata.ptr);
  }
  return result;
}

M_Status
nobis_import_private(struct nobis *nb, M_ModuleID module,
                     M_KeyData *kdprv, M_ACL *aclprv,
                     M_AppData *adprv, M_KeyID *kprv)
{
  M_Status result = Status_OK;
  M_KeyID template;

  if (!(result = nobis_acl_template
        (nb, module, aclprv, adprv,
         DeriveMech_DecryptMarshalled, &template))) {
    M_KeyID wrapper;

    if (!(result = nobis_generate_wrapper(nb, module, &wrapper))) {
      result = nobis_import_private_bulk
        (nb, module, kdprv, template, wrapper, kprv);
      nobis_destroy(nb, wrapper);
    }
    nobis_destroy(nb, template);
  }
  return result;
}

M_Status
nobis__import_public(struct nobis *nb, const char *file, unsigned line,
                     M_ModuleID module, const M_KeyData *kdpub, const M_ACL *aclpub,
                     const M_AppData *adpub, M_KeyID *kpub)
{
  M_Status result = Status_OK;
  M_Command command = {0};
  M_Reply reply;
  M_ACL aclpub_default = {0};
  M_Action actions[2] = {{0}};
  M_PermissionGroup group = {0};
  M_AppData appempty ={{0}};
  if (!adpub) adpub = &appempty;
  if (kpub) *kpub  = 0;

  if (!aclpub) { /* Use a permissive default public key ACL */
    aclpub_default.n_groups = 1;
    aclpub_default.groups = &group;
    group.n_actions = sizeof(actions) / sizeof(*actions);
    group.actions = actions;
    actions[0].type = Act_OpPermissions;
    actions[0].details.oppermissions.perms =
      Act_OpPermissions_Details_perms_DuplicateHandle |
      Act_OpPermissions_Details_perms_ExportAsPlain |
      Act_OpPermissions_Details_perms_GetAppData |
      Act_OpPermissions_Details_perms_SetAppData |
      Act_OpPermissions_Details_perms_ReduceACL |
      Act_OpPermissions_Details_perms_ExpandACL |
      Act_OpPermissions_Details_perms_GetACL;
    actions[1].type = Act_MakeBlob;
    actions[1].details.makeblob.flags =
      Act_MakeBlob_Details_flags_AllowKmOnly |
      Act_MakeBlob_Details_flags_AllowNonKm0 |
      Act_MakeBlob_Details_flags_AllowNullKmToken;

    /* This may need further tuning for additional key types. */
    if ((kdpub->type == KeyType_DSAPublic) ||
        (kdpub->type == KeyType_ECDSAPublic))
      actions[0].details.oppermissions.perms |=
        Act_OpPermissions_Details_perms_Verify;
    else if ((kdpub->type == KeyType_DHPublic) ||
             (kdpub->type == KeyType_ECDHPublic))
      actions[0].details.oppermissions.perms |=
        Act_OpPermissions_Details_perms_Encrypt;
    else actions[0].details.oppermissions.perms |=
           Act_OpPermissions_Details_perms_UseAsBlobKey |
           Act_OpPermissions_Details_perms_Encrypt |
           Act_OpPermissions_Details_perms_Verify;
    aclpub = &aclpub_default;
  }

  command.cmd = Cmd_Import;
  command.args.import.module = module;
  command.args.import.data   = *kdpub;
  command.args.import.acl    = *aclpub;
  command.args.import.appdata= *adpub;
  if (!(result = nobis__transact
        (nb, file, line, nobis_tx_fips, &command, &reply))) {
    if (kpub)
      *kpub = reply.reply.import.key;
    else nobis_destroy(nb, reply.reply.import.key);
    nobis_freereply(nb, &reply);
  }
  return result;
}

M_Status
nobis__export(struct nobis *nb, const char *file, unsigned line,
              M_ModuleID module, M_KeyID base, M_KeyID wrap,
              M_DeriveMech dmech, M_DeriveMech__DKParams *dkparams,
              M_ByteBlock *outbb)
{
  M_Status result = Status_OK;
  M_AppData ad = {{0}};
  M_ACL acl = {0};
  M_PermissionGroup group = {0};
  M_Action actions[1] = {{0}};
  M_KeyID keys[3] = {0, base, wrap};

  acl.n_groups = 1;
  acl.groups = &group;
  group.n_actions = sizeof(actions) / sizeof(*actions);
  group.actions = actions;
  actions[0].type = Act_OpPermissions;
  actions[0].details.oppermissions.perms =
    Act_OpPermissions_Details_perms_ExportAsPlain;
  if (!(result = nobis_acl_template
        (nb, module, &acl, &ad, dmech, &keys[0]))) {
    M_Command command = {0};
    M_Reply reply;
    memset(&command, 0, sizeof(command));
    command.cmd = Cmd_DeriveKey;
    command.args.derivekey.mech = dmech;
    command.args.derivekey.n_keys = sizeof(keys) / sizeof(*keys);
    command.args.derivekey.keys = keys;
    if (dkparams)
      command.args.derivekey.params = *dkparams;
    if (!(result = nobis__transact
          (nb, file, line, nobis_tx_signers, &command, &reply))) {
      M_KeyID k = reply.reply.derivekey.key;
      nobis_freereply(nb, &reply);

      memset(&command, 0, sizeof(command));
      command.cmd = Cmd_Export;
      command.args.export.key = k;
      if (!(result = nobis__transact
            (nb, file, line, 0, &command, &reply))) {
        if (outbb) {
          *outbb = reply.reply.export.data.data.random.k;
          memset(&reply.reply.export.data.data.random.k, 0,
                 sizeof(reply.reply.export.data.data.random.k));
        }
        nobis_freereply(nb, &reply);
      }
      nobis_destroy(nb, k);
    }
    nobis_destroy(nb, keys[0]);
  }

  return result;
}

M_Status
nobis__export_private(struct nobis *nb, const char *file, unsigned line,
                      M_ModuleID module, M_KeyID base_key, M_KeyID wrap_key,
                      M_KeyData *kdata)
{
  M_Status result = Status_OK;
  M_Command command;
  M_Reply reply;
  M_AppData ad = {{0}};
  M_ACL acl = {0};
  M_PermissionGroup group = {0};
  M_Action actions[1] = {{0}};
  M_KeyID keys[3] = {0, base_key, wrap_key};

  /* This ACL will belong to the encrypted key.  All it needs to
   * do is permit extraction so it can be decrypted. */
  acl.n_groups = 1;
  acl.groups = &group;
  group.n_actions = sizeof(actions) / sizeof(*actions);
  group.actions = actions;
  actions[0].type = Act_OpPermissions;
  actions[0].details.oppermissions.perms =
    Act_OpPermissions_Details_perms_ExportAsPlain;

  if (!(result = nobis_acl_template(nb, module, &acl,
        &ad, DeriveMech_EncryptMarshalled, &keys[0]))) {
    memset(&command, 0, sizeof(command));
    command.cmd = Cmd_DeriveKey;
    command.args.derivekey.mech = DeriveMech_EncryptMarshalled;
    command.args.derivekey.n_keys = sizeof(keys) / sizeof(*keys);
    command.args.derivekey.keys = keys;
    if (!(result = nobis__transact(nb, file, line, nobis_tx_signers,
          &command, &reply))) {
      M_KeyID k = reply.reply.derivekey.key;
      nobis_freereply(nb, &reply);

      /* Export the encrypted key material so it can be decrypted. */
      memset(&command, 0, sizeof(command));
      command.cmd = Cmd_Export;
      command.args.export.key = k;
      if (!(result = nobis_transact(nb, 0, &command, &reply))) {
        M_CipherText ctext;
        M_ByteBlock target = reply.reply.export.data.data.random.k;
        NF_Unmarshal_Context uctx;
        nobis_setctx_unmar(nb, target.ptr, target.len, &uctx);
        result = NOBIS_CHK_STATUS
          (nb, NF_Unmarshal_CipherText, (&uctx, &ctext));
        nobis_freereply(nb, &reply);

        if (!result) {
          memset(&command, 0, sizeof(command));
          command.cmd = Cmd_Decrypt;
          command.args.decrypt.key = wrap_key;
          command.args.decrypt.mech = Mech_Any;
          command.args.decrypt.cipher = ctext;
          command.args.decrypt.reply_type = PlainTextType_Bytes;
          if (!(result = nobis_transact(nb, 0, &command, &reply))) {
            target = reply.reply.decrypt.plain.data.bytes.data;
            nobis_setctx_unmar(nb, target.ptr, target.len, &uctx);
            result = NOBIS_CHK_STATUS
              (nb, NF_Unmarshal_KeyData, (&uctx, kdata));
            nobis_freereply(nb, &reply);
          }
        }
      }
      nobis_destroy(nb, k);
    }
    nobis_destroy(nb, keys[0]);
  }
  return result;
}

M_Status
nobis__export_public(struct nobis *nb, const char *file, unsigned line,
                     M_KeyID key, M_KeyData *kdata)
{
  M_Status  result = Status_OK;
  M_Reply   reply;
  M_Command command = {0};

  command.cmd = Cmd_Export;
  command.args.export.key = key;
  if (!(result = nobis__transact(nb, file, line, nobis_tx_signers,
        &command, &reply))) {
    *kdata = reply.reply.export.data;
    memset(&reply.reply.export.data, 0, sizeof(reply.reply.export.data));
    nobis_freereply(nb, &reply);
  }

  return result;
}

void
nobis_freeKeyData(struct nobis *nb, M_KeyData *kd)
{
  NF_Free_Context c;
  nobis_setctx_free(nb, &c);
  NF_Free_KeyData(&c, kd);
}

M_Status
nobis__digest(struct nobis *nb, const char *file, unsigned line,
              M_ModuleID module, M_Mech mech, unsigned chunk,
              const M_ByteBlock *in, M_ByteBlock *out)
{
  M_Status  result = Status_OK;
  M_Reply   reply;
  M_Command command = {0};
  if (!chunk)
    chunk = nobis__chunk_size;
  if (chunk >= in->len) {
    command.cmd = Cmd_Hash;
    command.args.hash.mech = mech;
    command.args.hash.plain.type = PlainTextType_Bytes;
    command.args.hash.plain.data.bytes.data = *in;
    if (!(result = nobis__transact
          (nb, file, line, 0, &command, &reply))) {
      M_ByteBlock copy = { 0, NULL };
      switch (reply.reply.hash.sig.mech) {
      case Mech_MD5Hash:
        copy.ptr = reply.reply.hash.sig.data.md5hash.h.bytes;
        copy.len = sizeof(reply.reply.hash.sig.data.md5hash.h.bytes);
        break;
      case Mech_SHA1Hash:
        copy.ptr = reply.reply.hash.sig.data.sha1hash.h.bytes;
        copy.len = sizeof(reply.reply.hash.sig.data.sha1hash.h.bytes);
        break;
      case Mech_SHA224Hash:
        copy.ptr = reply.reply.hash.sig.data.sha224hash.h.bytes;
        copy.len = sizeof(reply.reply.hash.sig.data.sha224hash.h.bytes);
        break;
      case Mech_SHA256Hash:
        copy.ptr = reply.reply.hash.sig.data.sha256hash.h.bytes;
        copy.len = sizeof(reply.reply.hash.sig.data.sha256hash.h.bytes);
        break;
      case Mech_SHA384Hash:
        copy.ptr = reply.reply.hash.sig.data.sha384hash.h.bytes;
        copy.len = sizeof(reply.reply.hash.sig.data.sha384hash.h.bytes);
        break;
      case Mech_SHA512Hash:
        copy.ptr = reply.reply.hash.sig.data.sha512hash.h.bytes;
        copy.len = sizeof(reply.reply.hash.sig.data.sha512hash.h.bytes);
        break;
      default:
        result = Status_NotYetImplemented;
      }
      if (!result && copy.len && copy.ptr) {
        if (!out->ptr || (out->len < copy.len))
          out->ptr = nobis_malloc(nb, out->len = copy.len);
        if (out->ptr)
          memcpy(out->ptr, copy.ptr, out->len = copy.len);
        else result = NOBIS_STATUS_NOMEM;
      }
      nobis_freereply(nb, &reply);
    }
  } else {
    command.cmd = Cmd_ChannelOpen;
    command.args.channelopen.module = module;
    command.args.channelopen.type = ChannelType_Simple;
    command.args.channelopen.mode = ChannelMode_Sign;
    command.args.channelopen.mech = mech;
    if (!(result = nobis__transact
          (nb, file, line, 0, &command, &reply))) {
      M_KeyID channel = reply.reply.channelopen.idch;
      unsigned written = 0, done = 0;
      nobis_freereply(nb, &reply);
      do {
        unsigned writing = (in->len > written + chunk) ?
          chunk : in->len - written;
        memset(&command, 0, sizeof(command));
        command.cmd = Cmd_ChannelUpdate;
        command.args.channelupdate.idch = channel;
        command.args.channelupdate.input.ptr = in->ptr + written;
        command.args.channelupdate.input.len = writing;
        command.args.channelupdate.flags =
          writing ? 0 : Cmd_ChannelUpdate_Args_flags_final;
        if (!(result = nobis__transact
              (nb, file, line, 0, &command, &reply))) {
          if (!writing) {
            if (!out->ptr ||
                (out->len < reply.reply.channelupdate.output.len)) {
              *out = reply.reply.channelupdate.output;
              memset(&reply.reply.channelupdate.output, 0,
                     sizeof(reply.reply.channelupdate.output));
            } else memcpy
                     (out->ptr, reply.reply.channelupdate.output.ptr,
                      out->len = reply.reply.channelupdate.output.len);
            done = 1;
          } else written += writing;
          nobis_freereply(nb, &reply);
        }
      } while (!result && !done);
      nobis_destroy(nb, channel);
    }
  }
  return result;
}

M_Status
nobis_sha256(struct nobis *nb, const M_ByteBlock *input,
             M_ByteBlock *output)
{ return nobis_digest(nb, 0, Mech_SHA256Hash, 0, input, output); }

M_Status
nobis_mgf1(struct nobis *nb, nobis_hashfn_t hashfn,
           const M_ByteBlock *input, M_ByteBlock *output)
{
  M_Status result = Status_OK;
  unsigned counter = 0;
  unsigned written = 0;
  unsigned char octets[4];
  M_ByteBlock value = { input->len + sizeof(octets), NULL };
  M_ByteBlock cbb = {0};

  if (!output->ptr) { /* ensure enough space to store output */
    output->ptr = nobis_malloc(nb, output->len);
    if (!output->ptr)
      result = NOBIS_STATUS_NOMEM;
  }

  if (!result) {
    if ((value.ptr = nobis_malloc(nb, value.len))) {
      memcpy(value.ptr, input->ptr, input->len);
    } else result = NOBIS_STATUS_NOMEM;
  }

  while (!result && written < output->len) {
    octets[0] = (counter >> 24) & 0xFF;
    octets[1] = (counter >> 16) & 0xFF;
    octets[2] = (counter >> 8) & 0xFF;
    octets[3] = counter & 0xFF;
    memcpy(value.ptr + input->len, octets, sizeof(octets));

    if (!(result = hashfn(nb, &value, &cbb))) {
      unsigned writes = (cbb.len > output->len - written) ?
        (output->len - written) : cbb.len;

      memcpy(output->ptr + written, cbb.ptr, writes);
      written += writes;
    }
  }

  nobis_free(nb, value.ptr);
  nobis_free(nb, cbb.ptr);
  return result;
}

M_Status
nobis__encrypt(struct nobis *nb, const char *file, unsigned line,
               M_KeyID kpub, M_Mech mech, unsigned chunk,
               M_IV *iv, const M_ByteBlock *in, M_ByteBlock *out)
{
  M_Status  result = Status_OK;
  M_Reply   reply;
  M_Command command = {0};
  if (!chunk)
    chunk = nobis__chunk_size;
  if (chunk >= in->len) {
    command.cmd = Cmd_Encrypt;
    command.args.encrypt.mech = mech;
    command.args.encrypt.key = kpub;
    command.args.encrypt.plain.type = PlainTextType_Bytes;
    command.args.encrypt.plain.data.bytes.data = *in;
    if (iv && iv->mech != Mech_Any) {
      command.args.encrypt.given_iv = iv;
      command.args.encrypt.flags |=
        Cmd_Encrypt_Args_flags_given_iv_present;
    }
    if (!(result = nobis_transact(nb, 0, &command, &reply))) {
      /* FIXME: do something clever with the ciphertext */
      nobis_freereply(nb, &reply);
    }
  } else {
    /* FIXME: do something with ChannelOpen/Update */
  }
  return result;
}

M_Status
nobis_rsa(struct nobis *nb, unsigned flags, M_KeyID k,
          const M_ByteBlock *in, M_ByteBlock *out)
{
  M_Status result = Status_OK;
  unsigned  txflags = (flags & nobis_rsa_signers) ?
    nobis_tx_signers : 0;
  M_Bignum  ctext = {0};
  M_Command command = {0};
  M_Reply   reply;
  M_Mech    mech;

  switch (flags & nobis_rsa_maskhash) {
  case nobis_rsa_sha512:
    mech = (flags & nobis_rsa_oaep) ?
      Mech_RSApPKCS1OAEPhSHA512 : Mech_RSAhSHA512pPKCS1;
    break;
  case nobis_rsa_sha256:
    mech = (flags & nobis_rsa_oaep) ?
      Mech_RSApPKCS1OAEPhSHA256 : Mech_RSAhSHA256pPKCS1;
    break;
  case nobis_rsa_sha1:
    mech = (flags & nobis_rsa_oaep) ?
      Mech_RSApPKCS1OAEP : Mech_RSAhSHA1pPKCS1;
    break;
  case nobis_rsa_md5:
    mech = (flags & nobis_rsa_oaep) ? Mech_Any : Mech_RSAhMD5pPKCS1;
    break;
  default:
    mech = (flags & nobis_rsa_oaep) ?
      Mech_RSApPKCS1OAEP : Mech_RSApPKCS1;
  }
  if (mech == Mech_Any)
    return nobis_fail_msg(nb, __func__, Status_InvalidParameter,
                          "OAEP not supported for hash");

  switch (flags & nobis_rsa_maskop) {
  case nobis_rsa_sign:
    command.cmd = Cmd_Sign;
    command.args.sign.key = k;
    command.args.sign.mech = mech;
    if ((flags & nobis_rsa_maskhash) == nobis_rsa_raw) {
      command.args.sign.plain.type = PlainTextType_Bignum;
      result = nobis_bignum_create
        (nb, &command.args.sign.plain.data.bignum.m, in);
      txflags |= nobis_tx_freecmd;
    } else {
      command.args.sign.plain.type = PlainTextType_Bytes;
      command.args.sign.plain.data.bytes.data = *in;
    }
    break;
  case nobis_rsa_verify:
    command.cmd = Cmd_Verify;
    command.args.verify.key = k;
    command.args.verify.mech = mech;
    if ((flags & nobis_rsa_maskhash) == nobis_rsa_raw) {
      command.args.verify.plain.type = PlainTextType_Bignum;
      result = nobis_bignum_create
        (nb, &command.args.verify.plain.data.bignum.m, in);
      txflags |= nobis_tx_freecmd;
    } else {
      command.args.verify.plain.type = PlainTextType_Bytes;
      command.args.verify.plain.data.bytes.data = *in;
    }
    command.args.verify.sig.mech = mech;
    result = nobis_bignum_create
      (nb, &command.args.verify.sig.data.rsappkcs1.m, out);
    break;
  case nobis_rsa_encrypt:
    command.cmd = Cmd_Encrypt;
    command.args.encrypt.key = k;
    command.args.encrypt.mech = mech;
    if ((flags & nobis_rsa_maskhash) == nobis_rsa_raw) {
      command.args.encrypt.plain.type = PlainTextType_Bignum;
      result = nobis_bignum_create
        (nb, &command.args.encrypt.plain.data.bignum.m, in);
      txflags |= nobis_tx_freecmd;
    } else {
      command.args.encrypt.plain.type = PlainTextType_Bytes;
      command.args.encrypt.plain.data.bytes.data = *in;
    }
    break;
  case nobis_rsa_decrypt:
    command.cmd = Cmd_Decrypt;
    command.args.decrypt.key = k;
    command.args.decrypt.mech = mech;
    command.args.decrypt.cipher.mech = mech;
    result = nobis_bignum_create(nb, &ctext, in);
    command.args.decrypt.cipher.data.rsappkcs1.m = ctext;
    command.args.decrypt.reply_type =
      ((flags & nobis_rsa_maskhash) == nobis_rsa_raw) ?
      PlainTextType_Bignum : PlainTextType_Bytes;
    break;
  default:
    return Status_InvalidParameter;
  }
  if (!result && !(result = nobis_transact
                   (nb, txflags, &command, &reply))) {
    switch (flags & nobis_rsa_maskop) {
    case nobis_rsa_sign:
      result = nobis_bignum_bytes
        (nb, &reply.reply.sign.sig.data.rsappkcs1.m, out);
      break;
    case nobis_rsa_verify: break;
    case nobis_rsa_encrypt:
      result = nobis_bignum_bytes
        (nb, &reply.reply.encrypt.cipher.data.rsappkcs1.m, out);
      break;
    case nobis_rsa_decrypt:
      if (reply.reply.decrypt.plain.type == PlainTextType_Bignum) {
        result = nobis_bignum_bytes
          (nb, &reply.reply.decrypt.plain.data.bignum.m, out);
      } else if (reply.reply.decrypt.plain.type ==
                 PlainTextType_Bytes) {
        *out = reply.reply.decrypt.plain.data.bytes.data;
        reply.reply.decrypt.plain.data.bytes.data.ptr = NULL;
        reply.reply.decrypt.plain.data.bytes.data.len = 0;
      } else {
        out->ptr = NULL;
        out->len = 0;
      }
    }
    nobis_freereply(nb, &reply);
  }
  nobis_bignum_cleanup(nb, &ctext);
  return result;
}

unsigned
nobis_rsa_mechflags(struct nobis *nb, M_Mech mech)
{
  unsigned result;
  switch (mech) {
  case Mech_RSApPKCS1:
    result = nobis_rsa_raw; break;
  case Mech_RSAhMD5pPKCS1:
    result = nobis_rsa_md5; break;
  case Mech_RSAhSHA1pPKCS1:
    result = nobis_rsa_sha1; break;
  case Mech_RSAhSHA224pPKCS1:
    result = nobis_rsa_sha224; break;
  case Mech_RSAhSHA256pPKCS1:
    result = nobis_rsa_sha256; break;
  case Mech_RSAhSHA384pPKCS1:
    result = nobis_rsa_sha384; break;
  case Mech_RSAhSHA512pPKCS1:
    result = nobis_rsa_sha512; break;
  case Mech_RSApPKCS1OAEP:
    result = nobis_rsa_raw | nobis_rsa_oaep; break;
  case Mech_RSApPKCS1OAEPhSHA224:
    result = nobis_rsa_sha224 | nobis_rsa_oaep; break;
  case Mech_RSApPKCS1OAEPhSHA256:
    result = nobis_rsa_sha256 | nobis_rsa_oaep; break;
  case Mech_RSApPKCS1OAEPhSHA384:
    result = nobis_rsa_sha384 | nobis_rsa_oaep; break;
  case Mech_RSApPKCS1OAEPhSHA512:
    result = nobis_rsa_sha512 | nobis_rsa_oaep; break;

  case Mech_RSAhRIPEMD160pPKCS1:
  case Mech_RSApPKCS1pPKCS11:
  case Mech_RSAhSHA1pPSS:
  case Mech_RSAhSHA256pPSS:
  case Mech_RSAhSHA384pPSS:
  case Mech_RSAhSHA512pPSS:
  case Mech_RSAhSHA224pPSS:
  case Mech_RSAhRIPEMD160pPSS:
  default:
    result = 0;
  }
  return result;
}

M_Status
nobis__keyinfo(struct nobis *nb, const char *file, unsigned line,
               M_KeyID k, M_KeyType *type, M_Word *nbits,
               M_KeyHash *hash)
{
  M_Status result;
  M_Command command = {0};
  M_Reply reply;
  command.cmd = Cmd_GetKeyInfoEx;
  command.args.getkeyinfoex.key = k;
  if (!(result = nobis__transact
        (nb, file, line, 0, &command, &reply))) {
    if (type)
      *type = reply.reply.getkeyinfoex.type;
    if (nbits)
      *nbits = reply.reply.getkeyinfoex.length;
    if (hash)
      memcpy(&hash->bytes, reply.reply.getkeyinfoex.hash.bytes,
             sizeof(hash->bytes));
    nobis_freereply(nb, &reply);
  }
  return result;
}

M_Status
nobis__getappdata(struct nobis *nb, const char *file, unsigned line,
                 M_KeyID k, M_AppData* appdata)
{
  M_Status result = Status_OK;
  M_Command command;
  M_Reply reply;
  memset(&command, 0, sizeof(command));
  command.cmd = Cmd_GetAppData;
  command.args.getappdata.key = k;
  if (!(result = nobis__transact(nb, file, line, nobis_tx_signers,
      &command, &reply))) {
    if (appdata)
      *appdata = reply.reply.getappdata.appdata;
    nobis_freereply(nb, &reply);
  }

  return result;
}

int
nobis_is_public(struct nobis *nb, M_KeyType kt, M_KeyID k)
{
  if (k) {
    M_Command command;
    M_Reply reply;
    memset(&command, 0, sizeof(command));
    command.cmd = Cmd_GetKeyInfo;
    command.args.getkeyinfo.key = k;
    if (!nobis_transact(nb, 0, &command, &reply)) {
      kt = reply.reply.getkeyinfo.type;
      nobis_freereply(nb, &reply);
    }
  }
  return ((kt == KeyType_RSAPublic) ||
          (kt == KeyType_DSAPublic) ||
          (kt == KeyType_DHPublic) ||
          (kt == KeyType_KCDSAPublic) ||
          (kt == KeyType_ECPublic) ||
          (kt == KeyType_ECDSAPublic) ||
          (kt == KeyType_ECDHPublic) ||
          (kt == KeyType_ECDHLaxPublic));
}

int
nobis_is_asymm(struct nobis *nb, M_KeyType kt, M_KeyID k)
{
  if (k) {
    M_Command command;
    M_Reply reply;
    memset(&command, 0, sizeof(command));
    command.cmd = Cmd_GetKeyInfo;
    command.args.getkeyinfo.key = k;
    if (!nobis_transact(nb, 0, &command, &reply)) {
      kt = reply.reply.getkeyinfo.type;
      nobis_freereply(nb, &reply);
    }
  }
  return (nobis_is_public(nb, kt, 0) ||
          (kt == KeyType_RSAPrivate) ||
          (kt == KeyType_DSAPrivate) ||
          (kt == KeyType_DHPrivate) ||
          (kt == KeyType_KCDSAPrivate) ||
          (kt == KeyType_ECPrivate) ||
          (kt == KeyType_ECDSAPrivate) ||
          (kt == KeyType_ECDHPrivate) ||
          (kt == KeyType_ECDHLaxPrivate));
}

M_Status
nobis__destroy(struct nobis *nb, const char *file, unsigned line,
               M_KeyID k)
{
  M_Status result = Status_OK;
#ifndef NF_CROSSCC
  /* Do not delete existing objects */
  if (NFKM_isexistingobject(nb->world, k, NULL))
    return Status_OK;
#endif
  if (k) {
    M_Command command;
    M_Reply reply;
    memset(&command, 0, sizeof(command));
    command.cmd = Cmd_Destroy;
    command.args.destroy.key = k;
    if (!(result = nobis__transact
          (nb, file, line, 0, &command, &reply)))
      nobis_freereply(nb, &reply);
  }
  return result;
}

M_Status
nobis_random(struct nobis *nb, M_ByteBlock *bb)
{
  M_Status result = Status_OK;
  static const unsigned chunk = 7168;
  unsigned written = 0;
  while (!result && (written < bb->len)) {
    M_Command command;
    M_Reply reply;
    memset(&command, 0, sizeof(command));
    command.cmd = Cmd_GenerateRandom;
    command.args.generaterandom.lenbytes =
      (chunk + written > bb->len) ? (bb->len - written) : chunk;
    if (!(result = nobis_transact(nb, 0, &command, &reply))) {
      M_ByteBlock take = reply.reply.generaterandom.data;
      if (take.len + written > bb->len)
        take.len = bb->len - written;
      memcpy(bb->ptr + written, take.ptr, take.len);
      written += take.len;
      nobis_freereply(nb, &reply);
    }
  }
  return result;
}

M_Status
nobis_randint(struct nobis *nb, unsigned limit, unsigned *value)
{
  unsigned base;
  M_ByteBlock bb = { sizeof(base), (unsigned char*)&base };
  M_Status result = nobis_random(nb, &bb);
  if (!result && value)
    *value = (unsigned)((double)limit * base / (UINT_MAX + 1.0));
  return result;
}

NF_Free_Context *
nobis_setctx_free(struct nobis *nb, NF_Free_Context *fctx)
{
#ifndef NF_CROSSCC
  fctx->u = nb->udata;
#else
  fctx->u = NULL;
#endif
  return fctx;
}

NF_Marshal_Context *
nobis_setctx_marsh(struct nobis *nb, unsigned char *ptr, unsigned len,
                   NF_Marshal_Context *mctx)
{
#ifndef NF_CROSSCC
  mctx->u = nb->udata;
#else
  mctx->u = NULL;
#endif
  mctx->op     = ptr;
  mctx->remain = len;
  return mctx;
}

NF_Unmarshal_Context *
nobis_setctx_unmar(struct nobis *nb, const unsigned char *ptr,
                   unsigned len, NF_Unmarshal_Context *uctx)
{
#ifndef NF_CROSSCC
  uctx->u = nb->udata;
#else
  uctx->u = NULL;
#endif
  uctx->ip     = ptr;
  uctx->remain = len;
  return uctx;
}

NF_Marshal_Context *
nobis_setctx_marbb(struct nobis *nb, M_ByteBlock *bb,
                   NF_Marshal_Context *mctx)
{
  return nobis_setctx_marsh(nb, bb->ptr, bb->len, mctx);
}

NF_Unmarshal_Context *
nobis_setctx_unmarbb(struct nobis *nb, const M_ByteBlock *bb,
                     NF_Unmarshal_Context *uctx)
{
  return nobis_setctx_unmar(nb, bb->ptr, bb->len, uctx);
}

NF_Print_Context *
nobis_setctx_print(struct nobis *nb, FILE *f, NF_Print_Context *pctx)
{
#ifndef NF_CROSSCC
  pctx->u = nb->udata;
#else
  pctx->u = NULL;
#endif
  pctx->file = f;
  return pctx;
}

char *
nobis_hexify(struct nobis *nb, unsigned flags, unsigned limit,
             const M_ByteBlock *src)
{
  char *result = NULL;
  unsigned ii, current = 0;
  unsigned size = (limit > 0 && src && limit < src->len) ?
    limit : src->len;
  unsigned allocation = 1 + size * 2  + size / 4 + size / 16;

  if (size > 0)
    result = nobis_malloc(nb, allocation);
  if (result) {
    for (ii = 0; ii < size; ii++) {
      if (!(flags & nobis_hex_lean) && ii && !(ii % 4)) {
        const char *sep = ((ii % 32) || !(flags & nobis_hex_lines)) ?
          ":" : "\n  ";
        snprintf(result + current, allocation - current, "%s", sep);
        current += (unsigned)strlen(sep);
      }
      snprintf(result + current, allocation - current,
               "%x%x", 0xF & (src->ptr[ii] >> 4), 0xF & src->ptr[ii]);
      current += 2;
    }
  }
  return result;
}

M_Status
nobis_unhexify(struct nobis *nb, unsigned flags, unsigned char *dest,
               unsigned limit, unsigned *filled, const char *src)
{
  unsigned didx = 0;
  char c;
  for (c = tolower((int)*src); *src; c = tolower((int)*++src)) {
    int hex = -1;
    if ((c == ':') || (isspace(c)))
      continue;

    if ((c >= '0') && (c <= '9'))
      hex = c - '0';
    else if ((c >= 'a') && (c <= 'f'))
      hex = c - 'a' + 10;

    if (hex >= 0) {
      if (dest && didx / 2 < limit)
        dest[didx/2] = (didx % 2) ?
          (dest[didx/2] | (hex & 0xF)) : (hex << 4);
      else if (dest)
        return (flags & nobis_uhex_quiet) ? Status_BufferFull :
          nobis_fail_msg(nb, __func__, Status_BufferFull,
                         "buffer is full");
      didx++;
    } else if (flags & nobis_uhex_ignore)
      continue;
    else return (flags & nobis_uhex_quiet) ? Status_Malformed :
           nobis_fail_msg(nb, __func__, Status_Malformed,
                          "unrecognized character `%c'", *src);
  }
  if (filled)
    *filled = (didx + 1)/2;
  return Status_OK;
}

M_Status
nobis_unhexbb(struct nobis *nb, unsigned flags,
              M_ByteBlock *bb, const char *hexvalue)
{
  M_Status result = Status_OK;
  if (hexvalue &&
      !(result = nobis_unhexify
        (nb, flags, NULL, (unsigned)strlen(hexvalue), &bb->len, hexvalue))) {
    if ((bb->ptr = nobis_malloc(nb, bb->len))) {
      if ((result = nobis_unhexify
           (nb, flags, bb->ptr, bb->len, NULL, hexvalue))) {
        nobis_free(nb, bb->ptr);
        bb->ptr = NULL;
      }
    } else result = NOBIS_STATUS_NOMEM;
  }
  return result;
}

M_Status
nobis_unhexbignum(struct nobis *nb, unsigned flags,
                  M_Bignum *bn, const char *hexvalue)
{
  M_Status result = Status_OK;
  M_ByteBlock bb = {0};
  if (!(result = nobis_unhexbb(nb, flags, &bb, hexvalue)))
    result = nobis_bignum_create(nb, bn, &bb);
  nobis_freebb(nb, &bb);
  return result;
}

M_Status
nobis_fail_vmsg(struct nobis *nb, const char *context, M_Status status,
                const char *message, va_list args)
{
  if (nb)
    fprintf(NOBIS_TRACE_FILE, "[%s] ", nb->progname);
  if (context)
    fprintf(NOBIS_TRACE_FILE, "%s failed with %s(%d)%s%s: ",
            context, NF_Lookup(status, NF_Status_enumtable),
            status, (status == Status_OSErrorErrno) ? ": " : "",
            (status == Status_OSErrorErrno) ?
            strerror(errno) : "");
  vfprintf(NOBIS_TRACE_FILE, message, args);
  fprintf(NOBIS_TRACE_FILE, "\n");
  return status;
}

M_Status
nobis_fail_msg(struct nobis *nb, const char *context, M_Status status,
               const char *message, ...)
{
  va_list args;
  va_start(args, message);
  nobis_fail_vmsg(nb, context, status, message, args);
  va_end(args);
  return status;
}

M_Status
nobis_check_status(struct nobis *nb, const char *file, unsigned line,
                   M_Status status, const char *fn)
{
  if (status != Status_OK)
    nobis_fail_msg(nb, fn, status, " at %s:%u", file, line);
  return status;
}

int
nobis_check_errno(struct nobis *nb, const char *context,
                  int result, const char *fn)
{
  if (result < 0)
    nobis_fail_msg(nb, context, result, "%s: %s", fn, strerror(errno));
  return result;
}

void *
nobis_check_null(struct nobis *nb, void *ptr, const char *fn,
                 const char *message, ...)
{
  if (ptr == NULL) {
    va_list args;
    va_start(args, message);
    nobis_fail_vmsg(nb, fn, 0, message, args);
    va_end(args);
  }
  return ptr;
}

M_ByteBlock *
nobis_reversebb(struct nobis *nb, unsigned span, M_ByteBlock *bb)
{
  if (span) {
    unsigned i, chunks = bb->len / span;
    for (i = 0; i < chunks / 2; ++i) {
      unsigned j;
      for (j = 0; j < span; ++j) {
        char swap = bb->ptr[(i * span) + j];
        bb->ptr[(i * span) + j] =
          bb->ptr[((chunks - i - 1) * span) + j];
        bb->ptr[((chunks - i - 1) * span) + j] = swap;
      }
    }
  }
  return bb;
}

static const unsigned char *
nobis__vprint_bytes(struct nobis *nb, const unsigned char *bytes,
                    unsigned length, const char *message, va_list args)
{
  unsigned i;
  if (message) {
    vfprintf(stdout, message, args);
    fprintf(stdout, " (%u bytes):\n", length);
  }
  if (bytes) {
    fprintf(stdout, "  ");
    for (i = 0; i < length; ++i) {
      if (i && !(i % 4))
        fprintf(stdout, (i % 32) ? ":" : "\n  ");
      fprintf(stdout, "%x%x", 0xF & (bytes[i] >> 4), 0xF & bytes[i]);
    }
    fprintf(stdout, "\n");
  } else fprintf(stdout, "  <null>\n");
  return bytes;
}

const unsigned char *
nobis_print_bytes(struct nobis *nb, const unsigned char *bytes,
                  unsigned length, const char *message, ...)
{
  va_list args;
  va_start(args, message);
  nobis__vprint_bytes(nb, bytes, length, message, args);
  va_end(args);
  return bytes;
}

const M_Bignum *
nobis_printbn(struct nobis *nb, const M_Bignum *bn, unsigned limit,
              const char *message, ...)
{
  M_ByteBlock bb;
  if (!(nobis_bignum_bytes(nb, bn, &bb))) {
    va_list args;
    va_start(args, message);
    nobis__vprint_bytes(nb, bb.ptr, (limit && limit < bb.len) ?
                        limit : bb.len, message, args);
    va_end(args);
    nobis_freebb(nb, &bb);
  }
  return bn;
}

const M_ByteBlock *
nobis_printbb(struct nobis *nb, const M_ByteBlock *bb, unsigned limit,
              const char *message, ...)
{
  va_list args;
  va_start(args, message);
  nobis__vprint_bytes(nb, bb->ptr, (limit && limit < bb->len) ?
                      limit : bb->len, message, args);
  va_end(args);
  return bb;
}

const M_KeyHash *
nobis_printkh(struct nobis *nb, const M_KeyHash *kh,
              const char *message, ...)
{
  va_list args;
  va_start(args, message);
  nobis__vprint_bytes(nb, kh->bytes, sizeof(kh->bytes), message, args);
  va_end(args);
  return kh;
}

M_Status
nobis_print_keyinfo(struct nobis *nb, M_KeyID k, unsigned flags,
                    const char *message, ...)
{
  M_Status result;
  M_KeyType ktype;
  M_Word ksize;
  M_KeyHash khash;
  M_ByteBlock khbb = { sizeof(khash.bytes), khash.bytes };

  if (message) {
    va_list args;
    va_start(args, message);
    vfprintf(stdout, message, args);
    va_end(args);
    fprintf(stdout, ":\n");
  }
  if (!(result = nobis_keyinfo(nb, k, &ktype, &ksize, &khash))) {
    nobis_printbb(nb, &khbb, 0, "  KeyID 0x%x %s-%u", (unsigned)k,
                  NF_Lookup(ktype, NF_KeyType_enumtable),
                  (unsigned)ksize);
    if (flags & nobis_printki_acl) {
      M_Command command = {0};
      M_Reply reply;
      command.cmd = Cmd_GetACL;
      command.args.getacl.key = k;
      if (!(result = nobis_transact(nb, 0, &command, &reply))) {
        NF_Print_Context pctx;
        nobis_setctx_print(nb, stdout, &pctx);
        fprintf(stdout, "  ");
        NF_Print_ACL(&pctx, 2, &reply.reply.getacl.acl);
        nobis_freereply(nb, &reply);
      }
    }
  }
  return result;
}

int
nobis_parity_isodd(unsigned char b)
{
  int p = 0;
  while (b) {
    p = ~p;
    b &= b - 1;
  }
  return p;
}

void
nobis_parity_setodd(M_ByteBlock* bb)
{
  M_Word i;
  for (i = 0; i < bb->len; ++i)
    if (!nobis_parity_isodd(bb->ptr[i]))
      bb->ptr[i] ^= 1;
}

M_Mech nobis_getHashMech( M_Mech mech)
{
  switch(mech)
  {
    case Mech_RSAhSHA1pPKCS1:
    case Mech_DES3wSHA1:
    case Mech_SHA1Hash:
    case Mech_HMACSHA1:
    case Mech_KCDSASHA1:
    case Mech_DLIESe3DEShSHA1:
    case Mech_DLIESeAEShSHA1:
    case Mech_BlobCryptv2kHasheRijndaelCBC0hSHA1mSHA1HMAC:
    case Mech_BlobCryptv2kRSAeRijndaelCBC0hSHA1mSHA1HMAC:
    case Mech_BlobCryptv2kDHeRijndaelCBC0hSHA1mSHA1HMAC:
    case Mech_BlobCryptv2kHasheDES3CBC0hSHA1mSHA1HMAC:
    case Mech_BlobCryptv2kRSAeDES3CBC0hSHA1mSHA1HMAC:
    case Mech_BlobCryptv2kDHeDES3CBC0hSHA1mSHA1HMAC:
    case Mech_RSAhSHA1pPSS:
      return Mech_SHA1Hash;
    case Mech_SHA256Hash:
    case Mech_HMACSHA256:
    case Mech_BlobCryptv2kHasheRijndaelCBC0hSHA256mSHA256HMAC:
    case Mech_BlobCryptv2kRSAeRijndaelCBC0hSHA256mSHA256HMAC:
    case Mech_BlobCryptv2kDHeRijndaelCBC0hSHA256mSHA256HMAC:
    case Mech_RSAhSHA256pPSS:
    case Mech_RSAhSHA256pPKCS1:
    case Mech_DSAhSHA256:
    case Mech_KCDSASHA256:
    case Mech_ECDSAhSHA256:
    case Mech_RSApPKCS1OAEPhSHA256:
      return Mech_SHA256Hash;
    case Mech_SHA224Hash:
    case Mech_HMACSHA224:
    case Mech_DSAhSHA224:
    case Mech_KCDSASHA224:
    case Mech_ECDSAhSHA224:
    case Mech_RSApPKCS1OAEPhSHA224:
      return  Mech_SHA224Hash;
    case Mech_SHA384Hash:
    case Mech_HMACSHA384:
    case Mech_RSAhSHA384pPSS:
    case Mech_RSAhSHA384pPKCS1:
    case Mech_DSAhSHA384:
    case Mech_ECDSAhSHA384:
    case Mech_RSApPKCS1OAEPhSHA384:
      return Mech_SHA384Hash;
    case Mech_SHA512Hash:
    case Mech_HMACSHA512:
    case Mech_BlobCryptv2kHasheRijndaelCBC0hSHA512mSHA512HMAC:
    case Mech_BlobCryptv2kRSAeRijndaelCBC0hSHA512mSHA512HMAC:
    case Mech_BlobCryptv2kDHeRijndaelCBC0hSHA512mSHA512HMAC:
    case Mech_RSAhSHA512pPSS:
    case Mech_RSAhSHA512pPKCS1:
    case Mech_DSAhSHA512:
    case Mech_ECDSAhSHA512:
    case Mech_RSApPKCS1OAEPhSHA512:
      return Mech_SHA512Hash;
    default:
      return 0;
  }
}
