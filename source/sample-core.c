/* sample-core.c
 * Copyright (C) 2015-2018 nCipher Security, LLC.  All rights reserved.
 * ---------------------------------------------------------------------
 * This module is designed to run inside an SEE appliation.  This
 * implements the core features of this project for Bitcoin Hierarchical
 * Deterministic Samples.  For testing only, this code can be compiled
 * into a host application and run in a host process.  HOWEVER, this
 * exposes critical secrets in memory and must only be used for test
 * keys that are not HSM secured.
 * ------------------------------------------------------------------ */

#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <ctype.h>
#include "nobis.h"
#include "sample-marshal.h"
#include "sample-core.h"

#define REPMSG(nb, rep, msg)                                      \
    nobis_fail_msg((nb), __func__, (rep), "%s: %s (%d) at %s %u", \
    (msg), SampleStr_Rep((rep)), (rep), __FILE__, __LINE__)

/******
 * Globals setup during sample_core_init()
 ******/
static M_ModuleID s_mod;
static M_ByteBlock s_fixedrandom;

/******
 * Globals setup during sample_core_runtimeinit()
 ******/
static M_Status s_rtinitstat = -1;

/******
 * Globals
 ******/
static const M_Hash ZEROHASH;

#ifdef MACHINE_DEBUG
#ifndef NF_CROSSCC

#endif
#endif

/* see header file documentation */
M_Status sample_core_init(struct nobis *nb, M_ModuleID module)
{
  int rc;

  /* for the cross compile on the host for debugging core
     inside SEElib, this should always be zero */
  s_mod = module;

  /* do any more init that is required */
  s_fixedrandom.len = 7*1024;
  s_fixedrandom.ptr = nobis_malloc(nb, s_fixedrandom.len);
  nobis_random(nb, &s_fixedrandom);
  rc = Status_OK;

  return rc;
}

/* see header file documentation */
void sample_core_cleanup(struct nobis *nb)
{
  NF_Free_Context fctx;
  nobis_setctx_free(nb, &fctx);

  nobis_free(nb, s_fixedrandom.ptr);

  s_rtinitstat = -1;
}

/* see header file documentation */
M_Status
sample_core_runtimeinit(struct nobis *nb)
{
  int rc;

  if (s_rtinitstat != (M_Status)-1)
    return s_rtinitstat;

  rc = Status_OK;

  return s_rtinitstat = rc;
}

/* @brief Fill a buffer with fixed random data
 *
 * @param nb nCore abstraction.
 * @param ptr buffer to fill with data
 * @param size number of bytes to write into the buffer
 * @return number of bytes written to the buffer */
static M_Word
sample_read_fixed_random(struct nobis *nb, unsigned char *ptr, M_Word size)
{
  M_Word written = 0;
  memset(ptr, 0, size);
  while (size - written > s_fixedrandom.len) {
    memcpy(ptr + written, s_fixedrandom.ptr, s_fixedrandom.len);
    written += s_fixedrandom.len;
  }
  if (written < size) {
    memcpy(ptr + written, s_fixedrandom.ptr, size - written);
    written += size - written;
  }

  return written;
}

/* see header file documentation */
M_Status
SampleEchoBlock(struct nobis *nb,
                     const M_SampleOp_EchoBlock_Args *args,
                     M_SampleOp_EchoBlock_Reply *reply,
                     M_SampleRep *repcode)
{
  int stat = Status_OK;

  reply->block.ptr = nobis_malloc(nb, args->block.len);
  if (reply->block.ptr) {
    reply->block.len = args->block.len;
    memcpy(reply->block.ptr, args->block.ptr, reply->block.len);
  } else {
    *repcode = SampleRep_nCoreFailed;
    stat = NOBIS_STATUS_NOMEM;
  }

  return stat;
}

/* see header file documentation */
M_Status
SamplePushBlock(struct nobis *nb,
                     const M_SampleOp_PushBlock_Args *args,
                     M_SampleRep *repcode)
{
  int stat = Status_OK;

  return stat;
}

/* see header file documentation */
M_Status
SamplePullBlock(struct nobis *nb,
                     const M_SampleOp_PullBlock_Args *args,
                     M_SampleOp_PullBlock_Reply *reply,
                     M_SampleRep *repcode)
{
  int stat = Status_OK;

  reply->block.ptr = nobis_malloc(nb, args->size);
  if (reply->block.ptr) {
    reply->block.len = sample_read_fixed_random(nb, reply->block.ptr, args->size);
  } else {
    *repcode = SampleRep_nCoreFailed;
    stat = NOBIS_STATUS_NOMEM;
  }

  return stat;
}

/* see header file documentation */
M_Status
SamplePushPullBlock(struct nobis *nb,
                     const M_SampleOp_PushPullBlock_Args *args,
                     M_SampleOp_PushPullBlock_Reply *reply,
                     M_SampleRep *repcode)
{
  int stat = Status_OK;

  reply->block.ptr = nobis_malloc(nb, args->size);
  if (reply->block.ptr) {
    reply->block.len = sample_read_fixed_random(nb, reply->block.ptr, args->size);
  } else {
    *repcode = SampleRep_nCoreFailed;
    stat = NOBIS_STATUS_NOMEM;
  }

  return stat;
}

/* see header file documentation */
M_Status
SampleNoOpTransact(struct nobis *nb,
                     const M_SampleOp_NoOpTransact_Args *args,
                     M_SampleRep *repcode)
{
  int stat = Status_OK;
  M_Word count = args->count;
  M_Reply r = {0};
  M_Command c = {0, Cmd_NoOp};
  c.args.noop.module = s_mod;

  for (; !stat && count; --count) {
    stat = nobis_transact(nb, 0, &c, &r);
  }

  if (stat)
    *repcode = SampleRep_nCoreFailed;

  return stat;
}
