import nfpython as nf
import optparse
import time

op = optparse.OptionParser(version="%prog 1.0")
op.add_option('-m','--module', dest='module', type='int', default=1,
    metavar='MODULE', help='Select module to use. [default: %default]')
op.add_option('-p','--published-obj', dest='pubobj', type='string',
    metavar='PUBOBJ', help='Published Object name of SEE application')
opts, args=op.parse_args()

if opts.pubobj is None:
  op.error("PUBOBJ must be specified")

conn=nf.connection()
w=conn.transact(nf.Command(
    ['GetPublishedObject',0,0,opts.pubobj,opts.module])).reply.object

print "Monitoring published object '%s' on module %d..." % (opts.pubobj, opts.module)
try:
  while True:
    s=conn.transact(nf.Command(['TraceSEEWorld',0,w])).reply.data.tostring()
    if len(s) > 0:
      print s
    time.sleep(0.010)
finally:
  conn.transact(nf.Command(['Destroy', 0, w]))  
  print "Shutdown"
