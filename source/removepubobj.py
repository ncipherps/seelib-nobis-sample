# Copyright (C) 2014 nCipher Security, LLC.  All rights reserved.
# This example source code is provided for your information and assistance.
# Note that there is NO WARRANTY.

import nfpython as nf
import optparse

op = optparse.OptionParser(version="%prog 1.0")
op.add_option('-m','--module', dest='module', type=int, default=1,
              metavar='MODULE', help='Select module to use. [default: %default]')
op.add_option('-p','--pubobj', dest='pubname',
              metavar='PUBOJB', help='Published object for SEE application.')
opts, args=op.parse_args()

if opts.pubname is None:
  op.error('Publisehd object name must be specified.')

moduleid=opts.module
pubname=opts.pubname

conn=nf.connection(priv=True, needworldinfo=False)
conn.transact(nf.Command(['RemovePublishedObject',0,pubname,moduleid,0]))
