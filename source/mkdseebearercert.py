import nfpython as nf
import nfkm
import nfkmutils
import rqcard

conn = nf.connection()
winfo = nfkm.getinfo(conn)

certknsodsee = nfkm.loaddelegcert(conn, 'DSEE')
mech = certknsodsee.body.signature.mech
print mech

acl = nf.ACL([[[]]])
acl.groups[0].actions.append(nf.Action(['NSOPermissions', ['DebugSEEWorld']]))
certmsg = nf.CertSignMessage(['CertMsgHeader', ['acl_present'], [], acl, 'CertMsgFooter'])
certmsg = nf.marshal(certmsg)
print certmsg

args = rqcard.RQArgs([0, 'Scroll', 0, 'AnyOne', 0, 'LoadAdmin', 0,
    ['dsee'],[], 'Loading DSEE'])
res = rqcard.doload(conn, args)
module = res.pmr.module
dsee = res.pmr.result.keys[0]

cmd = nf.Command(['Sign', 0, 0, dsee, mech, 'Bytes', certmsg])
sig = conn.transact(cmd).reply.sig

pub = nfkmutils.loadblob(conn, winfo.blobpubkdsee, module)
cmd = nf.Command(['Export', 0, pub])
pubkeydata = conn.transact(cmd).reply.data

cl = nf.CertificateList()
cl.certs.append(nf.Certificate())
cl.certs.append(certknsodsee)

cl.certs[0].keyhash = winfo.hkdsee
cl.certs[0].type = 'SingleCert'
cl.certs[0].body.certsignmsg = certmsg
cl.certs[0].body.signature = sig
cl.certs[0].body.pubkeydata = pubkeydata

nf.marshal(cl).write(open('dseebearercert.bin', 'wb'))
