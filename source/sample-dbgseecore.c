/* sample-dbgseecore.c
 * Copyright (C) 2015-2018 nCipher Security, LLC.  All rights reserved.
 * ---------------------------------------------------------------------
 * Main module for building the core of the SEE machine as host code
 * for debug testing in a host environment
 * ------------------------------------------------------------------ */

#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <errno.h>
#include <time.h>

#include <nfopt.h>
#include "nobis.h"
#include "sample-marshal.h"
#include "sample-core.h"

/* Command line parser options */
static const char *progname = NULL;
static int o_module = 1, o_slot = 0;

static const nfopt options[] = {
  NFOPT_HELP
  NFOPT_HEADING("Generic Options")
  NFOPT_INT('m', "module", "MODULE", &o_module, &ir_pos,
            "Use module MODULE (default 1).")
  NFOPT_INT('s', "slot", "SLOT", &o_slot, &ir_nonneg,
            "Use slot SLOT for smart cards (default 0).")
  NFOPT_END
};

static const nfopt_proginfo proginfo = {
  "$p [options]",
  "Sample Sample SEE - Host build for debugging of SEE Core code\n"
  "Copyright (C) 2015 nCipher Security, LLC.  All rights reserved.",
  "This program is for debugging only.  The SEE core is complie into\n"
  "a host application.\n",
  options
};

static int readuserdata(struct nobis *nb, M_SampleUserData *ud)
{
  static const char *UDFILE = "sample-seelib-userdata.in";
  M_ByteBlock bb;
  int rc;

  if (!(rc = nobis_file2bb(nb, &bb, UDFILE))) {
    rc = NOBIS_CHK_STATUS(nb, SampleUserData_Unmarshal,
                          (nb, bb.ptr, bb.len, ud));
    nobis_free(nb, bb.ptr);
  }

  return rc;
}

static M_SampleRep
sample_check_rep(struct nobis *nb, const char *file, unsigned line,
                 M_SampleRep rep, M_Status stat, const char *fn)
{
  if (rep != SampleRep_Success) {
    if (nb)
      fprintf(NOBIS_TRACE_FILE, "[%s] ", nb->progname);
    if (stat != Status_OK)
      fprintf(NOBIS_TRACE_FILE, "%s failed with %s(%d): nCore %s(%d) at %s:%u",
              fn, SampleStr_Rep(rep), rep, NF_Lookup(stat, NF_Status_enumtable),
              stat, file, line);
    else
      fprintf(NOBIS_TRACE_FILE, "%s failed with %s(%d) at %s:%u",
              fn, SampleStr_Rep(rep), rep, file, line);
    fprintf(NOBIS_TRACE_FILE, "\n");
  }

  return rep;

}

#define SAMPLE_CHK_REP(nb, rep, stat, op) \
  sample_check_rep(nb, __FILE__, __LINE__, rep, stat, SampleStr_Op(op))

static int test(struct nobis *nb)
{
  return 0;
}

int main(int argc, char *argv[])
{
  struct nobis nb;
  M_SampleUserData ud;
  int fail = 0;

  if (nfopt_parse(&argc, &argv, &progname, &proginfo))
    return EXIT_FAILURE;

  /* supress compiler warning about unused static function */
  (void)sample_check_rep;

  if (!nobis_setup(&nb, 0, progname)) {
    fprintf(NOBIS_TRACE_FILE, "nobis_setup complete\n");
    if (!nobis_fips_auth(&nb, o_module, o_slot)) {
      fprintf(NOBIS_TRACE_FILE, "nobis_fips_auth complete\n");
      if (!sample_core_init(&nb, o_module)) {
        fprintf(NOBIS_TRACE_FILE, "sample_core_init complete\n");
        if (!readuserdata(&nb, &ud)) {
          if (!sample_core_runtimeinit(&nb)) {
            fprintf(NOBIS_TRACE_FILE, "sample_core_runtimeinit complete\n");

            test(&nb);
          } else ++fail;

          SampleUserData_Free(&nb, &ud);
        } else ++fail;

        sample_core_cleanup(&nb);
      } else ++ fail;
    } else ++ fail;

    nobis_cleanup(&nb);
  } else ++ fail;

  return fail ? EXIT_FAILURE : EXIT_SUCCESS;
}
