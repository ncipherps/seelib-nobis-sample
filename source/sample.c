/* sample.c
 * Copyright (C) 2015-2018 nCipher Security, LLC.  All rights reserved.
 * ---------------------------------------------------------------------
 * Main module for sample sample SEE machine hostside.
 * ------------------------------------------------------------------ */

#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <errno.h>
#include <time.h>

#include <nfopt.h>
#include "nobis.h"
#include "sample-marshal.h"

#define PROJECT_CONTROL "userdata-signer"

static M_Status seetransact(struct nobis *nb, M_KeyID w, M_SampleJobCmd* csee, M_SampleJobReply* rsee, int debug)
{
  M_Status stat;
  M_Command c = {0};
  M_Reply r = {0};
  int len = JOB_LIMIT;
  unsigned char buf[JOB_LIMIT];

  stat = (M_Status)SampleJob_MarshalCmd(nb, csee, buf, &len);
  if (!stat) {
    c.cmd = Cmd_SEEJob;
    c.args.seejob.worldid = w;
    c.args.seejob.seeargs.len = len;
    c.args.seejob.seeargs.ptr = buf;

    stat = nobis_transact(nb, 0, &c, &r);
    if (!stat)
      stat = (M_Status)SampleJob_UnmarshalReply(nb, r.reply.seejob.seereply.ptr,
                                              r.reply.seejob.seereply.len, rsee);
    else
      printf("In seetransact nobis_transact failed (%d)\n", stat);
  }else{
    printf("In seetransact SampleJob_MarshalCmd failed\n");
  }

  if (debug)
    nobis_seew_trace(nb, w, stderr);

  return stat;
}

static M_SampleRep
sample_check_rep(struct nobis *nb, const char *file, unsigned line,
                 M_SampleRep rep, M_Status stat, const char *fn)
{
  if (rep != SampleRep_Success) {
    if (nb)
      fprintf(NOBIS_TRACE_FILE, "[%s] ", nb->progname);
    if (stat != Status_OK)
      fprintf(NOBIS_TRACE_FILE, "%s failed with %s(%d): nCore %s(%d) at %s:%u",
              fn, SampleStr_Rep(rep), rep, NF_Lookup(stat, NF_Status_enumtable),
              stat, file, line);
    else
      fprintf(NOBIS_TRACE_FILE, "%s failed with %s(%d) at %s:%u",
              fn, SampleStr_Rep(rep), rep, file, line);
    fprintf(NOBIS_TRACE_FILE, "\n");
  }

  return rep;

}

#define SAMPLE_CHK_REP(nb, rep, stat, op) \
  sample_check_rep(nb, __FILE__, __LINE__, rep, stat, SampleStr_Op(op))


static int print_userdata(struct nobis *nb, const char *file)
{
  int rc = EXIT_FAILURE;
  M_ByteBlock data = {0, NULL};
  M_SampleUserData ud = {0};

  if (!nobis_file2bb(nb, &data, file)
      && !NOBIS_CHK_STATUS(nb, SampleUserData_Unmarshal,
                          (nb, data.ptr, data.len, &ud))) {
    SampleUserData_Print(nb, stdout, 0, &ud);
    rc = EXIT_SUCCESS;
  }

  SampleUserData_Free(nb, &ud);
  nobis_free(nb, data.ptr);

  return rc;
}

static const char *o_control = PROJECT_CONTROL;

static int create_userdata(struct nobis *nb, const char *file)
{
  int rc = EXIT_FAILURE;
  NFKM_Key *nk_control = NULL;
  NFKM_Key *nk_machine = NULL;
  M_SampleUserData ud = {UD_MAGICHEADER, UD_CURRENTVER};
  M_ByteBlock data = {0, NULL};

  ud.hkm = nb->world->hkm;
  ud.hkmwk = nb->world->hkmwk;

  if ((!o_control
        || !nobis_sworld_keyinfo(nb, &nk_control, 0, "seeinteg", o_control))) {
    data.len = INT_MAX;
    if (!NOBIS_CHK_STATUS(nb, SampleUserData_Marshal, (nb, &ud, NULL, &data.len))
        && NULL != (data.ptr = nobis_malloc(nb, data.len))
        && !NOBIS_CHK_STATUS(nb, SampleUserData_Marshal, (nb, &ud, data.ptr, &data.len))
        && !nobis_bb2file(nb, &data, "wb", file)
		&& !nobis_sworld_keyinfo(nb, &nk_machine, 0, "seeinteg", "machine-signer")){
      fprintf(stdout, "%s:  succesfully created tbs userdata file '%s'.\n\n",
              nb->progname, file);
      if (nk_control)
        fprintf(stdout,
              "Use `tct2 --sign-and-pack ...` to sign and produce a 'sar' file.\n"
              "  * Must be signed with key 'seeinteg/%s'\n"
              "  * The machine must be signed by 'seeinteg/%s'\n"
              "For dev test:\n"
              "`tct2 --sign-and-pack -k %s --machine-key-ident=%s "
              "-o sample-seelib-userdata.sar %s`\n",
              nk_control->ident, nk_machine->ident, nk_control->ident, nk_machine->ident, file);
      else
        fprintf(stdout,
              "  * This is a totally unsecure configuration\n"
              "  * File does not need to be signed but must be packed\n\n"
              "For dev test only:\n"
              "Use `tct2 --pack -o sample-seelib-userdata.sar %s\n", file);


      rc = EXIT_SUCCESS;
    }
  }
  
  /* release memory */
  nobis_sworld_freekeyinfo(nb, nk_control);
  nobis_sworld_freekeyinfo(nb, nk_machine);
  nobis_freebb(nb, &data);

  return rc;
}

/* Command line parser options */
enum oflags {
  oflag_debug    = (1 << 0),
  oflag_unittest = (1 << 1),
  oflag_createud = (1 << 2),
  oflag_printud  = (1 << 3),
  oflag_reduced_security = (1 << 4),
};

static const char *progname = NULL;
static unsigned o_flags = oflag_unittest;
static int o_module = 1, o_slot = 0, o_adminslot = 0;
static char *o_userdata = NULL;
static char *o_pubobj = NULL;

static const nfopt options[] = {
  NFOPT_HELP
  NFOPT_HEADING("Generic Options")
  NFOPT_INT('m', "module", "MODULE", &o_module, &ir_pos,
            "Use module MODULE (default 1).")
  NFOPT_INT('s', "slot", "SLOT", &o_slot, &ir_nonneg,
            "Use slot SLOT for smart cards (default 0).")
  NFOPT_INT('\0', "adminslot", "SLOT", &o_adminslot, &ir_nonneg,
            "Use slot SLOT for ACS (default 0).")
  NFOPT_FLAG('d', "debug", &o_flags, oflag_debug,
             "Display debugging information on stderr.")
  NFOPT_STRING('U', "userdata", "FILENAME", &o_userdata,
               "Signed archive to use when creating SEE world. "
               "Or plaintext I/O file for create/print userdata.")
  NFOPT_STRING('p', "pubobj", "OBJECT", &o_pubobj,
               "Published object to use as SEE world. Note:  "
               "If userdata and pubobj options are both provided "
               "the userdata is used to attempt to start the SEE "
               "application (CreateSEEWorld). Upon success this "
               "will be publihsed (SetPublishedObject) with pubobj name.")
  NFOPT_HEADING("Actions")
  NFOPT_FLAG('T', "unittest", &o_flags, oflag_unittest,
             "DEFAULT; ignored if any other action is specified.")
  NFOPT_FLAG('C', "create-userdata", &o_flags, oflag_createud,
             "Create a to-be-signed userdata file; "
             "recommended name: sample-seelib-userdata.in")
  NFOPT_FLAG('P', "print-userdata", &o_flags, oflag_printud,
             "Print a to-be-signed userdata file")
  NFOPT_HEADING("To-Be-Signed Userdata File Options")
  NFOPT_FLAG('\0', "allow-reduced-security", &o_flags, oflag_reduced_security,
               "Allow one or zero TXSigners and/or Allow Module protected "
               "sample and/or Allow !SEEApp sample.")
  NFOPT_STRING('\0', "control", "IDENT", &o_control,
               "seeinteg key for SEEApp protection of the sample; "
               "Default: " PROJECT_CONTROL)
  NFOPT_END
};

static const nfopt_proginfo proginfo = {
  "$p [options]",
  "Sample Sample SEE - Host Executable\n"
  "Copyright (C) 2015 nCipher Security, LLC.  All rights reserved.",
  "This program is intended as an example of the host processing \n"
  "necessary to support the SEE machine.\n",
  options
};

struct nobis nb;
int fail = 0;
M_KeyID   seew = 0;

static int init_see_application(void)
{
  static int bSuccessfullyCompleted = 0;
  int fail = 0;
  M_SampleJobCmd csee = {0};
  M_SampleJobReply rsee = {0};
  unsigned char buffer[2048];

  if (bSuccessfullyCompleted)
    return 0;

  /* This command must be sent once to this SEE application after the SEE application
     is started.  This command loads fips if required and also servers as a
     initialize after startup command.  It is safe to call this mutlple time;
     subsequent calls are ignored.
  */
  csee.op = SampleOp_FIPSandInit;
  csee.args.fips.packed.ptr = buffer;
  csee.args.fips.packed.len = sizeof(buffer);
  if (!nobis_fips_pack(&nb, o_module, seew, &csee.args.fips.packed)
      && !seetransact(&nb, seew, &csee, &rsee, o_flags & oflag_debug)
      && !SAMPLE_CHK_REP(&nb, rsee.rep, rsee.intstat, csee.op)) {
    printf("%s successful\n", SampleStr_Op(rsee.op));
  } else  ++fail;
  SampleJob_FreeReply(&nb, &rsee);

  if (!fail)
    bSuccessfullyCompleted = 1;

  return fail;
}

static int run_tests(void)
{
  M_SampleJobCmd csee = {0};
  M_SampleJobReply rsee = {0};
  if (!fail) {
    csee.op = SampleOp_MachineVersion;
    if (!seetransact(&nb, seew, &csee, &rsee, o_flags & oflag_debug)
        && !SAMPLE_CHK_REP(&nb, rsee.rep, rsee.intstat, csee.op)) {
      printf("Machine version %d.%d.%d\n",
          rsee.reply.machineversion.major,
          rsee.reply.machineversion.minor,
          rsee.reply.machineversion.patch);
    } else ++fail;
    /* nothing allocated SampleJob_FreeCmd(&nb, &csee); */
    SampleJob_FreeReply(&nb, &rsee);
    memset(&csee, 0, sizeof(csee));
    memset(&rsee, 0, sizeof(rsee));
  }
  if (!fail) {
    csee.op = SampleOp_NoOp;
    if (!seetransact(&nb, seew, &csee, &rsee, o_flags & oflag_debug)
        && !SAMPLE_CHK_REP(&nb, rsee.rep, rsee.intstat, csee.op)) {
      printf("%s successful\n", SampleStr_Op(rsee.op));
    } else ++fail;
    SampleJob_FreeReply(&nb, &rsee);
    memset(&csee, 0, sizeof(csee));
    memset(&rsee, 0, sizeof(rsee));
  }
  if (!fail) {
    fail += init_see_application();
  }
  if (!fail) {
    csee.op = SampleOp_EchoBlock;
    csee.args.echoblock.block.len = 7*1024;
    csee.args.echoblock.block.ptr = nobis_malloc(&nb, csee.args.echoblock.block.len);
    nobis_random(&nb, &csee.args.echoblock.block);
    if (!seetransact(&nb, seew, &csee, &rsee, o_flags & oflag_debug)
        && !SAMPLE_CHK_REP(&nb, rsee.rep, rsee.intstat, csee.op)) {
      printf("%s successful (%d bytes)\n", SampleStr_Op(rsee.op),
          csee.args.echoblock.block.len);
    } else ++fail;
    SampleJob_FreeCmd(&nb, &csee);
    SampleJob_FreeReply(&nb, &rsee);
    memset(&csee, 0, sizeof(csee));
    memset(&rsee, 0, sizeof(rsee));
  }
  if (!fail) {
    csee.op = SampleOp_PushBlock;
    csee.args.pushblock.block.len = 7*1024;
    csee.args.pushblock.block.ptr = nobis_malloc(&nb, csee.args.pushblock.block.len);
    nobis_random(&nb, &csee.args.pushblock.block);
    if (!seetransact(&nb, seew, &csee, &rsee, o_flags & oflag_debug)
        && !SAMPLE_CHK_REP(&nb, rsee.rep, rsee.intstat, csee.op)) {
      printf("%s successful (%d bytes)\n", SampleStr_Op(rsee.op),
          csee.args.pushblock.block.len);
    } else ++fail;
    SampleJob_FreeCmd(&nb, &csee);
    SampleJob_FreeReply(&nb, &rsee);
    memset(&csee, 0, sizeof(csee));
    memset(&rsee, 0, sizeof(rsee));
  }
  if (!fail) {
    csee.op = SampleOp_PullBlock;
    csee.args.pullblock.size = 7*1024;
    if (!seetransact(&nb, seew, &csee, &rsee, o_flags & oflag_debug)
        && !SAMPLE_CHK_REP(&nb, rsee.rep, rsee.intstat, csee.op)) {
      printf("%s successful (%d bytes)\n", SampleStr_Op(rsee.op),
          csee.args.pullblock.size);
    } else ++fail;
    SampleJob_FreeCmd(&nb, &csee);
    SampleJob_FreeReply(&nb, &rsee);
    memset(&csee, 0, sizeof(csee));
    memset(&rsee, 0, sizeof(rsee));
  }
  if (!fail) {
    csee.op = SampleOp_PushPullBlock;
    csee.args.pushpullblock.block.len = 255*1024;
    csee.args.pushpullblock.block.ptr = nobis_malloc(&nb, csee.args.pushpullblock.block.len);
    csee.args.pushpullblock.size = 4*1024;
    nobis_random(&nb, &csee.args.pushpullblock.block);
    if (!seetransact(&nb, seew, &csee, &rsee, o_flags & oflag_debug)
        && !SAMPLE_CHK_REP(&nb, rsee.rep, rsee.intstat, csee.op)) {
      printf("%s successful (%d, %d)\n", SampleStr_Op(rsee.op),
          csee.args.pushpullblock.block.len,
          csee.args.pushpullblock.size);
    } else ++fail;
    SampleJob_FreeCmd(&nb, &csee);
    SampleJob_FreeReply(&nb, &rsee);
    memset(&csee, 0, sizeof(csee));
    memset(&rsee, 0, sizeof(rsee));
  }
  if (!fail) {
    csee.op = SampleOp_PushPullBlock;
    csee.args.pushpullblock.block.len = 4*1024;
    csee.args.pushpullblock.block.ptr = nobis_malloc(&nb, csee.args.pushpullblock.block.len);
    csee.args.pushpullblock.size = 255*1024;
    nobis_random(&nb, &csee.args.pushpullblock.block);
    if (!seetransact(&nb, seew, &csee, &rsee, o_flags & oflag_debug)
        && !SAMPLE_CHK_REP(&nb, rsee.rep, rsee.intstat, csee.op)) {
      printf("%s successful (%d, %d)\n", SampleStr_Op(rsee.op),
          csee.args.pushpullblock.block.len,
          csee.args.pushpullblock.size);
    } else ++fail;
    SampleJob_FreeCmd(&nb, &csee);
    SampleJob_FreeReply(&nb, &rsee);
    memset(&csee, 0, sizeof(csee));
    memset(&rsee, 0, sizeof(rsee));
  }
  if (!fail) {
    csee.op = SampleOp_NoOpTransact;
    csee.args.nooptransact.count = 4;
    if (!seetransact(&nb, seew, &csee, &rsee, o_flags & oflag_debug)
        && !SAMPLE_CHK_REP(&nb, rsee.rep, rsee.intstat, csee.op)) {
      printf("%s successful (%d transact(s))\n", SampleStr_Op(rsee.op),
          csee.args.nooptransact.count);
    } else ++fail;
    SampleJob_FreeCmd(&nb, &csee);
    SampleJob_FreeReply(&nb, &rsee);
    memset(&csee, 0, sizeof(csee));
    memset(&rsee, 0, sizeof(rsee));
  }

  return fail;
}

int main(int argc, char *argv[])
{
  if (nfopt_parse(&argc, &argv, &progname, &proginfo))
    return EXIT_FAILURE;
  if (argc) {
    fprintf(stderr, "%s: unrecognized argument `%s'\n  "
            "(run `%s --help' for more extensive help)\n",
            progname, argv[0], progname);
    return EXIT_FAILURE;
  }

  if (o_flags & (oflag_createud | oflag_printud))
    o_flags &= ~oflag_unittest;

  if (o_control[0] == '\0') o_control = NULL;

  /* confirm required command line paramaters depending on action */
  if ((o_flags & (oflag_unittest)) && !o_userdata && !o_pubobj) {
    fprintf(stderr, "%s: userdata or pubobj must be specified\n", progname);
    return EXIT_FAILURE;
  }
  if ((o_flags & oflag_createud) && !o_userdata) {
    fprintf(stderr, "%s: userdata file name required with --create-userdata\n",
            progname);
    return EXIT_FAILURE;
  }
  if ((o_flags & oflag_printud) && !o_userdata) {
    fprintf(stderr, "%s: userdata file name required with --print-userdata\n",
            progname);
    return EXIT_FAILURE;
  }
  if ((o_flags & oflag_createud) && !(o_flags & oflag_reduced_security)) {
    if (!o_control) {
      fprintf(stderr, "%s: '--allow-reduced-security' required to remove "
              "\n  sample SEEApp key\n",
              progname);
      return EXIT_FAILURE;
    }
  }

  /* setup nobis framework (access to hardserver and HSMs);
     setup fips if required; connect to SEE if required */
  if (!nobis_setup(&nb, o_userdata ?
                   nobis_setup_privconn : 0, progname)) {
    if (o_flags & (oflag_unittest)) {
      if (nobis_fips_auth(&nb, o_module, o_slot)) {
        ++fail;
      }
    }

    if (!fail && (o_flags & (oflag_unittest))) {
      /* start or connect to the SEE application */
      if (o_userdata) {
        M_Word    initstat = 0;
        M_ByteBlock seeud;
        if (!nobis_file2bb(&nb, &seeud, o_userdata)) {
          if (!nobis_seew_create
              (&nb, o_module, o_adminslot, (o_flags & oflag_debug),
               &seeud, &initstat, &seew)) {
            if (initstat) {
              fprintf(stderr, "%s: SEE init failed: %d\n",
                      progname, (unsigned)initstat);
              ++fail;
            }
          } else ++fail;
        } else ++fail;
      } else if (o_pubobj) {
        if (nobis_seew_getpublished(&nb, o_module, o_pubobj, &seew))
          ++fail;
      }

      if (!fail && o_userdata && o_pubobj) {
        M_Reply r = {0};
        M_Command c = {0, Cmd_SetPublishedObject};
        M_Cmd_SetPublishedObject_Args* args = &c.args.setpublishedobject;
        args->flags = Cmd_SetPublishedObject_Args_flags_object_present;
        args->name.ptr = o_pubobj;
        args->module = o_module;
        args->object = &seew;
        nobis_transact(&nb, 0, &c, &r);
      }
    }

    /* create the raw unsigned userdata file */
    (void)(!fail && (o_flags & oflag_createud) && create_userdata(&nb, o_userdata));

    /* print the raw unsigned userdata file */
    (void)(!fail && (o_flags & oflag_printud) && print_userdata(&nb, o_userdata));

    /* run unit test of SEE application */
    (void)(!fail && (o_flags & oflag_unittest) && run_tests());

    nobis_destroy(&nb, seew);

    nobis_cleanup(&nb);
  }

  return fail ? EXIT_FAILURE : EXIT_SUCCESS;
}
