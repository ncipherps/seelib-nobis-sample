/* sample-core.h
 * Copyright (C) 2015-2018 nCipher Security, LLC.  All rights reserved.
 * ---------------------------------------------------------------------
 * This module is designed to run inside an SEE application.  This
 * implements the core features of this project for Bitcoin Hierarchical
 * Deterministic Samples.  For testing only, this code can be compiled
 * into a host application and run in a host process.  HOWEVER, this
 * exposes critical secrets in memory and must only be used for test
 * keys that are not HSM secured.
 * ------------------------------------------------------------------ */
#ifndef SAMPLE_CORE_H
#define SAMPLE_CORE_H

#include "nobis.h"

/** @file sample-core.h
 *  @brief Core functions for Sample SEE machine
 *
 */


/* @brief Clean up module globals
 *
 * Cleans up module globals setup by sample_core_init() and
 * sample_core_runtimeinit().  This is meant to be called right before
 * program exit.  It may be possible to call the init functions again
 * after this completes but that is not tested and not really part of
 * the design.
 *
 * @param nb nCore abstraction.*/
void sample_core_cleanup(struct nobis *nb);

/* @brief Initialize module globals and state.
 *
 * Initializes module globals and state.  This is meant to be called when
 * the SEE machine starts up _BEFORE_ job processing is started.
 *
 * @param nb nCore abstraction.
 * @param module should be zero inside SEE; this parameter is to support
 *               building this code into a host application for testing only;
 * @return Status_OK or a failure code from messages-a-en.h. */
M_Status sample_core_init(struct nobis *nb, M_ModuleID module);

/* @brief Additional initialization of module globals and state.
 *
 * Runtime initialization of module globals and state.  This initialization
 * cannot execute until FIPS Authorization is loaded into the SEE application.
 * This functions is expected to be called right after FIPS is loaded to
 * complete this setup.  And it must execute successfully before any of the
 * jobs that handle sample keys can work.
 *
 * @param nb nCore abstraction.
 * @return Status_OK or a failure code from messages-a-en.h. */
M_Status sample_core_runtimeinit(struct nobis *nb);

/* @brief SampleEchoBlock
 *
 * Receive a byte block of arbitrary size and echo it back to the caller.
 *
 * @param nb nCore abstraction.
 *
 * @param args->block a block of arbitrary data to echo back to caller
 *
 * @param reply->block the block of arbitrary data received from the caller
 *
 * @param repcode [out] WalletRep_Success or a failure code
 * @return Status_OK or a failure code from messages-a-en.h. */
M_Status
SampleEchoBlock(struct nobis *nb,
                     const M_SampleOp_EchoBlock_Args *args,
                     M_SampleOp_EchoBlock_Reply *reply,
                     M_SampleRep *repcode);

/* @brief SamplePushBlock
 *
 * Receive a byte block of arbitrary size from a caller.
 *
 * @param nb nCore abstraction.
 *
 * @param args->block a block of arbitrary data recieved from caller
 *
 * @param repcode [out] WalletRep_Success or a failure code
 * @return Status_OK or a failure code from messages-a-en.h. */
M_Status
SamplePushBlock(struct nobis *nb,
                     const M_SampleOp_PushBlock_Args *args,
                     M_SampleRep *repcode);

/* @brief SamplePullBlock
 *
 * Receive a size value from a caller and return a byte block of that
 * size containing arbitrary data
 *
 * @param nb nCore abstraction.
 *
 * @param args->size size of arbitrary block of data to return
 *
 * @param reply->block a block of aribtrary data that is the size request by the caller
 *
 * @param repcode [out] WalletRep_Success or a failure code
 * @return Status_OK or a failure code from messages-a-en.h. */
M_Status
SamplePullBlock(struct nobis *nb,
                     const M_SampleOp_PullBlock_Args *args,
                     M_SampleOp_PullBlock_Reply *reply,
                     M_SampleRep *repcode);

/* @brief SamplePushPullBlock
 *
 * Receive a byte block of arbitrary size from a caller and recieve a
 * separate size value;  return a byte block of that size containing
 * arbitrary data
 *
 * @param nb nCore abstraction.
 *
 * @param args->block a block of arbitrary data recieved from caller
 * @param args->size size of arbitrary block of data to return
 *
 * @param reply->block a block of aribtrary data that is the size request by the caller
 *
 * @param repcode [out] WalletRep_Success or a failure code
 * @return Status_OK or a failure code from messages-a-en.h. */
M_Status
SamplePushPullBlock(struct nobis *nb,
                     const M_SampleOp_PushPullBlock_Args *args,
                     M_SampleOp_PushPullBlock_Reply *reply,
                     M_SampleRep *repcode);

/* @brief SampleNoOpTransact
 *
 * Receive a count from the caller and then execute that number of
 * NoOp transaction to the nCore firmware before returning
 *
 * @param nb nCore abstraction.
 *
 * @param args->count number of Cmd_NoOp to execute to firmware
 *
 * @param repcode [out] WalletRep_Success or a failure code
 * @return Status_OK or a failure code from messages-a-en.h. */
M_Status
SampleNoOpTransact(struct nobis *nb,
                     const M_SampleOp_NoOpTransact_Args *args,
                     M_SampleRep *repcode);

#endif
