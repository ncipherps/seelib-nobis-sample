#!/usr/bin/env python
#
# (C) nCipher Security Limited 2019
#
# The copyright in this software is the property of nCipher Security Limited.
# This software may not be used, sold, licensed, disclosed, transferred, copied,
# modified or reproduced in whole or in part or in any manner or form other than
# in accordance with the licence agreement provided with this software or
# otherwise without the prior written consent of nCipher Security Limited.
#

import nfpython as nf
import perf_common as pc
import perf

# ----------------------------------------------------
# Miscellaneous tests
# ----------------------------------------------------
class test_prime_gen(perf.PerfTest):
  description = '%(sz)d-bit prime generation.'
  name = 'Miscellaneous'
  cases = [(n/2,) for n in pc.rsa_sizes ]
  def __init__(self, sz):
    self.sz = sz
  def start_test(self, c):
    self.cmd = nf.Command(['GeneratePrime', 0, self.sz])
  def req_reps(self): return 512 * (2048 / self.sz)
  unit = 'primes'
  discard_reply = True

class test_random_gen(perf.PerfTest):
  description = 'Random material generation.'
  name = 'Miscellaneous'
  unit = '1k blocks'
  cases = [ () ]
  def __init__(self):
    pass
  def start_test(self, c):
    self.cmd = nf.Command(['GenerateRandom', 0, 1024])
  def req_reps(self): return 1024 * 16 # 16MB
  discard_reply = True

class test_noop(perf.PerfTest):
  description = 'NoOp'
  name = 'Miscellaneous'
  cases = [ () ]
  def __init__(self):
    pass
  def start_test(self, c):
    self.cmd = nf.Command(['NoOp', 0, c.module_id])
  def req_reps(self): return 1024 * 32
  unit = 'jobs'
  discard_reply = True

# ----------------------------------------------------
# SEE test using published object "sample"
# ----------------------------------------------------
class test_see_job(perf.PerfTest):
  name = 'Miscellaneous'
  unit = 'jobs'
  echopushpulllen = [48, 1024, 7*1024, 8*1024, 32*1024, 255*1024]
  cases = [ ('MachineVersion',1,None,None), ('NoOp',2,None,None)]
  cases += [ ('NoOpTransact', 8, 2**x, None) for x in range(0, 8)]
  cases += [ (name, id, datalen, None)
              for name, id in [('EchoBlock',4),('PushBlock',5), ('PullBlock',6)]
              for datalen in echopushpulllen ]
  cases += [ ('PushPullBlock', 7, datalen, datalen2)
              for datalen in echopushpulllen
              for datalen2 in echopushpulllen ]
  pubobj = 'sample'

  def __init__(self, jname, jid, jdatalen, jdatalen2):
    self.jname = jname
    self.jid = jid
    if jdatalen is not None:
      self.jdatalen = jdatalen
    if jdatalen2 is not None:
      self.jdatalen2 = jdatalen2

  def describe(self):
    if hasattr(self,"jdatalen2"):
      description = 'SEE %(jname)s %(jdatalen)6d - %(jdatalen2)6d'
    elif hasattr(self,"jdatalen"):
      if self.jid == 8:
        description = 'SEE %(jname)s %(jdatalen)dx.'
      else:
        description = 'SEE %(jdatalen)d byte %(jname)s.'
    else:
      description = 'SEE %(jname)s Job Test.'
    return description % (self.__dict__)

  def start_test(self, c):
    # connect to the published object
    w=c.transact(nf.Command(['GetPublishedObject',0,0,test_see_job.pubobj,c.module_id])).reply.object

    m = nf.marshal(nf.Word(self.jid))
    if self.jid in [1, 2]:
      pass
    elif self.jid in [4, 5]:
      bb = nf.ByteBlock('00')*self.jdatalen
      m += nf.marshal(bb)
    elif self.jid in [6, 8]:
      m += nf.marshal(nf.Word(self.jdatalen))
    elif self.jid == 7:
      bb = nf.ByteBlock('00')*self.jdatalen
      m += nf.marshal(bb)
      m += nf.marshal(nf.Word(self.jdatalen2))
    else:
      raise ValueError("jid %d not handled" % self.jid)

    # this command will be optimized and execute repeatedly
    self.cmd = nf.Command(['SEEJob',0,w,m])

  def req_reps(self):
    reps = 1024*32
    if self.jid == 8:
      reps /= self.jdatalen
    elif self.jid == 7:
      reps /= (self.jdatalen + self.jdatalen2 + 1023)/1024
    elif self.jid in [4, 5, 6]:
      reps /= (self.jdatalen + 1023)/1024
    if reps < 256: reps = 256
    return reps

  discard_reply = True


tests = perf.find_tests('perf_misc', 'Miscellaneous', 'misc')

