# Copyright (C) 2014 nCipher Security, LLC.  All rights reserved.
# This example source code is provided for your information and assistance.
# Note that there is NO WARRANTY.

import nfpython as nf
import nfkm
import rqcard
import optparse

op = optparse.OptionParser(version="%prog 1.0")
op.add_option('-m','--module', dest='module', type=int, default=1,
              metavar='MODULE', help='Select module to use. [default: %default]')
op.add_option('-p','--pubobj', dest='pubname',
              metavar='PUBOJB', help='Published object for SEE application.')
op.add_option('-u','--userdata', dest='userdata',
              metavar='FILE', help='userdata SAR file used to start SEE applicaiton')
op.add_option('-d','--trace', dest='trace', default=False, action='store_true',
              help='start SEE application with trace debug enabled [default: %default]')
op.add_option('-b','--dseebearercert', dest='dseebearercert',
              metavar='FILE', help='marshalled certlist dseebearercert')
op.add_option('','--useknso', dest='useknso', default=False, action='store_true',
              help='use KNSO key insteaed of DSEE to enable debug [default: %default]')
opts, args=op.parse_args()

if opts.pubname is None:
  op.error('Publisehd object name must be specified.')

if opts.userdata is None:
  op.error('userdata SAR file name must be specified.')

moduleid=opts.module
pubname=opts.pubname

# read all the userdata in on shot, not good for very large file
# but userdata is usually a small file
userdata=open(opts.userdata,'rb').read()
userdata=nf.ByteBlock(userdata, fromraw=True);

# Cmd_SetPublishedObject requires a privileged connection
conn=nf.connection(priv=True)
info=nfkm.getinfo(conn)

def findexistingobject(winfo, module, hash):
  for e in winfo.existingobjects:
    if e.module == module and e.hash == hash:
      cmd = nf.Command(['Duplicate', 0, e.id])
      return conn.transact(cmd).reply.newkey
  return None

kauth = ltauth = hkauth = None
if opts.dseebearercert:
  data = open(opts.dseebearercert,'rb').read()
  data = nf.ByteBlock(data, fromraw=True)
  opts.dseebearercert = nf.unmarshal(data, nf.CertificateList)[0]
elif opts.trace and not info.flags.isset('SEEDebugForAll') \
    and info.modules[moduleid-1].state != 'Factory':
  hkauth = info.hkdsee if not opts.useknso else info.hknso
  kauth = findexistingobject(info, moduleid, hkauth)
  if kauth is None:
    # load Admin key kdsee or nso
    if opts.useknso:
      args = rqcard.RQArgs([0, 'scroll', 0, 'specific', 0, moduleid, 'loadadmin',
            0, ['nso'],[], 'Loading NSO Admin Root Key'])
    else:
      args = rqcard.RQArgs([0, 'scroll', 0, 'specific', 0, moduleid, 'loadadmin',
            0, ['dsee'],[], 'Loading DSEE Admin Key'])
    res = rqcard.doload(conn, args)
    kauth = res.pmr.result.keys[0]
    ltauth = res.pmr.result.keytokens[0]

# load the userdata file into the HSM
# one shot, not good for large user data
# should handle <= 255 KB on bigjob enabled modules
# only <= 7KB without bigjobs
cmd=nf.Command(['CreateBuffer',0,moduleid,0,len(userdata)])
idbuf=conn.transact(cmd).reply.id
cmd=nf.Command(['LoadBuffer',0,idbuf,'Final',userdata])
conn.transact(cmd)

if opts.trace:
  # setup start command with debug enabled
  cmd=nf.Command(['CreateSEEWorld','certs_present','EnableDebug',idbuf])
  if info.modules[moduleid-1].state != 'Factory':
    if opts.dseebearercert:
      cmd.certs = opts.dseebearercert
    elif not info.flags.isset('SEEDebugForAll'):
      cmd.certs.certs.append(nf.Certificate([hkauth, 'signingkey', kauth]))
      if not opts.useknso:
        cmd.certs.certs.append(nfkm.loaddelegcert(conn,'DSEE'))
else:
  cmd=nf.Command(['CreateSEEWorld',0,0,idbuf])

# start the SEE application
r = conn.transact(cmd)
seewid = r.reply.worldid
if r.reply.initstatus != 0:
  print("CreateSEEWorld initstatus == %d" % r.reply.initstatus)

# set published object to keep SEE applicaiton running after we exit and to
# allow other processes to find the SEE applicaiton by name
cmd=nf.Command(['SetPublishedObject',0,'object_present',pubname,moduleid,seewid])
conn.transact(cmd)

# clean up the objects we created
conn.transact(nf.Command(['Destroy',0,seewid]))
conn.transact(nf.Command(['Destroy',0,idbuf]))
if kauth: conn.transact(nf.Command(['Destroy',0,kauth]))
if ltauth: conn.transact(nf.Command(['Destroy',0,ltauth]))

