# project.mak                                    -*- mode: makefile; -*-
# Copyright (C) 2010-2015 nCipher Security, LLC.  All rights reserved.
# ----------------------------------------------------------------------
# Reusable fragments for Makefiles in SEE projects.  Before including
# this file specify the following macros:
#
#   PROJECT - name of the project (used as part of SEE machine names)
#   PROJECT_CONTROL - name of seeinteg key to use for signing
#   PROJECT_FLAGS   - extra things to pass to see-sock-serv
#   PROJECT_MODULE  - number of module to use
#   TARGETS - things that should get built when no arguments are given
#   SRCPATH - location of source files
#   PPC_SEELIB_OBJECTS - object files for PowerPC builds
#   ARM_SEELIB_OBJECTS - object files for StrongARM builds
#   PPC_OBJECTS   - object files for PowerPC builds
#   ARM_OBJECTS   - object files for StrongARM (older units and Edge)
#   WIN32_OBJECTS - object files for Windows targets
#   UNIX_OBJECTS  - object files for Unix targets
#
# NFAST_PATH is used for command execution while NFAST_HOME is used for
# command parameters.

#NFAST_HOME = c:\Program Files (x86)\nCipher\nfast
#NFAST_KMDATA = c:\ProgramData\nCipher\Key Management Data
NFAST_PATH = $(NFAST_HOME)
NFAST_DEV = "$(NFAST_HOME)"/c/csd
NFAST_EX = c/csd/examples
NFAST_EXAMPLE = $(NFAST_DEV)/examples
NFAST_SEE_INCLUDE = $(NFAST_DEV)/include-see
GLIBSEE_CPPFLAGS = -I$(SRCPATH) \
                  -I$(NFAST_SEE_INCLUDE)/cutils \
                  -I$(NFAST_SEE_INCLUDE)/glibsee \
                   -I$(NFAST_SEE_INCLUDE)/hilibs \
		  -I$(NFAST_EXAMPLE)/hilibs \
		  -I$(NFAST_EXAMPLE)/cutils
BSDLIB_CPPFLAGS = -DNF_CROSSCC_BSDUSR=1 -D_THREAD_SAFE -I$(SRCPATH) \
	          -I$(NFAST_SEE_INCLUDE)/bsdlib \
	          -I$(NFAST_SEE_INCLUDE)/cutils \
	          -I$(NFAST_SEE_INCLUDE)/bsdsee \
	          -I$(NFAST_SEE_INCLUDE)/ssllib \
	          -I$(NFAST_SEE_INCLUDE)/sworld \
	          -I$(NFAST_SEE_INCLUDE)/hilibs \
		  -I$(NFAST_EXAMPLE)/cutils \
		  -I$(NFAST_EXAMPLE)/hilibs \
		  -I$(NFAST_EXAMPLE)/bsdsee
SEELIB_CPPFLAGS = -DNF_CROSSCC=1 -D_THREAD_SAFE -I$(SRCPATH) \
	         -I$(NFAST_SEE_INCLUDE)/module \
	         -I$(NFAST_SEE_INCLUDE)/module/rtlib \
	         -I$(NFAST_EXAMPLE)/csee/utils/module
GLIBSEE_CFLAGS = -DNFAST_CONF_H=\"nfast-conf-auto.h\" -DNF_CROSSCC_PPC_GCC \
		-D__NG_BLOB__ -D__NGSOLO__ -D_GNU_WRAPPER_ENABLED_ -O3 -g \
		-Wall -c -fmessage-length=0 -Wpointer-arith -Wwrite-strings \
		-Wstrict-prototypes -mpowerpc -mcpu=e5500 \
		-mno-toc -mbig-endian -mhard-float -mno-multiple -mno-string \
		-meabi -mprototype -DNF_CROSSCC=1 -DNF_CROSSCC_PPC_GCC=1 \
		-D_THREAD_SAFE
CROSS_CFLAGS = -O3 -Wall -Werror -Wpointer-arith -Wwrite-strings \
	       -Wstrict-prototypes
GLIBSEE_LDFLAGS = -Wl,-wrap=read -Wl,-wrap=write -Wl,-wrap=socket \
		-Wl,-wrap=send -Wl,-wrap=sendto -Wl,-wrap=recv \
		-Wl,-wrap=recvfrom -Wl,-wrap=listen -Wl,-wrap=connect \
		-Wl,-wrap=bind -Wl,-wrap=setsockopt -Wl,-wrap=select \
		-Wl,-wrap=accept -Wl,-wrap=poll -Bstatic
GLIBSEE_LDLIB	= $(PPCELF_LIBPATH)/libipccore.a \
		$(PPCELF_LIBPATH)/libfaksys.a \
		$(PPCELF_LIBPATH)/libcutils.a \
		$(PPCELF_LIBPATH)/libvfsextras.a \
		$(PPCELF_LIBPATH)/libseewrpr.a \
		$(PPCELF_LIBPATH)/seelib.a \
		$(PPCELF_LIBPATH)/libipccore.a \
		$(PPCELF_LIBPATH)/libsolotrace.a \
		$(PPCELF_LIBPATH)/libnfstub.a \
		$(PPCELF_LIBPATH)/libfaksys.a \
		$(PPCELF_LIBPATH)/libseewrpr.a \
		-lpthread -lrt
ELFTOOL       = "$(NFAST_PATH)/bin/elftool"
TCT2          = "$(NFAST_PATH)/bin/tct2"
LOADMACHE     = "$(NFAST_PATH)/bin/loadmache"
CPIOC         = "$(NFAST_PATH)/bin/cpioc"
GENERATEKEY   = "$(NFAST_PATH)/bin/generatekey"
SEE_SOCK_SERV = "$(NFAST_PATH)/bin/see-sock-serv"
PRELOAD       = "$(NFAST_PATH)/bin/preload"

UNIX_NFUSER_OBJECTS = nfopt.o nftypes.o tokenise.o readpp-unix.o \
	ncthread-unix.o ncthread-upcalls.o normalmalloc.o
WIN32_NFUSER_OBJECTS = nfopt.obj nftypes.obj tokenise.obj \
        readpp-nt.obj ncthread-nt.obj condvar.obj ncthread-upcalls.obj

CLEANUP = *.o *.obj *.elf *.pdb *.cpio

##### Target Rules
all: $(TARGETS)

clean: local-clean
	-del /q /f $(CLEANUP)
	-rm -rf $(CLEANUP)

.PHONY: all clean local-clean

$(PROJECT)-userdata.sar: $(PROJECT)-userdata.cpio
	$(TCT2) --sign-and-pack --machine-key-ident $(PROJECT_MACHSIGN) \
	  -k $(PROJECT_CONTROL) -m $(PROJECT_MODULE) \
	  -o $@ $(PROJECT)-userdata.cpio

##### Settings and rules for SEEMachineType_PowerPCELF.
PPCELF_PREFIX   = powerpc-codesafe-linux-gnu-
PPCELF_LIBNAME  = ppc-linux
PPCELF_CPPFLAGS = -DNF_CROSSCC_PPC_GCC=1 -DNF_CROSSCC=1 -D_THREAD_SAFE \
		-I$(SRCPATH) \
		-I$(NFAST_SEE_INCLUDE)/module \
		-I$(NFAST_SEE_INCLUDE)/module/glibsee
PPCELF_CFLAGS   = -mpowerpc -mcpu=e5500 -mno-toc -mbig-endian \
	       -mhard-float -mno-multiple -mno-string -meabi \
	       -mprototype -mstrict-align -memb -fno-builtin \
	       $(CROSS_CFLAGS)
PPCELF_LDFLAGS = -Bstatic -lpthread
PPCELF_LIBPATH = $(NFAST_DEV)/lib-$(PPCELF_LIBNAME)-gcc
PPCELF_SEELIB_LDLIBS = $(PPCELF_LIBPATH)/seelib.a $(PPCELF_LIBPATH)/rtlib/librtusr.a
PPCELF_CC = "$(NFAST_PATH)"/gcc/bin/$(PPCELF_PREFIX)gcc
PPCELF_LD = "$(NFAST_PATH)"/gcc/bin/$(PPCELF_PREFIX)gcc

nobis-seelib-ppc-linux.o: $(SRCPATH)/nobis.c $(SRCPATH)/nobis.h
	$(PPCELF_CC) $(PPCELF_CFLAGS) $(PPCELF_CPPFLAGS) \
	  -o $@ -c $(SRCPATH)/nobis.c

$(PROJECT)-seelib-ppc-linux.elf: $(PPCELF_SEELIB_OBJECTS)
	$(PPCELF_LD) $(PPCELF_LDFLAGS) -o $@ $(PPCELF_SEELIB_OBJECTS) \
	  $(PPCELF_SEELIB_LDLIBS)

$(PROJECT)-seelib-ppc-linux.sar: $(PROJECT)-seelib-ppc-linux.elf
	$(TCT2) --sign-and-pack --is-machine -k $(PROJECT_MACHSIGN) \
	  -T PowerPCELF -m $(PROJECT_MODULE) -o $@ $(PROJECT)-seelib-ppc-linux.elf

##### Settings and rules for SEEMachineType_PowerPCSXF.
PPC_PREFIX   = powerpc-codesafe-linux-gnu-
PPC_LIBNAME  = ppc
PPC_CPPFLAGS = -DNF_CROSSCC_PPC_GCC=1
PPC_CFLAGS   = -mpowerpc -mcpu=603e -mno-toc -mbig-endian \
	       -mhard-float -mno-multiple -mno-string -meabi \
	       -mprototype -mstrict-align -memb -fno-builtin \
	       $(CROSS_CFLAGS)
PPC_LDFLAGS = -warn-common -nostdlib -Ttext 0xa00000 -Tdata 0xd00000
PPC_LIBPATH = $(NFAST_DEV)/lib-$(PPC_LIBNAME)-gcc
PPC_CC = "$(NFAST_HOME)/gcc/bin/$(PPC_PREFIX)gcc"
PPC_LD = "$(NFAST_HOME)/gcc/bin/$(PPC_PREFIX)ld"
PPC_SEELIB_LDLIBS = $(PPC_LIBPATH)/seelib.a \
	            $(PPC_LIBPATH)/rtlib/librtusr.a
PPC_LDLIBS = $(PPC_LIBPATH)/libncssl.a \
	     $(PPC_LIBPATH)/libncsslcrypt.a \
	     $(PPC_LIBPATH)/libloadkeysw.a \
	     $(PPC_LIBPATH)/libpreparsed-certs.a \
	     $(PPC_LIBPATH)/libnfkm.a \
	     $(PPC_LIBPATH)/libcutils.a \
	     $(PPC_LIBPATH)/hostinetsocks.o \
	     $(PPC_LIBPATH)/procfs.o \
	     $(PPC_LIBPATH)/libvfsextras.a \
	     $(PPC_LIBPATH)/libc.a

nobis-seelib-ppc.o: $(SRCPATH)/nobis.c $(SRCPATH)/nobis.h
	$(PPC_CC) $(PPC_CFLAGS) $(PPC_CPPFLAGS) $(SEELIB_CPPFLAGS) \
	  -o $@ -c $(SRCPATH)/nobis.c

$(PROJECT)-seelib-ppc.elf: $(PPC_SEELIB_OBJECTS)
	$(PPC_LD) $(PPC_LDFLAGS) -o $@ $(PPC_SEELIB_OBJECTS) \
	  $(PPC_SEELIB_LDLIBS)

$(PROJECT)-seelib-ppc.sxf: $(PROJECT)-seelib-ppc.elf
	$(ELFTOOL) --sxf $(PROJECT)-seelib-ppc.elf $@

$(PROJECT)-seelib-ppc.sar: $(PROJECT)-seelib-ppc.sxf
	$(TCT2) --sign-and-pack --is-machine -k $(PROJECT_MACHSIGN) \
	  -T PowerPCSXF -m $(PROJECT_MODULE) -o $@ $(PROJECT)-seelib-ppc.sxf

##### Settings and rules for SEEMachineType_gen1AIF and SEEMachineType_ARMtype2.
# SEEMachineType_ARMtype2 only has different linker directives see below.
ARM_PREFIX   = strongarm-codesafe-elf-
ARM_LIBNAME  = arm
ARM_CPPFLAGS = -DNF_CROSSCC_ARM_GCC=1
ARM_CFLAGS   = $(CROSS_CFLAGS) -mapcs-32 -msoft-float -mlittle-endian \
	       -march=armv4 -nostdinc -fno-builtin
ARM_LDFLAGS = -warn-common -nostdlib -Ttext 0xa00000 -Tdata 0xd00000
ARM_LIBPATH = $(NFAST_DEV)/lib-$(ARM_LIBNAME)-gcc
ARM_CC = "$(NFAST_HOME)/gcc/bin/$(ARM_PREFIX)gcc"
ARM_LD = "$(NFAST_HOME)/gcc/bin/$(ARM_PREFIX)ld"
ARM_SEELIB_LDLIBS = $(ARM_LIBPATH)/seelib.a \
	            $(ARM_LIBPATH)/rtlib/librtusr.a
ARM_LDLIBS = $(ARM_LIBPATH)/libncssl.a \
	     $(ARM_LIBPATH)/libncsslcrypt.a \
	     $(ARM_LIBPATH)/libpreparsed-certs.a \
	     $(ARM_LIBPATH)/libloadkeysw.a \
	     $(ARM_LIBPATH)/libnfkm.a \
	     $(ARM_LIBPATH)/libcutils.a \
	     $(ARM_LIBPATH)/hostinetsocks.o \
	     $(ARM_LIBPATH)/procfs.o \
	     $(ARM_LIBPATH)/libvfsextras.a \
	     $(ARM_LIBPATH)/libc.a

nobis-seelib-arm.o: $(SRCPATH)/nobis.c $(SRCPATH)/nobis.h
	$(ARM_CC) $(ARM_CFLAGS) $(ARM_CPPFLAGS) $(SEELIB_CPPFLAGS) \
	  -o $@ -c $(SRCPATH)/nobis.c

$(PROJECT)-seelib-arm.elf: $(ARM_SEELIB_OBJECTS)
	$(ARM_LD) $(ARM_LDFLAGS) -o $@ $(ARM_SEELIB_OBJECTS) \
	  $(ARM_SEELIB_LDLIBS)

$(PROJECT)-seelib-arm.sxf: $(PROJECT)-seelib-arm.elf
	$(ELFTOOL) --sxf $(PROJECT)-seelib-arm.elf $@

$(PROJECT)-seelib-arm.sar: $(PROJECT)-seelib-arm.sxf
	$(TCT2) --sign-and-pack --is-machine -k $(PROJECT_MACHSIGN) \
	  -T gen1AIF -m $(PROJECT_MODULE) -o $@ $(PROJECT)-seelib-arm.sxf

##### Settings and rules for SEEMachineType_ARMtype2 (nShield Edge devices).
EDG_LDFLAGS = -warn-common -nostdlib -Ttext 0x8c00000 -Tdata 0x8e00000

$(PROJECT)-seelib-edg.elf: $(ARM_SEELIB_OBJECTS)
	$(ARM_LD) $(EDG_LDFLAGS) -o $@ $(ARM_SEELIB_OBJECTS) \
	  $(ARM_SEELIB_LDLIBS)

$(PROJECT)-seelib-edg.sxf: $(PROJECT)-seelib-edg.elf
	$(ELFTOOL) --sxf $(PROJECT)-seelib-edg.elf $@

$(PROJECT)-seelib-edg.sar: $(PROJECT)-seelib-edg.sxf
	$(TCT2) --sign-and-pack --is-machine -k $(PROJECT_MACHSIGN) \
	  -T ARMtype2 -m $(PROJECT_MODULE) -o $@ \
	  $(PROJECT)-seelib-edg.sxf

##### Settings and rules for WIN32 executables.
NFAST_WIN32_CC = vs2013-32
NFAST_WIN32_INCLUDE = $(NFAST_DEV)/$(NFAST_WIN32_CC)/include
NFAST_WIN32_LIB = $(NFAST_DEV)/$(NFAST_WIN32_CC)/lib

WIN32_CFLAGS = -W3 -Ox -Zi -WX -we4101 -we4189 -wd4996 -wd4237 -MT
WIN32_CPPFLAGS = -I$(NFAST_WIN32_INCLUDE)/sworld \
	         -I$(NFAST_WIN32_INCLUDE)/nflibs \
	         -I$(NFAST_WIN32_INCLUDE)/msgs \
	         -I$(NFAST_WIN32_INCLUDE)/hilibs \
	         -I$(NFAST_WIN32_INCLUDE)/cutils \
	         -I$(NFAST_WIN32_INCLUDE)/nflog \
	         -I$(NFAST_EXAMPLE)/nfuser \
	         -I$(NFAST_EXAMPLE)/csee/utils/host -I.
WIN32_CC = cl
WIN32_LD = link
WIN32_LDFLAGS = 
WIN32_LDLIBS = $(NFAST_WIN32_LIB)/libtrqcard.lib \
	       $(NFAST_WIN32_LIB)/libtnfkm.lib \
	       $(NFAST_WIN32_LIB)/libtnfstub.lib \
	       $(NFAST_WIN32_LIB)/libtnflog.lib \
	       $(NFAST_WIN32_LIB)/libtcutils.lib \
	       advapi32.lib ws2_32.lib user32.lib gdi32.lib \
	       kernel32.lib shell32.lib Bcrypt.lib

nobis.obj: $(SRCPATH)/nobis.c $(SRCPATH)/nobis.h
	$(WIN32_CC) $(WIN32_CFLAGS) $(WIN32_CPPFLAGS) -Fo$@ \
	  -c $(SRCPATH)/nobis.c

readpp-nt.obj: "$(NFAST_PATH)/$(NFAST_EX)/nfuser/readpp-nt.c"
	$(WIN32_CC) $(WIN32_CFLAGS) $(WIN32_CPPFLAGS) -Fo$@ \
	  -c "$(NFAST_HOME)/$(NFAST_EX)/nfuser/readpp-nt.c"

nfopt.obj: "$(NFAST_PATH)/$(NFAST_EX)/nfuser/nfopt.c"
	$(WIN32_CC) $(WIN32_CFLAGS) $(WIN32_CPPFLAGS) -Fo$@ \
	  -c "$(NFAST_HOME)/$(NFAST_EX)/nfuser/nfopt.c"

nftypes.obj: "$(NFAST_PATH)/$(NFAST_EX)/nfuser/nftypes.c"
	$(WIN32_CC) $(WIN32_CFLAGS) $(WIN32_CPPFLAGS) -Fo$@ \
	  -c "$(NFAST_HOME)/$(NFAST_EX)/nfuser/nftypes.c"

tokenise.obj: "$(NFAST_PATH)/$(NFAST_EX)/nfuser/tokenise.c"
	$(WIN32_CC) $(WIN32_CFLAGS) $(WIN32_CPPFLAGS) -Fo$@ \
	  -c "$(NFAST_HOME)/$(NFAST_EX)/nfuser/tokenise.c"

ncthread-nt.obj: "$(NFAST_PATH)/$(NFAST_EX)/nfuser/ncthread-nt.c"
	$(WIN32_CC) $(WIN32_CFLAGS) $(WIN32_CPPFLAGS) -Fo$@ \
	  -c "$(NFAST_HOME)/$(NFAST_EX)/nfuser/ncthread-nt.c"

condvar.obj: "$(NFAST_PATH)/$(NFAST_EX)/nfuser/condvar.c"
	$(WIN32_CC) $(WIN32_CFLAGS) $(WIN32_CPPFLAGS) -Fo$@ \
	  -c "$(NFAST_HOME)/$(NFAST_EX)/nfuser/condvar.c"

ncthread-upcalls.obj: \
	  "$(NFAST_PATH)/$(NFAST_EX)/nfuser/ncthread-upcalls.c"
	$(WIN32_CC) $(WIN32_CFLAGS) $(WIN32_CPPFLAGS) -Fo$@ \
	  -c "$(NFAST_HOME)/$(NFAST_EX)/nfuser/ncthread-upcalls.c"

$(PROJECT).exe: $(PROJECT).obj $(WIN32_OBJECTS)
	$(WIN32_LD) $(WIN32_LDLAGS) -out:$@ \
	  -DEBUG -PDB:$(PROJECT).pdb \
	  $(PROJECT).obj $(WIN32_OBJECTS) $(WIN32_LDLIBS)

$(PROJECT)-dbgseecore.exe: $(WIN32_SEECORE_OBJECTS) $(WIN32_OBJECTS)
	$(WIN32_LD) $(WIN32_LDLAGS) -out:$@ \
	  -DEBUG -PDB:$(PROJECT)-dbgseecore.pdb \
	  $(WIN32_SEECORE_OBJECTS) $(WIN32_OBJECTS) $(WIN32_LDLIBS)

##### Settings and rules for Unix executables.
NFAST_UNIX_CC = gcc
NFAST_INCLUDE = $(NFAST_DEV)/$(NFAST_UNIX_CC)/include
CC = gcc
CFLAGS   = -g -Wall -Werror
CPPFLAGS = -I$(NFAST_INCLUDE)/sworld \
	   -I$(NFAST_INCLUDE)/nflibs \
	   -I$(NFAST_INCLUDE)/msgs \
	   -I$(NFAST_INCLUDE)/hilibs \
	   -I$(NFAST_INCLUDE)/cutils \
	   -I$(NFAST_INCLUDE)/nflog \
	   -I$(NFAST_EXAMPLE)/nfuser \
	   -I$(NFAST_EXAMPLE)/csee/utils/host -I.
LD = gcc
LDFLAGS =
LIBPATH = $(NFAST_DEV)/$(NFAST_UNIX_CC)/lib
LDLIBS = $(LIBPATH)/libnfkm.a \
	 $(LIBPATH)/libnfstub.a \
	 $(LIBPATH)/libnflog.a \
	 $(LIBPATH)/libcutils.a \
	 -lpthread

nobis.o: $(SRCPATH)/nobis.c $(SRCPATH)/nobis.h
	$(CC) $(CFLAGS) $(CPPFLAGS) -o $@ \
	  -c $(SRCPATH)/nobis.c

readpp-unix.o: $(NFAST_PATH)/$(NFAST_EX)/nfuser/readpp-unix.c
	$(CC) $(CFLAGS) $(CPPFLAGS) -o $@ \
	  -c "$(NFAST_HOME)/$(NFAST_EX)/nfuser/readpp-unix.c"

tokenise.o: $(NFAST_PATH)/$(NFAST_EX)/nfuser/tokenise.c
	$(CC) $(CFLAGS) $(CPPFLAGS) -o $@ \
	  -c "$(NFAST_HOME)/$(NFAST_EX)/nfuser/tokenise.c"

nftypes.o: $(NFAST_PATH)/$(NFAST_EX)/nfuser/nftypes.c
	$(CC) $(CFLAGS) $(CPPFLAGS) -o $@ \
	  -c "$(NFAST_HOME)/$(NFAST_EX)/nfuser/nftypes.c"

nfopt.o: $(NFAST_PATH)/$(NFAST_EX)/nfuser/nfopt.c
	$(CC) $(CFLAGS) $(CPPFLAGS) -o $@ \
	  -c "$(NFAST_HOME)/$(NFAST_EX)/nfuser/nfopt.c"

ncthread-unix.o: $(NFAST_PATH)/$(NFAST_EX)/nfuser/ncthread-unix.c
	$(CC) $(CFLAGS) $(CPPFLAGS) -o $@ \
	  -c "$(NFAST_HOME)/$(NFAST_EX)/nfuser/ncthread-unix.c"

ncthread-upcalls.o: $(NFAST_PATH)/$(NFAST_EX)/nfuser/ncthread-upcalls.c
	$(CC) $(CFLAGS) $(CPPFLAGS) -o $@ \
	  -c "$(NFAST_HOME)/$(NFAST_EX)/nfuser/ncthread-upcalls.c"

normalmalloc.o: $(NFAST_PATH)/$(NFAST_EX)/cutils/normalmalloc.c \
	  $(NFAST_PATH)/$(NFAST_EX)/cutils/debugmalloc.h
	$(CC) $(CFLAGS) $(CPPFLAGS) -o $@ -c \
	  "$(NFAST_HOME)/$(NFAST_EX)/cutils/normalmalloc.c"

host-buffer.o: $(NFAST_PATH)/$(NFAST_EX)/hilibs/host-buffer.c \
	  $(NFAST_PATH)/$(NFAST_EX)/hilibs/host-buffer.h
	$(CC) $(CFLAGS) $(CPPFLAGS) -o $@ -c \
	  "$(NFAST_HOME)/$(NFAST_EX)/hilibs/host-buffer.c"

$(PROJECT)-dbgseecore: $(UNIX_OBJECTS) $(UNIX_SEECORE_OBJECTS)
	$(LD) $(LDFLAGS) -o $@ \
	  $(UNIX_SEECORE_OBJECTS) $(UNIX_OBJECTS) $(LDLIBS)

$(PROJECT): $(PROJECT).o $(UNIX_OBJECTS)
	$(LD) $(LDFLAGS) -o $@ $(PROJECT).o $(UNIX_OBJECTS) $(LDLIBS)
