== Sample seelib nobis Project

Windows and Linux are the supported host platforms for this project.
It works with most Security World settings.  On Windows, Visual Studio
2013 has been tested but Visual Studio Express has not (though it
should work, in principal).

Makefile targets
================
Default target builds the SEE machine for PowerPCSXF (ppc) and
PowerPCELF (ppc-linux).

win32all and unixall
--------------------
build host executable files

sample-win32-ppc sample-unix-ppc
--------------------------------
Load SEE macine on PowerPCSXF module and run sample executable to do a
test run.  Note:  sample-seelib-userdata.in file must be created
before these targets will work.

sample-win32-ppc-linux sample-unix-ppc-linux
--------------------------------
Load SEE macine on PowerPCELF (XC Platform) module and run sample
executable to do a test run.  Note:  sample-seelib-userdata.in file must
be created before these targets will work.

Creating the user data file
===========================
`sample -CPU sample-seelib-userdata.in`--This command line will create
and print the user data file.  Some Makefile targets assume the name
of this file is "sample-seelib-userdata.in".

The sample executable is created with the win32all or unixall Makefile
target.  Or sample.exe (Windows) or sample (Unix).

sample-dbgseecore executable
============================
The .c module and executable is meant to allow running part of the 
SEE application in a host application to support debugging of the
SEE code in a host emulated environment.



